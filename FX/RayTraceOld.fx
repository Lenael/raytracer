
// Indices of refraction
#define Air 1.0
#define Bubble 1.06

// Air to glass ratio of the indices of refraction (Eta)
#define Eta (Air / Bubble)

// see http://en.wikipedia.org/wiki/Refractive_index Reflectivity
#define R0 (((Air - Bubble) * (Air - Bubble)) / ((Air + Bubble) * (Air + Bubble)))

// Adjust NUM_STACK_NODES in C file, if this value is changed.
#define MAX_DEPTH 5

#define NUM_SPHERES 6
#define NUM_LIGHTS 1

#define INFINITY 1000000.0


////////////////////////////////////////////////////////////////////////////


Buffer<float3> b_directions;
Buffer<float4> b_positions;

#define NUM_STACK_NODES (2 * 2 * 2 * 2 * 2 - 1)
#define STACK_NODE_FLOATS (4 + (3 ) + 4 + 4 + (3 ) + 4 + 1 + 1 + 1 + 1)
#define WIDTH 640
#define HEIGHT 480

struct Stack
{
   float4 position;
   float3 direction;
   float4 color;
   float4 hitPosition;
   float3 hitDirection;
   float4 biasedPositiveHitPosition;
   float valid;
   float fresnel;
   float sphereIndex;
   float insideSphere;
};


RWStructuredBuffer<Stack> b_stacks;

struct Material
{
   float4 emissiveColor;
   float4 diffuseColor;
   float4 specularColor;
   float shininess;
   float alpha;
   float reflectivity;
};

struct Sphere
{
   float4 center;
   float radius;
   // Padding[3]
   Material material;
};

StructuredBuffer<Sphere> b_spheres;

struct PointLight
{
   float4 position;
   float4 color;
};

StructuredBuffer<PointLight> b_pointLights;


float fresnel(float3 incident, float3 normal, float r0)
{
   // see http://en.wikipedia.org/wiki/Schlick%27s_approximation

   return r0 + (1.0 - r0) * pow(1.0 - dot(-incident, normal), 5.0);
}

int intersectRaySphere(out float tNear, out float tFar, out bool insideSphere, float4 rayStart, float3 rayDirection, float4 sphereCenter, float radius)
{
   // see http://de.wikipedia.org/wiki/Quadratische_Gleichung (German)
   // see Real-Time Collision Detection p177

   int intersections = 0;
   float3 m;
   float b, c, discriminant, sqrtDiscriminant, t;

   //

   m = (rayStart - sphereCenter).xyz;

   b = dot(m, rayDirection);
   c = dot(m, m) - radius * radius;

   // Ray pointing away from sphere and ray starts outside sphere.
   if (b > 0.0 && c > 0.0)
   {
      return intersections;
   }

   discriminant = b * b - c;

   // No solution, so no intersection. Ray passes the sphere.
   if (discriminant < 0.0)
   {
      return intersections;
   }

   sqrtDiscriminant = sqrt(discriminant);

   // If we come so far, we have at least one intersection.

   insideSphere = false;

   t = -b - sqrtDiscriminant;

   // Ray starts inside the sphere, as t is negative.
   if (t < 0.0)
   {
      // Use origin as intersection point.
      t = 0.0;

      insideSphere = true;
   }

   intersections++;

   tNear = t;

   // Tangent point. Only one intersection. So leave.
   if (discriminant == 0.0)
   {
      // Save in both for convenience.
      tFar = t;

      return intersections;
   }

   //

   t = -b + sqrtDiscriminant;

   intersections++;

   tFar = t;

   return intersections;
}

int getReflectIndex(int index)
{
   return index * 2 + 1;
}

int getRefractIndex(int index)
{
   return index * 2 + 2;
}

void trace(int pixelPos, int maxLoops, int rayIndex)
{
   if (b_stacks[pixelPos * maxLoops + rayIndex].valid <= 0.0)
   {
      return;
   }

   const float bias = 1e-4;

   int i, k;

   float tNear = INFINITY;
   int sphereNearIndex = -1;
   bool insideSphereNear = false;

   float3 ray;

   //

   float4 rayPosition = b_stacks[pixelPos * maxLoops + rayIndex].position;

   float3 rayDirection = b_stacks[pixelPos * maxLoops + rayIndex].direction;

   for (i = 0; i < NUM_SPHERES; i++)
   {
      float t0 = INFINITY;
      float t1 = INFINITY;
      bool insideSphere = false;

      int numberIntersections = intersectRaySphere(t0, t1, insideSphere, rayPosition, rayDirection, b_spheres[i].center, b_spheres[i].radius);

      if (numberIntersections > 0)
      {
         // If intersection happened inside the sphere, take second intersection point, as this one is on the surface.
         if (insideSphere)
         {
            t0 = t1;
         }

         // Found a sphere, which is closer.
         if (t0 < tNear)
         {
            tNear = t0;
            sphereNearIndex = i;
            insideSphereNear = insideSphere;	
         }
      }
   }

   //

   // No intersection, return background color / ambient light.
   if (sphereNearIndex < 0)
   {
      b_stacks[pixelPos * maxLoops + rayIndex].sphereIndex = -1.0f;

      return;
   }

   b_stacks[pixelPos * maxLoops + rayIndex].sphereIndex = float(sphereNearIndex);	
   b_stacks[pixelPos * maxLoops + rayIndex].insideSphere = float(insideSphereNear);

   //

   // Calculate ray hit position ...
   ray = rayDirection * tNear;
   float4 hitPosition = rayPosition + float4(ray, 0.0);

   // ... and normal
   float3 hitDirection = normalize((hitPosition - b_spheres[sphereNearIndex].center).xyz);

   // If inside the sphere, reverse hit vector, as ray comes from inside.
   if (insideSphereNear)
   {
      hitDirection *= -1.0;
   }

   //

   // Biasing, to avoid artifacts.
   float3 biasedHitDirection = hitDirection * bias;
   float4 biasedPositiveHitPosition = hitPosition + float4(biasedHitDirection, 0.0);
   float4 biasedNegativeHitPosition = hitPosition - float4(biasedHitDirection, 0.0);

   //

   b_stacks[pixelPos * maxLoops + rayIndex].fresnel = fresnel(rayDirection, hitDirection, R0);

   int reflectionIndex = getReflectIndex(rayIndex);

   // Reflection ...
   if (b_spheres[sphereNearIndex].material.reflectivity > 0.0 && reflectionIndex < maxLoops)
   {
      float3 reflectionDirection = normalize(reflect(rayDirection, hitDirection));

      b_stacks[pixelPos * maxLoops + reflectionIndex].position = biasedPositiveHitPosition;
      b_stacks[pixelPos * maxLoops + reflectionIndex].direction = reflectionDirection;
      b_stacks[pixelPos * maxLoops + reflectionIndex].valid = 1.0;
   }

   int refractionIndex = getRefractIndex(rayIndex);

   // ... refraction.
   if (b_spheres[sphereNearIndex].material.alpha < 1.0 && refractionIndex < maxLoops)
   {
      // If inside, it is from glass to air.
      float eta = insideSphereNear ? 1.0 / Eta : Eta;

      float3 refractionDirection = normalize(refract(rayDirection, hitDirection, eta));

      b_stacks[pixelPos * maxLoops + refractionIndex].position = biasedNegativeHitPosition;
      b_stacks[pixelPos * maxLoops + refractionIndex].direction = refractionDirection;
      b_stacks[pixelPos * maxLoops + refractionIndex].valid = 1.0;
   }
   else
   {
      b_stacks[pixelPos * maxLoops + rayIndex].fresnel = 1.0;
   }

   b_stacks[pixelPos * maxLoops + rayIndex].hitPosition = hitPosition;
   b_stacks[pixelPos * maxLoops + rayIndex].hitDirection = hitDirection;

   b_stacks[pixelPos * maxLoops + rayIndex].biasedPositiveHitPosition = biasedPositiveHitPosition;
}

void shade(int pixelPos, int maxLoops, int rayIndex)
{
   int i, k;

   if (b_stacks[pixelPos * maxLoops + rayIndex].valid <= 0.0)
   {
      return;
   }

   int sphereNearIndex = int(b_stacks[pixelPos * maxLoops + rayIndex].sphereIndex);

   if (sphereNearIndex < 0)
   {
      b_stacks[pixelPos * maxLoops + rayIndex].color = float4(0.8, 0.8, 0.8, 1.0);

      return;   }	


   bool insideSphereNear = bool(b_stacks[pixelPos * maxLoops + rayIndex].insideSphere);

   float4 rayPosition = b_stacks[pixelPos * maxLoops + rayIndex].position;
   float3 rayDirection = b_stacks[pixelPos * maxLoops + rayIndex].direction;

   float4 reflectionColor = float4(0.0, 0.0, 0.0, 1.0);
   float4 refractionColor = float4(0.0, 0.0, 0.0, 1.0);

   int reflectionIndex = getReflectIndex(rayIndex);

   float fresnel = b_stacks[pixelPos * maxLoops + rayIndex].fresnel;

   // Reflection ...
   if (b_spheres[sphereNearIndex].material.reflectivity > 0.0 && reflectionIndex < maxLoops)
   {
      if (b_stacks[pixelPos * maxLoops + reflectionIndex].valid > 0.0)
      {
         reflectionColor = b_stacks[pixelPos * maxLoops + reflectionIndex].color;
      }
   }

   int refractionIndex = getRefractIndex(rayIndex);

   // ... refraction.
   if (b_spheres[sphereNearIndex].material.alpha < 1.0 && refractionIndex < maxLoops)
   {
      if (b_stacks[pixelPos * maxLoops + refractionIndex].valid > 0.0)
      {
         refractionColor = b_stacks[pixelPos * maxLoops + refractionIndex].color;
      }
   }

   //

   float3 eyeDirection = rayDirection * -1.0;

   float4 biasedPositiveHitPosition = b_stacks[pixelPos * maxLoops + rayIndex].biasedPositiveHitPosition;
   float3 hitDirection = b_stacks[pixelPos * maxLoops + rayIndex].hitDirection;

   float4 pixelColor = float4(0.0, 0.0, 0.0, 1.0);

   // Diffuse and specular color
   for (i = 0; i < NUM_LIGHTS; i++)
   {
      bool obstacle = false;

      float3 lightDirection = normalize((b_pointLights[i].position - b_stacks[pixelPos * maxLoops + rayIndex].hitPosition).xyz);
      float3 incidentLightDirection = lightDirection * -1.0;

      // Check for obstacles between current hit point surface and point light.
      for (k = 0; k < NUM_SPHERES; k++)
      {
         if (k == sphereNearIndex)
         {
            continue;
         }

         float t0 = INFINITY;
         float t1 = INFINITY;
         bool insideSphere = false;

         int numberIntersections = intersectRaySphere(t0, t1, insideSphere, biasedPositiveHitPosition, lightDirection, b_spheres[k].center, b_spheres[k].radius);

         if (numberIntersections > 0)
         {
            obstacle = true;

            break;
         }
      }

      // If no obstacle, illuminate hit point surface.
      if (!obstacle)
      {
         float diffuseIntensity = max(0.0, dot(hitDirection, lightDirection));

         if (diffuseIntensity > 0.0)
         {
            pixelColor += diffuseIntensity * b_spheres[sphereNearIndex].material.diffuseColor * b_pointLights[i].color;

            float3 specularReflection = normalize(reflect(incidentLightDirection, hitDirection));

            float eDotR = max(0.0, dot(eyeDirection, specularReflection));

            if (eDotR > 0.0 && !insideSphereNear)
            {
               float specularIntensity = pow(eDotR, b_spheres[sphereNearIndex].material.shininess);

               pixelColor += specularIntensity * b_spheres[sphereNearIndex].material.specularColor * b_pointLights[i].color;
            }
         }
      }
   }

   // Emissive color
   pixelColor += b_spheres[sphereNearIndex].material.emissiveColor;

   // Final color with reflection and refraction
   pixelColor = (1.0 - fresnel) * refractionColor * (1.0 - b_spheres[sphereNearIndex].material.alpha) + pixelColor * (1.0 - b_spheres[sphereNearIndex].material.reflectivity) * b_spheres[sphereNearIndex].material.alpha + fresnel * reflectionColor * b_spheres[sphereNearIndex].material.reflectivity;

   b_stacks[pixelPos * maxLoops + rayIndex].color = pixelColor;
}

void clear(int pixelPos, int maxLoops, int rayIndex)
{
   b_stacks[pixelPos * maxLoops + rayIndex].valid = 0.0;
}


struct PixelStruct
{
   Stack stacks[34];
   //float4 finalColor; // save it directly to b_textureOut
   bool finished;
};

RWBuffer<float4> b_textureOut;
RWStructuredBuffer<PixelStruct> b_ImageData;

[numthreads(32,32,1)]
void CS(int3 dtid : SV_DispatchThreadID)
{
   //gOut[dtid.x].v1 = gIn1[dtid.x].v1 + gIn2[dtid.x].v1;
   //gOut[dtid.x].v2 = gIn1[dtid.x].v2 + gIn2[dtid.x].v2;

   //-------------------------------------------------------------------------------------------

   //Stack tab1[65535];

   for (int i = 0; i < 1080*1920; ++i)
   {
      b_ImageData[0].finished = false;
   }

   //int2 dimension = imageSize(u_texture);
   int2 dimension = int2(640,480);

   //int2 storePos = int2(gl_GlobalInvocationID.xy);
   int2 storePos = int2(dtid.xy);

   int pixelPos = storePos.x + storePos.y * dimension.x;

   // Maximum possible traces
   int maxLoops = int(exp2(MAX_DEPTH) - 1.0);

   // Init ray stack 0 with initial direction and position.
   b_stacks[pixelPos * maxLoops + 0].position = b_positions[pixelPos];
   b_stacks[pixelPos * maxLoops + 0].direction = b_directions[pixelPos];
   b_stacks[pixelPos * maxLoops + 0].valid = 1.0;

   // Trace all possible rays initiated by the origin ray.
   for ( i = 0; i < maxLoops; i++)
   {
      trace(pixelPos, maxLoops, i);
   }

   // Loop from leaf nodes to root and calculate final color.
   for (i = maxLoops - 1; i >= 0; i--)
   {
      shade(pixelPos, maxLoops, i);
   }	

   // Clear for next frame.
   for (i = 0; i < maxLoops; i++)
   {
      clear(pixelPos, maxLoops, i);
   }

   // In index 0, the final color for this pixel is stored.
   //imageStore(u_texture, storePos, b_stacks[pixelPos * maxLoops + 0].color);
   //b_textureOut[pixelPos] = float4( b_directions[pixelPos].x, b_directions[pixelPos].y, b_directions[pixelPos].z, 1.0f);

	b_textureOut[pixelPos] = b_stacks[pixelPos*maxLoops+0].color;
	//b_textureOut[pixelPos] = float4(1.0f, 0.0f, 0.0f, 1.0f);

}

technique11 RayTrace
{
   pass P0
   {
      SetVertexShader( NULL );
      SetPixelShader( NULL );
      SetComputeShader( CompileShader( cs_5_0, CS() ) );
   }
}