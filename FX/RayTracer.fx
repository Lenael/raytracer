//struct Params
//{
//   int2 dimension;
//};
struct Material
{
   float3 f3EmissiveColor;
   float3 f3Color;
   //float3 f3DiffuseColor;
   //float fPadding;
   float fShininess;
   float fAlpha;
   float fReflectivity;
};

struct Sphere
{
   float3 f3Position;
   float fRadius;
   Material sMaterial;
};

RWBuffer<float4> b_TextureOut;
StructuredBuffer<Sphere> b_Spheres;

cbuffer Common
{
   uint2 cbDimension;
   float cbFov;
   float cbPi;
   //musi byc 4, bo cbuffer musi miec rozmiar bedacy potega 16
   float4 f4BackgroundColor;
   bool bGeometricCollision;
   float3 f3Offset;
   float fRefractiveIndex;
   float3 f3ByteOffset;
};

float3 CreatePrimaryRay(int2 pixelPosition)
{
   float invWidth = 1.0f / cbDimension.x;
   float invHeight = 1.0f / cbDimension.y;
   float aspectRatio = (float)cbDimension.x / (float)cbDimension.y;
   float angle = tan(cbPi * 0.5f * cbFov / 180.0f);
   float x2d = (2.0f * ((pixelPosition.x + 0.5f) * invWidth) - 1.0f) * angle * aspectRatio;
   float y2d = (1.0f - 2.0f * ((pixelPosition.y + 0.5f) * invHeight)) * angle;

   return normalize(float3(x2d, y2d, -1.0f));
}

//t0 i t1 -> DLUGOSCI wektorow punktow kolizji
bool RayIntersectSphere(float3 A_f3RayOrigin, float3 A_f3RayDirection, float3 A_f3SphereCenter, float A_fSphereRadius, out float t0, out float t1)
{
   float3 l = A_f3SphereCenter - A_f3RayOrigin;

   t0 = 0.0f;
   t1 = 0.0f;

   //tca - DLUGOSC 
   //iloczyn skalarny - DLUGOSC RZUTU WEKTORA A NA B 
   float tca = dot(l, A_f3RayDirection);

   // sfera znajduje sie za promieniem
   if (tca < 0)
      return false;

   l = dot(l, l);  // TODO: sprawdzic, czy to to samo
   //l = l*l;

   // dkwadrat to odleglosc od srodka sfery prostopadle do kierunku  raydir
   float d2 = l.x - tca * tca;

   float r2 = pow(A_fSphereRadius, 2);

   // wtedy ray jest poza sfera
   if (d2 > r2)
      return false;

   float thc = sqrt(r2 - d2);

   t0 = tca - thc;
   t1 = tca + thc;

   return true;
}

struct Node
{
   float3 f3Position;
   float3 f3Color;
      //wektory normalne!
   //float3 f3DiffuseDirection;
      // tutaj prawdopodobnie wystarczy tylko jeden wektor. Bo z dziecka (jednego promienia) zrobia sie trzy, wiec dla kazdego rodzaju promienia f3Direction bedzie kierunkiem promienia
   //float3 f3RayDirection;
   //float4 hitPosition;
   //float3 hitDirection;
   //float4 biasedPositiveHitPosition;
   //float valid;
   float fFresnel;
   uint uiSphereIndex;
   //float insideSphere;
   float3 f3DiffuseColor;
   //float3 f3RefractionColor
   //float3 f3ReflectionColor;
};

//struct Material
//{
//   float4 f4EmissiveColor;
//   float4 f4DiffuseColor;
//   float4 f4SpecularColor;
//   float fShininess;
//   float fAlpha;
//   float fReflectivity;
//};

//int GetParent(int A_numInput)
//{
//   return floor((float)A_numInput / 3.0f);
//}
//
////childnumber - 1, 2 or 3
//int GetChild(int A_numInput, int A_numChildNumber)
//{
//   return 3 * A_numInput + A_numChildNumber;
//}

// Indices of refraction
#define AIR_REFRACTION 1.0
#define GLASS_REFRACTION 1.06
#define THRESHOLD 0.00000000001f
#define FLOAT3_EQUALS(x, y) (abs((x) - (y)) < THRESHOLD)
#define FLOAT_EQUALS(x, y) (abs((x) - (y)) < THRESHOLD)

// Air to glass ratio of the indices of refraction (Eta)
#define AIR_GRASS_REFRACTION (AIR_REFRACTION / GLASS_REFRACTION)

// see http://en.wikipedia.org/wiki/Refractive_index Reflectivity
#define R0 (((AIR_REFRACTION - GLASS_REFRACTION) * (AIR_REFRACTION - GLASS_REFRACTION)) / ((AIR_REFRACTION + GLASS_REFRACTION) * (AIR_REFRACTION + GLASS_REFRACTION)))

#define GETCHILD(x, c) 3*(x)+(c)
#define GETPARENT(x) floor(((float)(x)-1.0f)/3.0f)

float ComputeFresnel(float3 A_f3IncidentRay, float3 A_f3Normal)
{
   // see http://en.wikipedia.org/wiki/Schlick%27s_approximation

   return R0 + (1.0 - R0) * pow(1.0 - dot(A_f3IncidentRay, A_f3Normal), 5.0);
}

//bool CompareFloat3(float3 A_f3First, float3 A_f4Second)
//{
//   ((A_f3First.x - A_f3Second.x) + (A_f3First.y - A_f3Second.y) + (A_f3First.z - A_f3Second.z)) > THRESHOLD;
//}

#define FLOAT_MAX 3.402823466e+38F
#define REFLECTION_CHILD 1
#define DIFFUSE_CHILD 2
#define REFRACTION_CHILD 3
#define NUM_NODES 121 //4 poziomy

bool FindCollisionObjectIndex(uint A_uiSpheresNumber, float3 A_f3RayOrigin, float3 A_f3RayDirection, out float A_fNearestObjectDistance, out uint A_uiIndex)
{
   float fInfinity = FLOAT_MAX;
   A_fNearestObjectDistance = fInfinity;
   A_uiIndex = A_uiSpheresNumber;
   bool bCollisionFound = false;

   for (uint j = 0; j < A_uiSpheresNumber; ++j)
   {
      float t0 = fInfinity, t1 = fInfinity;
      if (RayIntersectSphere(A_f3RayOrigin, A_f3RayDirection, b_Spheres[j].f3Position, b_Spheres[j].fRadius, t0, t1))
      {
         bCollisionFound = true;
         if (t0 < 0)
         {
            //we're inside sphere
            t0 = t1;
         }

         if (t0 < A_fNearestObjectDistance)
         {
            A_fNearestObjectDistance = t0;
            A_uiIndex = j;
         }
      }
   }
   return bCollisionFound;
}

[numthreads(32, 32, 1)]
//[numthreads(16, 16, 1)]
void CS(int3 dtid : SV_DispatchThreadID)
{
   // TODO: zrobic opcje wylaczania badania dyfuzyjnych - nie traceowac cienia przezroczystosci
   //int2 pixelPosition = int2(dtid.xy);
   int pixelNumber = dtid.x + dtid.y * cbDimension.x;

   //const int numNodes = 243; //3^5, a moze 2^5? w koncu diffuse nie musi sie odbic, wiec po co to... chyba, ze trzeba sprawdzic, czy zaslaniajacy obiekt nie jest przezroczysty
   uint numSpheres, stride;
   b_Spheres.GetDimensions(numSpheres, stride);
   //float3 f3Offset = float3(0.0f, 3.0f, 0.0f);

   //for (uint i = 0; i < numSpheres; ++i)
   //{
   //   b_Spheres[i].f3Position = b_Spheres[i].f3Position + float3(3.0f, 0.0f, 0.0f);
   //}

   float3 primaryRay = CreatePrimaryRay(dtid.xy);
   Node nodes[NUM_NODES];
   
   //temporary variables
   float fTemp1 = 0.0f;
   float fTemp2 = 0.0f;
   
   //set primary ray parameters ----------------------------------------------------------------------------------------
   nodes[0].f3Position = float3(0.0f, 0.0f, 0.0f);
   nodes[0].f3Color = float3(0.0f, 0.0f, 0.0f);

   //find first collision object
   //float fFirstNearestObjectDistance = FLOAT_MAX;
   //uint uiFirstSphereIndex = numSpheres;
   float fNearestObjectDistance = FLOAT_MAX;
   uint uiNextSphereIndex = numSpheres;
   float t0 = FLOAT_MAX, t1 = FLOAT_MAX;

   for (uint j = 0; j < numSpheres; ++j)
   {
      t0 = FLOAT_MAX, t1 = FLOAT_MAX;
      // --------------------------------- rayintersectsphere inline

      //fTemp1 = b_Spheres[j].f3Position - primaryRay;

      //t0 = 0.0f;
      //t1 = 0.0f;

      //fTemp2 = dot(fTemp1, primaryRay);

      //if (fTemp2 < 0)
      //   continue;

      //fTemp1 = dot(fTemp1, fTemp1);

      //float d2 = fTemp1.x - fTemp2 * fTemp2;
      //fTemp1 = pow(b_Spheres[j].fRadius, 2);

      //if (d2 > fTemp1)
      //   continue;

      //float thc = sqrt(fTemp1 - d2);

      //t0 = fTemp2 - thc;
      //t1 = fTemp2 + thc;

      // --------------------------------------------------------------

      if (RayIntersectSphere(nodes[0].f3Position, primaryRay, b_Spheres[j].f3Position + f3Offset, b_Spheres[j].fRadius, t0, t1))
      {
         if (t0 < 0)
         {
            //we're inside sphere  
            t0 = t1;
         }
         
         if (t0 < fNearestObjectDistance)
         {
            fNearestObjectDistance = t0;
            uiNextSphereIndex = j;
         }
      }
   }
   //////////////

   if (uiNextSphereIndex == numSpheres)
   {
      //nie znaleziono sfery kolizyjnej dla primary ray - zwroc dla piksela kolor tla



      b_TextureOut[pixelNumber] = f4BackgroundColor;
      return;
   }

   // znaleziono sfere kolizyjna dla primary ray
   float3 f3FirstPhit = nodes[0].f3Position + primaryRay * fNearestObjectDistance;
   nodes[1].uiSphereIndex = uiNextSphereIndex;
   nodes[2].uiSphereIndex = uiNextSphereIndex;
   nodes[3].uiSphereIndex = uiNextSphereIndex;
   nodes[1].f3Position = f3FirstPhit;
   nodes[2].f3Position = f3FirstPhit;
   nodes[3].f3Position = f3FirstPhit;

   //-------------------------------------------------------------------------------------------------------------------------
   
   for (int i = 1; i < NUM_NODES; ++i)
   {  
      //jesli dany node ma x koloru na -1.0f, to nie liczymy, bo juz liczenie sie skonczylo
      if (FLOAT3_EQUALS(nodes[i].f3Color.x, -1.0f))
      {
         nodes[i].f3Color = float3(0.0f, 0.0f, 0.0f);

         nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         continue;
      }

      // sprawdzamy, czy dany node jest Reflection rayem, Diffuse Rayem, czy Refraction rayem. Wybieramy i robimy obliczenia
      int iParentIndex = GETPARENT(i); 

      int iNumType = (((uint)i + 2) % 3)+1;

      nodes[i].f3Color = float3(0.0f, 0.0f, 0.0f);

      // 0. Tworzymy initray
      float3 f3Ray = nodes[i].f3Position - nodes[iParentIndex].f3Position;
      float3 f3NormalizedRay = normalize(f3Ray);  //tutaj cos to robi w ogole? chyba nie, bo juz jest znormalizowane

      //wektor normalny w miejsu kolizji ze sfer� = phit - spherepos
      float3 f3nphit = nodes[i].f3Position - b_Spheres[nodes[i].uiSphereIndex].f3Position + f3Offset;

      bool inside = false;

      if (dot(f3NormalizedRay, f3nphit) > 0.0f)
      {
         f3nphit = -f3nphit;
         inside = true;
      }


      if (iNumType == REFLECTION_CHILD)
      {
         // reflected color

         if (b_Spheres[nodes[i].uiSphereIndex].sMaterial.fReflectivity == 0.0f)
         {
            // USTAW FLAGE, ZEBY CHILDY NIE LICZYLY PROMIENI, BO AKTUALNA SFERA NIE ODBIJA! -------------------------------------------------------------------------
            nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            continue;
         }

         // w parencie mamy punkt kolizji i idxsphere kolizyjnej i w childzie punkt kolizji i idx sphery z ktora ta kolizjza jest
         // robimy f3InitRayDirection
         // Create reflection ray -> najbierw budujemy reflaction ray, bo w parencie mamy tylko info o Position i RayDirection -> z tego budujemy sobie reflection ray
         //   szukamy kolizji
         //   jesli brak kolizji -> kolor tla -> ustawiamy w child jakas flage, ktora pozniej sprawdzamy przed obliczeniami -> jesli jest ustawione skipujemy obliczenia i przechodzimy dalej (continue)
         //   jesli kolizja jest, pobieramy info o kolorze i ustawiamy w child hitPosition jako f3Position oraz f3refldir jako f3RayDirection oraz pobieramy index sfery i zapisujemy w odpowiednim childzie



         // 1. Tworzymy reflaction ray
         float fraydotnphit = dot(f3NormalizedRay, f3nphit);
         // tworzymy wspolczynnik fresnela
         nodes[i].fFresnel = lerp(pow(1.0f + fraydotnphit, 5.0f), 1.0f, fRefractiveIndex); ///////////
         //nodes[i].fFresnel = lerp(pow(1.0f + fraydotnphit, 3.0f), 1.0f, 0.4f); ///////////

         //nodes[i].fFresnel = 1.0f; ///////////
         //nodes[i].fFresnel = ComputeFresnel(f3NormalizedRay, f3nphit);


         // obliczamy kierunek odbicia - zmienne juz znormalizowane
         // reflection = raydir - 2 * (ray o normal) * normal
         float3 f3refldir = f3NormalizedRay - 2.0f * fraydotnphit * f3nphit;
         float3 f3normalizedRefldir = normalize(f3refldir);


         //nodes[GetChild(i, REFLECTION_CHILD)].f3Position = f3phit // produces warning X4714 - too many registers
         //nodes[GETCHILD(i, REFLECTION_CHILD)].f3Position = f3phit;



         //find closes sphere that intersect our ray
         fNearestObjectDistance = FLOAT_MAX;
         uiNextSphereIndex = numSpheres;
         
         for (uint j = 0; j < numSpheres; ++j)
         {
            t0 = FLOAT_MAX, t1 = FLOAT_MAX;
            if (RayIntersectSphere(nodes[i].f3Position, f3normalizedRefldir, b_Spheres[j].f3Position + f3Offset, b_Spheres[j].fRadius, t0, t1))
            {
               if (t0 < 0)
               {
                  //we're inside sphere
                  t0 = t1;
               }

               if (t0 < fNearestObjectDistance)
               {
                  fNearestObjectDistance = t0;
                  uiNextSphereIndex = j;
               }
            }
         }
         
         //jesli sfera nie istnieje, zwracamy kolor tla TODO: ---------------------- ustawic jakas informacje dla childow, zeby nie byly liczoneeeeeeeeeeeeeeeeee---------------
         if (uiNextSphereIndex == numSpheres)
         {
            nodes[i].f3Color.xyz = f4BackgroundColor.xyz * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color;
            nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         }
         else
         {
            //float ft = -f3raydotnphit.x;////////////


            // zapisz kolor tej sfery, phit i sphereidx w childzie
            nodes[i].f3Color = b_Spheres[uiNextSphereIndex].sMaterial.f3Color * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color;// *b_Spheres[uiNextSphereIndex].sMaterial.fReflectivity * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color;

            //obliczamy hit position i zapisujemy w odpowiednim childzie
            float3 f3nextPhit = nodes[i].f3Position + f3normalizedRefldir * fNearestObjectDistance;

            uint child = GETCHILD(i, REFLECTION_CHILD);

            if (child < NUM_NODES) ///////////////// - jeszcze tu lepiej zabezpieczyc
            {
               nodes[GETCHILD(i, REFLECTION_CHILD)].f3Position = f3nextPhit;
               nodes[GETCHILD(i, REFLECTION_CHILD)].uiSphereIndex = uiNextSphereIndex;
               nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Position = f3nextPhit;
               nodes[GETCHILD(i, DIFFUSE_CHILD)].uiSphereIndex = uiNextSphereIndex;
               nodes[GETCHILD(i, REFRACTION_CHILD)].f3Position = f3nextPhit;
               nodes[GETCHILD(i, REFRACTION_CHILD)].uiSphereIndex = uiNextSphereIndex;
            }
         }
      }
      else if (iNumType == DIFFUSE_CHILD)
      {
         // difuse color
         //   na razie robimy tak, ze zasloniete obiekty przez przezroczyste obiekty rzucaja tylko cien
         //   pozniej dodamy dodatkowy ray, ktory spowoduje, ze cien nie bedzie calkowity (bo przezroczyste obiekty takich nie rzucaja)

         // jesli kula jest reflection lub refraction, to nie obliczamy diffusion
         if (b_Spheres[nodes[i].uiSphereIndex].sMaterial.fReflectivity != 0.0f || b_Spheres[nodes[i].uiSphereIndex].sMaterial.fAlpha != 1.0f)
         {
            //nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            //nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            //nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);


            //uint rchild = GETCHILD(i, REFLECTION_CHILD);
            //uint dchild = GETCHILD(i, DIFFUSE_CHILD);
            //uint rechild = GETCHILD(i, REFRACTION_CHILD);
            nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            continue;
         }

         //if (b_Spheres[nodes[i].uiSphereIndex].sMaterial.fAlpha != 1.0f)
         //{
         //   // USTAW FLAGE, ZEBY CHILDY NIE LICZYLY PROMIENI, BO AKTUALNA SFERA NIE ODBIJA! -------------------------------------------------------------------------            
         //   nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         //   nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         //   nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         //   continue;
         //}


         for (uint j = 0; j < numSpheres; ++j)
         {
            // sprawdzamy, czy emituje swiatlo (szukamy sfer, ktore oswietlaja ten punkt)
            //  moze da sie wydajniej sprawdzic, czy wektor jest niezerowy
            if (b_Spheres[j].sMaterial.f3EmissiveColor.x > 0.0f || b_Spheres[j].sMaterial.f3EmissiveColor.y > 0.0f || b_Spheres[j].sMaterial.f3EmissiveColor.z > 0.0f)
            {
               //float3 f3Transmission = float3(1.0f, 1.0f, 1.0f);
               // obliczamy wektor w kierunku zrodla swiatla, zaczynajacy sie w punkcie dla ktorego liczymy kolor
               float3 f3InLightDirection = normalize(b_Spheres[j].f3Position + f3Offset - nodes[i].f3Position);

                  /////float fDiffuseIntensity = max(0.0f, dot(f3nphit, f3InLightDirection));

                  //nodes[i].f3Color += b_Spheres[j].sMaterial.f3EmissiveColor * max(0.0f, dot(f3nphit, f3InLightDirection)) * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color;

               float3 f3TransmissionTest = float3(1.0f, 1.0f, 1.0f);

               //sprawdzamy, czy na drodze promienia swiatla do naszego punktu jest jakis obiekt zaslaniajacy
               for (uint k = 0; k < numSpheres; ++k)
               {
                  //jesli to nie ta sama sfera
                  if (j != k)
                  {
                     t0 = FLOAT_MAX, t1 = FLOAT_MAX;
                     if (RayIntersectSphere(nodes[i].f3Position, f3InLightDirection, b_Spheres[k].f3Position + f3Offset, b_Spheres[k].fRadius, t0, t1))
                     {
                        f3TransmissionTest = float3(0.0f, 0.0f, 0.0f);
                     }
                  }
               }
               nodes[i].f3Color += b_Spheres[j].sMaterial.f3EmissiveColor * max(0.0f, dot(f3nphit, f3InLightDirection)) * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color * f3TransmissionTest;
            }
         }

         nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
      }
      else if (iNumType == REFRACTION_CHILD)
      {
         // transparent color
         if (b_Spheres[nodes[i].uiSphereIndex].sMaterial.fAlpha == 1.0f)
         {
            // USTAW FLAGE, ZEBY CHILDY NIE LICZYLY PROMIENI, BO AKTUALNA SFERA NIE ODBIJA! -------------------------------------------------------------------------
            nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            continue;
         }
         
         //float fraydotnphit = dot(f3NormalizedRay, f3nphit);
         //// tworzymy wspolczynnik fresnela
         //nodes[i].fFresnel = lerp(pow(1.0f + fraydotnphit, 3.0f), 1.0f, 0.4f); ///////////

         float fIor = 1.1f;
         //sprawdzamy, czy jestesmy w srodku, czy na zewnatrz
         float fE = (inside) ? fIor : 1.0f / fIor;

         float fCosi = -dot(f3nphit, f3NormalizedRay).x;
         float k = 1.0f - fE * fE * (1.0f - fCosi * fCosi);
         float3 f3RefrDir = normalize(f3Ray * fE + f3nphit * (fE *  fCosi - sqrt(k)));

         //XMStoreFloat3(&t1, xmphit - xmnhit * blad);
         //XMStoreFloat3(&t2, xmrefrdir);

         //xmrefraction = XMLoadFloat3(&Trace(t1, t2, A_vSpheres, A_iDepth + 1));

         for (uint j = 0; j < numSpheres; ++j)
         {
            t0 = FLOAT_MAX, t1 = FLOAT_MAX;
            if (RayIntersectSphere(nodes[0].f3Position, f3RefrDir, b_Spheres[j].f3Position + f3Offset, b_Spheres[j].fRadius, t0, t1))
            {
               if (t0 < 0)
               {
                  //we're inside sphere  
                  t0 = t1;
               }

               if (t0 < fNearestObjectDistance)
               {
                  fNearestObjectDistance = t0;
                  uiNextSphereIndex = j;
               }
            }
         }

         if (uiNextSphereIndex == numSpheres)
         {
            nodes[i].f3Color.xyz = f4BackgroundColor.xyz * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color *b_Spheres[nodes[i].uiSphereIndex].sMaterial.fAlpha; // tutaj nie ma byc przezroczystosci?
            nodes[GETCHILD(i, REFLECTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
            nodes[GETCHILD(i, REFRACTION_CHILD)].f3Color = float3(-1.0f, -1.0f, -1.0f);
         }
         else
         {
            // zapisz kolor tej sfery, phit i sphereidx w childzie
            //xmsurfaceColor = (xmreflection * fresnel + xmrefraction * (1.0f - fresnel) * sphere->m_fTransparency) * XMLoadFloat3(&(sphere->m_f3Color));
            nodes[i].f3Color = b_Spheres[uiNextSphereIndex].sMaterial.f3Color * b_Spheres[nodes[i].uiSphereIndex].sMaterial.fAlpha * b_Spheres[nodes[i].uiSphereIndex].sMaterial.f3Color;

            //obliczamy hit position i zapisujemy w odpowiednim childzie
            float3 f3nextPhit = nodes[i].f3Position + f3RefrDir * fNearestObjectDistance;

             uint child = GETCHILD(i, REFLECTION_CHILD);

            if (child < NUM_NODES) ///////////////// - jeszcze tu lepiej zabezpieczyc
            {
               nodes[GETCHILD(i, REFLECTION_CHILD)].f3Position = f3nextPhit;
               nodes[GETCHILD(i, REFLECTION_CHILD)].uiSphereIndex = uiNextSphereIndex;
               nodes[GETCHILD(i, DIFFUSE_CHILD)].f3Position = f3nextPhit;
               nodes[GETCHILD(i, DIFFUSE_CHILD)].uiSphereIndex = uiNextSphereIndex;
               nodes[GETCHILD(i, REFRACTION_CHILD)].f3Position = f3nextPhit;
               nodes[GETCHILD(i, REFRACTION_CHILD)].uiSphereIndex = uiNextSphereIndex;
            }
         }
      
      }
   }

   //calculate final color from color tree
   for (i = NUM_NODES-1; i > 0; i -= 3)
   {
      uint uiParentIndex = GETPARENT(i);
      //i-2 = reflection_child
      //i-1 = diffuse_child
      //i-0 = refraction_child

      //tu widzimy, ze refrakcja musi byc wczesniej przemnozona z sphere transparency
      //xmsurfaceColor = (xmreflection * fresnel + xmrefraction * (1.0f - fresnel) * sphere->m_fTransparency) * XMLoadFloat3(&(sphere->m_f3Color));

      //xmsurfaceColor += XMLoadFloat3(&(sphere->m_f3Color)) * xmtransmission * max(0.0f, XMVectorGetX(XMVector3Dot(xmnhit, xmlightDirection))) * XMLoadFloat3(&(A_vSpheres[i]->m_f3EmissionColor));



      //nodes[uiParentIndex].f3Color = nodes[uiParentIndex].f3Color * nodes[i - 2].f3Color;

      // working for diffuse color
      nodes[uiParentIndex].f3Color = (nodes[uiParentIndex].f3Color + nodes[i - 1].f3Color);// + nodes[i - 2].f3Color * nodes[i - 2].fFresnel;// *0.3f;// *nodes[i - 2].fFresnel;

      // working for reflection color
      //nodes[uiParentIndex].f3Color = nodes[uiParentIndex].f3Color + nodes[i - 2].f3Color * nodes[i - 2].fFresnel;

      //nodes[uiParentIndex].f3Color = nodes[uiParentIndex].f3Color + nodes[i - 2].f3Color * nodes[i - 2].fFresnel + nodes[i - 1].f3Color;

      //nodes[uiParentIndex].f3Color = nodes[uiParentIndex].f3Color + nodes[i - 2].f3Color * nodes[i - 2].fFresnel + nodes[i].f3Color * (1.0f - nodes[i - 2].fFresnel) + nodes[i - 1].f3Color;
   }

   

   //float t0, t1;
   //if ( RayIntersectSphere(float3(0.0f, 0.0f, 0.0f), primaryRay, float3(0.0f, 0.0f, -5.0f), 1.0f, t0, t1) )
   //   b_TextureOut[pixelNumber] = float4(1.0f, 0.0f, 0.0f, 1.0f);
   //else
   //   b_TextureOut[pixelNumber] = float4(1.0f, 1.0f, 1.0f, 1.0f);

   b_TextureOut[pixelNumber] = float4(nodes[0].f3Color.x, nodes[0].f3Color.y, nodes[0].f3Color.z, 1.0f);

   //uint numStructs, stride;
   //b_Spheres.GetDimensions(numStructs, stride);
   //b_TextureOut[pixelNumber] = float4(numStructs, stride, 0.0f, 0.0f);
}


technique11 RayTrace
{
   pass P0
   {
      SetVertexShader( NULL );
      SetPixelShader( NULL );
      SetComputeShader( CompileShader( cs_5_0, CS() ) );
   }
}