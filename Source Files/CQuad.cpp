#include "../Header Files/CQuad.h"
#include "../Header Files/Common.h"
#include "../Header Files/CVertex.h"
#include "../Header Files/CEffects.h"
#include "../Header Files/CRenderStates.h"
#include "../Header Files/DDSTextureLoader.h"

CQuad::CQuad()
{
}

CQuad::~CQuad()
{
   RELEASECOM( m_pVB );
   RELEASECOM( m_pIB );
   RELEASECOM( m_TextureSRV );
   //RELEASECOM( m_BrickNormalTextureSRV );
}

CQuad::CQuad( CSystem* A_pSystem )
{
   m_pSystem = A_pSystem;

   XMStoreFloat4x4( &m_mWorld, XMMatrixIdentity() );
}

void CQuad::Init()
{
   Init( 10.0f, 10.0f );
}

void CQuad::Init( float A_fWidth, float A_fHeight )
{
   m_fWidth    = A_fWidth;
   m_fHeight   = A_fHeight;
   m_fDepth    = 0.0f;

   m_vVertices.clear();
	m_vIndices.clear();

	// Create the vertices.

	Vertex v[4];

	float w = 0.5f*m_fWidth;
	float h = 0.5f*m_fHeight;
	float d = 0.5f*m_fDepth;
    
	// Fill in the quad data
	v[0] = Vertex( -w, -h, -d, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
	v[1] = Vertex( -w, +h, -d, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	v[2] = Vertex( +w, +h, -d, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	v[3] = Vertex( +w, -h, -d, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f);

	m_vVertices.assign(&v[0], &v[4]);
 
	// Create the indices.

	UINT i[6];

	// Fill in the quad index data
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	m_vIndices.assign(&i[0], &i[6]);

   // przepisac z Vertex do posnormaltextan
   // TODO: czemu nie od razu do posnormaltextan? sprobowac czy zadziala pozniej 
   std::vector<::Vertex::PosNormalTexTan> vertices(m_vVertices.size());

	UINT k = 0;
	for(size_t i = 0; i < m_vVertices.size(); ++i, ++k)
	{
		vertices[k].Pos      = m_vVertices[i].Position;
		vertices[k].Normal   = m_vVertices[i].Normal;
		vertices[k].Tex      = m_vVertices[i].TexC;
		vertices[k].TangentU = m_vVertices[i].TangentU;
	}

   // Build Vertex buffer
   D3D11_BUFFER_DESC vbd;
   vbd.Usage = D3D11_USAGE_IMMUTABLE;
   vbd.ByteWidth = sizeof(::Vertex::PosNormalTexTan) * m_vVertices.size();
   vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
   vbd.CPUAccessFlags = 0;
   vbd.MiscFlags = 0;
   D3D11_SUBRESOURCE_DATA vinitData;
   vinitData.pSysMem = &vertices[0];
   m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&vbd, &vinitData, &m_pVB);

   // Build Index buffer
   D3D11_BUFFER_DESC ibd;
   ibd.Usage = D3D11_USAGE_IMMUTABLE;
   ibd.ByteWidth = sizeof(UINT) * m_vIndices.size();
   ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
   ibd.CPUAccessFlags = 0;
   ibd.MiscFlags = 0;
   D3D11_SUBRESOURCE_DATA iinitData;
   iinitData.pSysMem = &m_vIndices[0];
   m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&ibd, &iinitData, &m_pIB);

   // Set material
	m_Material.Ambient  = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	m_Material.Diffuse  = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_Material.Specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_Material.Reflect  = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

   // Load textures
   //CreateDDSTextureFromFile( m_pSystem->m_pDirectx->m_pD3DDevice, L"Textures/bricks.dds", nullptr, &m_TextureSRV );
}

void CQuad::SetTextureSRV( ID3D11ShaderResourceView* A_TextureSRV )
{
   //RELEASECOM( m_TextureSRV );
   m_TextureSRV = A_TextureSRV;
}

void CQuad::Render()
{
      XMMATRIX view     = m_pSystem->m_pCamera->View();
      XMMATRIX proj     = m_pSystem->m_pCamera->Proj();
      XMMATRIX viewProj = m_pSystem->m_pCamera->ViewProj();

      ID3DX11EffectTechnique* activeTech       = CEffects::BasicFX->Light0TexTech;
      
      m_pSystem->m_pDirectx->m_pD3DContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

      XMMATRIX world;
      XMMATRIX worldInvTranspose;
      XMMATRIX worldViewProj;

      UINT stride = sizeof(::Vertex::PosNormalTexTan);
      UINT offset = 0;
      
      m_pSystem->m_pDirectx->m_pD3DContext->IASetInputLayout(InputLayouts::PosNormalTexTan);
      m_pSystem->m_pDirectx->m_pD3DContext->IASetVertexBuffers(0, 1, &m_pVB, &stride, &offset);
      m_pSystem->m_pDirectx->m_pD3DContext->IASetIndexBuffer(m_pIB, DXGI_FORMAT_R32_UINT, 0);
	
      D3DX11_TECHNIQUE_DESC techDesc;
      activeTech->GetDesc( &techDesc );
      for(UINT p = 0; p < techDesc.Passes; ++p)
      {
         // Draw quad
         world = XMLoadFloat4x4(&m_mWorld);

         world.r[3] = XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f );
         XMVECTOR det = XMMatrixDeterminant( world );
         
         world = XMLoadFloat4x4(&m_mWorld);
         worldInvTranspose = XMMatrixTranspose( XMMatrixInverse( &det, world ) );
         worldViewProj = world*view*proj;

	      CEffects::BasicFX->SetWorld(world);
	      CEffects::BasicFX->SetWorldInvTranspose(worldInvTranspose);
	      CEffects::BasicFX->SetWorldViewProj(worldViewProj);
         CEffects::BasicFX->SetTexTransform(XMMatrixScaling(1.0f, 1.0f, 1.0f));
	      CEffects::BasicFX->SetMaterial(m_Material);
	      CEffects::BasicFX->SetDiffuseMap(m_TextureSRV);

         activeTech->GetPassByIndex(p)->Apply(0, m_pSystem->m_pDirectx->m_pD3DContext);
         m_pSystem->m_pDirectx->m_pD3DContext->DrawIndexed(m_vIndices.size(), 0, 0);
      }
}

std::vector<CQuad::Vertex> CQuad::GetVertices() const
{
   return m_vVertices;
}

void CQuad::SetPosition( CXMMATRIX M )
{
   XMStoreFloat4x4( &m_mWorld, M );
}

std::vector<UINT> CQuad::GetIndices() const
{
   return m_vIndices;
}