#include "../Header Files/CGPURaytracer.h"
#include "../Header Files/Common.h"
#include "../Header Files/CEffects.h"

CGPURaytracer::CGPURaytracer()
{
   m_iWidth       = 640; //TODO: wziac to z CSystem
   m_iHeight      = 480;


   m_pOutputTextureBuffer        = NULL;
   m_pOutputTextureDebugBuffer   = NULL;
   m_pTexture        = NULL;
   m_pTextureUAV     = NULL;
   m_pTextureSRV     = NULL;
   m_pSpheres        = NULL;
   m_pCommon         = NULL;
   m_f3Offset = XMFLOAT3(0.0f, 0.0f, 0.0f);
}

CGPURaytracer::~CGPURaytracer()
{
   RELEASECOM(m_pTextureUAV);
   RELEASECOM(m_pTextureSRV);
   RELEASECOM(m_pSpheres);
}

CGPURaytracer::CGPURaytracer(CSystem* A_pSystem)
   : CGPURaytracer()
{
   m_pSystem = A_pSystem;
   m_pQuad = new CQuad(m_pSystem);
}


void  CGPURaytracer::GenerateComplexScene(Sphere A_tabSphere[], int A_iSphereNum, float A_fRadius, float A_fOffsetX, float A_fOffsetY, float A_fOffsetZ)
{
   float x = 0, y = 0, z = 0;

   x += A_fOffsetX;
   y += A_fOffsetY;
   z += A_fOffsetZ;

   //draw first sphere

   Sphere sphere;
   sphere.f3Center = XMFLOAT3(x, y, z);
   sphere.fRadius = 1.0f;
   sphere.sMaterial.f3DiffuseColor = XMFLOAT3(1.0f, 0.0f, 0.0f);
   sphere.sMaterial.f3EmissiveColor = XMFLOAT3(0.0f, 0.0f, 0.0f);
   sphere.sMaterial.fAlpha = 0.5f;
   sphere.sMaterial.fReflectivity = 1.0f;
   sphere.sMaterial.fShininess = 0.0f;

   A_tabSphere[0] = sphere;
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(x, y, z), A_fRadius, XMFLOAT3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

   int sphereCnt = 1;
   int level = 1;
   while (sphereCnt < A_iSphereNum)
   {
      //count sphere number on this level
      float r = A_fRadius * level; //radius of circle with spheres on this level
      float twopir = 2 * XM_PI*r;
      float floorLevelSPhereNum = floorf(twopir / (2.0f*A_fRadius));
      int iLevelSphereNum = (int)floorLevelSPhereNum;

      float angleStep = 2.0f*(float)XM_PI / floorLevelSPhereNum;

      for (int i = 0; i<iLevelSphereNum; ++i)
      {
         x = r * cosf(angleStep*i) + A_fOffsetX;
         y = r * sinf(angleStep*i) + A_fOffsetY;
         z = A_fOffsetZ + level * r*0.2f;


         Sphere sphere;
         sphere.f3Center = XMFLOAT3(x, y, z);
         sphere.fRadius = 1.0f;
         sphere.sMaterial.f3EmissiveColor = XMFLOAT3(0.0f, 0.0f, 0.0f);
         sphere.sMaterial.fAlpha = 0.5f;
         sphere.sMaterial.fReflectivity = 1.0f;
         sphere.sMaterial.fShininess = 0.0f;

         //save sphere
         if (sphereCnt % 3 == 0)
            sphere.sMaterial.f3DiffuseColor = XMFLOAT3(1.0f, 0.0f, 0.0f);
         else if (sphereCnt % 3 == 1)
            sphere.sMaterial.f3DiffuseColor = XMFLOAT3(0.0f, 1.0f, 0.0f);
         else if (sphereCnt % 3 == 2)
            sphere.sMaterial.f3DiffuseColor = XMFLOAT3(0.0f, 0.0f, 1.0f);

         A_tabSphere[sphereCnt] = sphere;

         ++sphereCnt;
         if (sphereCnt == A_iSphereNum)
            break;
      }
      ++level;
   }
}

//CGPURaytracer::Material::Material(XMFLOAT3 A_f3EmissiveColor, XMFLOAT3 A_f3DiffuseColor, float A_fShininess, float A_fAlpha, float A_fReflectivity)
//{
//   f3EmissiveColor = A_f3EmissiveColor;
//   f3DiffuseColor = A_f3DiffuseColor;
//   fShininess = A_fShininess;
//   fAlpha = A_fAlpha;
//   fReflectivity = A_fReflectivity;
//}

void CGPURaytracer::SetupScene()
{
   if (m_pSystem->m_numSceneNumber == 1)
   {
      //m_numSpheresNumber = 4;
      m_vCSpheres.clear();
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, 20.0f, 0.0f), 3.0f, Material(XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 1.0f, 0.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, -10003.0f, -20.0f), 10000.0f, Material( XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.8f, 0.0f), 0.0f, 1.0f, 0.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(-2.3f, 0.0f, -20.0f), 2.0f, Material( XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.90f, 0.50f, 0.20f), 0.0f, 1.0f, 0.0f )));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(2.3f, 0.0f, -20.0f), 2.0f, Material( XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.20f, 0.50f, 0.90f), 0.0f, 1.0f, 0.0f )));
   }
   else if (m_pSystem->m_numSceneNumber == 2)
   {
      m_vCSpheres.clear();
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, Material( XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f )));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000.0f, Material( XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 1.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(-6.0f, 2.0f, -40.0f), 4.0f, Material( XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.9f, 0.9f, 0.4f), 0.0f, 0.0f, 1.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(5.0f, 0.0f, -35.0f), 3.0f, Material( XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.35f, 0.8f, 0.3f), 0.0f, 0.0f, 1.0f)));
   }
   else if (m_pSystem->m_numSceneNumber == 3)
   {
      m_vCSpheres.clear();
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, Material(XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000.0f, Material(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 1.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(0.0f, 0.0f, -20.0f), 4.0f, Material(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.6f, 0.6f, 1.0f), 0.0f, 0.8f, 1.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(5.0f, 0.0f, -25.0f), 3.0f, Material(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.35f, 0.8f, 0.3f), 0.0f, 0.0f, 1.0f)));
      m_vCSpheres.push_back(Sphere(XMFLOAT3(-4.0f, 0.0f, -25.0f), 3.0f, Material(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.35f, 0.9f, 0.8f), 0.0f, 0.0f, 1.0f)));
   }
   else if (m_pSystem->m_numSceneNumber == 4)
   {
   }
   else if (m_pSystem->m_numSceneNumber == 5)
   {

   }
   else if (m_pSystem->m_numSceneNumber == 6)
   {

   }



   if (m_pSpheres != NULL)
      m_pSpheres->Release();
   // Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
   D3D11_BUFFER_DESC inputDescSpheres;
   inputDescSpheres.Usage = D3D11_USAGE_DEFAULT;
   //inputDescSpheres.ByteWidth = sizeof(Sphere) * NUM_SPHERES;
   inputDescSpheres.ByteWidth = m_vCSpheres.size()*sizeof(Sphere);
   inputDescSpheres.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   inputDescSpheres.CPUAccessFlags = 0;
   inputDescSpheres.StructureByteStride = sizeof(Sphere);
   inputDescSpheres.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   D3D11_SUBRESOURCE_DATA vinitDataSpheres;
   //vinitDataSpheres.pSysMem = &spheres[0];
   vinitDataSpheres.pSysMem = &m_vCSpheres[0];

   ID3D11Buffer* bufferSpheres = 0;
   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescSpheres, &vinitDataSpheres, &bufferSpheres));

   D3D11_SHADER_RESOURCE_VIEW_DESC srvDescSpheres;
   srvDescSpheres.Format = DXGI_FORMAT_UNKNOWN;
   srvDescSpheres.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   srvDescSpheres.BufferEx.FirstElement = 0;
   srvDescSpheres.BufferEx.Flags = 0;
   //srvDescSpheres.BufferEx.NumElements = NUM_SPHERES;
   srvDescSpheres.BufferEx.NumElements = m_vCSpheres.size();

   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferSpheres, &srvDescSpheres, &m_pSpheres));

   RELEASECOM(bufferSpheres);
}

void CGPURaytracer::Init()
{
   // Supply the vertex shader constant data.
   Common VsConstData;
   VsConstData.dimension.x = m_iWidth;
   VsConstData.dimension.y = m_iHeight;
   VsConstData.fov = 30;  //TODO: get it from csystem class, or higher
   VsConstData.pi = XM_PI;
   VsConstData.f4BackgroundColor = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);
   VsConstData.bGeometricCollision = true;
   VsConstData.f3Offset = m_f3Offset;
   VsConstData.fRefractiveIndex = m_pSystem->m_fRefractiveIndex;

   // Fill in a buffer description.
   D3D11_BUFFER_DESC cbDesc;
   cbDesc.ByteWidth = sizeof(VsConstData);
   cbDesc.Usage = D3D11_USAGE_DYNAMIC;
   cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
   cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
   cbDesc.MiscFlags = 0;
   cbDesc.StructureByteStride = 0;

   // Fill in the subresource data.
   D3D11_SUBRESOURCE_DATA InitData;
   InitData.pSysMem = &VsConstData;
   InitData.SysMemPitch = 0;
   InitData.SysMemSlicePitch = 0;

   // Create the buffer.
   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&cbDesc, &InitData, &m_pCommon));

   // create structured buffer for spheres ------------------------------------------------------------------------------------------------
//#define NUM_SPHERES 4
//
//   Sphere spheres[NUM_SPHERES] = {
//      //test wydajnosci
//      //{ XMFLOAT3(0.0f, 0.0f, -100.0f), 2.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), 0.0f, 0.8f, 1.0f } },
//      //{ XMFLOAT3(0.0f, 0.0f, -20.0f), 2.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), 0.0f, 0.8f, 1.0f } },
//      //{ XMFLOAT3(0.0f, 0.0f, -20.0f), 2.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), 0.0f, 0.8f, 1.0f } },
//      //{ XMFLOAT3(0.0f, 0.0f, -20.0f), 2.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f), 0.0f, 0.8f, 1.0f } }
//
//      ////scena z dyfuzj�
//      //{ XMFLOAT3(0.0f, 20.0f, 0.0f), 3.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f } },
//      //{ XMFLOAT3(0.0f, -10003.0f, -20.0f), 10000.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.8f, 0.0f), 0.0f, 0.0f, 0.0f } },
//      //{ XMFLOAT3(-2.3f, 0.0f, -20.0f), 2.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.90f, 0.50f, 0.20f), 0.0f, 0.0f, 0.0f } },
//      //{ XMFLOAT3(2.3f, 0.0f, -20.0f), 2.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.20f, 0.50f, 0.90f), 0.0f, 0.0f, 0.0f } }
//
//      ////scena z odbiciem - dyfuzja+reflection
//      //{ XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f } },
//      //{ XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 1.0f } },
//      //{ XMFLOAT3(-6.0f, 2.0f, -40.0f), 4.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.9f, 0.9f, 0.4f), 0.0f, 0.0f, 1.0f } },
//      //{ XMFLOAT3(5.0f, 0.0f, -35.0f), 3.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.35f, 0.8f, 0.3f), 0.0f, 0.0f, 1.0f } }
//      //const XMFLOAT3 &A_f3Position, const float &A_fRadius, const XMFLOAT3 &A_f3Color, fReflection, const float &A_fTransparency, const XMFLOAT3 &A_f3EmissionColor)
//
//      //scena z refrakcja - dyfuzja+refrakcja
//      //{ XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f } },
//      //{ XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 1.0f } },
//      //{ XMFLOAT3(0.0f, 0.0f, -20.0f), 4.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.6f, 0.6f, 1.0f), 0.0f, 0.8f, 1.0f } },
//      //{ XMFLOAT3(5.0f, 0.0f, -25.0f), 3.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.35f, 0.8f, 0.3f), 0.0f, 0.0f, 1.0f } },
//      //{ XMFLOAT3(-4.0f, 0.0f, -25.0f), 3.0f, { XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.35f, 0.9f, 0.8f), 0.0f, 0.0f, 1.0f } }
//   };

   SetupScene();
   //GenerateComplexScene(spheres, NUM_SPHERES, 1, 0, 0, -50);

   //// Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
   //D3D11_BUFFER_DESC inputDescSpheres;
   //inputDescSpheres.Usage = D3D11_USAGE_DEFAULT;
   ////inputDescSpheres.ByteWidth = sizeof(Sphere) * NUM_SPHERES;
   //inputDescSpheres.ByteWidth = m_vCSpheres.size()*sizeof(Sphere);
   //inputDescSpheres.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   //inputDescSpheres.CPUAccessFlags = 0;
   //inputDescSpheres.StructureByteStride = sizeof(Sphere);
   //inputDescSpheres.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   //D3D11_SUBRESOURCE_DATA vinitDataSpheres;
   ////vinitDataSpheres.pSysMem = &spheres[0];
   //vinitDataSpheres.pSysMem = &m_vCSpheres[0];

   //ID3D11Buffer* bufferSpheres = 0;
   //HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescSpheres, &vinitDataSpheres, &bufferSpheres));

   //D3D11_SHADER_RESOURCE_VIEW_DESC srvDescSpheres;
   //srvDescSpheres.Format = DXGI_FORMAT_UNKNOWN;
   //srvDescSpheres.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   //srvDescSpheres.BufferEx.FirstElement = 0;
   //srvDescSpheres.BufferEx.Flags = 0;
   //srvDescSpheres.BufferEx.NumElements = m_vCSpheres.size();
   /////srvDescSpheres.BufferEx.NumElements = NUM_SPHERES;

   //HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferSpheres, &srvDescSpheres, &m_pSpheres));

   //RELEASECOM(bufferSpheres);

   // create texture 2d and pass it to the quad -------------------------------------------------------------------------------------------
   m_pQuad->Init( 11.6f, 8.3f );

   int quadWidth = 640;
   int quadHeight = 480;

   D3D11_TEXTURE2D_DESC descQuad;
   descQuad.Width = quadWidth;
   descQuad.Height = quadHeight;
   descQuad.MipLevels = 1;
   descQuad.ArraySize = 1;
   descQuad.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
   descQuad.SampleDesc.Count = 1;
   descQuad.SampleDesc.Quality = 0;
   descQuad.Usage = D3D11_USAGE_DYNAMIC;
   descQuad.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   descQuad.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
   descQuad.MiscFlags = 0;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateTexture2D( &descQuad, NULL, &m_pTexture ) );

   D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = descQuad.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	viewDesc.Texture2DArray.MostDetailedMip = 0;
	viewDesc.Texture2DArray.MipLevels = descQuad.MipLevels;
	viewDesc.Texture2DArray.FirstArraySlice = 0;
	viewDesc.Texture2DArray.ArraySize = descQuad.ArraySize;

	HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(m_pTexture, &viewDesc, &m_pTextureSRV) );

   m_pQuad->SetTextureSRV( m_pTextureSRV );

   // ----------------------------- output texture -------------------

   // Create a read-write buffer the compute shader can write to (D3D11_BIND_UNORDERED_ACCESS).
   D3D11_BUFFER_DESC outputDescTexture;
   outputDescTexture.Usage = D3D11_USAGE_DEFAULT;
   outputDescTexture.ByteWidth = sizeof(XMFLOAT4) * GetNumPixels();
   outputDescTexture.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
   outputDescTexture.CPUAccessFlags = 0;
   outputDescTexture.StructureByteStride = sizeof(XMFLOAT4);
   outputDescTexture.MiscFlags = 0;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDescTexture, 0, &m_pOutputTextureBuffer) );

   // Create a system memory version of the buffer to read the results back from.
   outputDescTexture.Usage = D3D11_USAGE_STAGING;
   outputDescTexture.BindFlags = 0;
   outputDescTexture.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDescTexture, 0, &m_pOutputTextureDebugBuffer) );

   D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescTexture;
   uavDescTexture.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
   uavDescTexture.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
   uavDescTexture.Buffer.FirstElement = 0;
   uavDescTexture.Buffer.Flags = 0;
   uavDescTexture.Buffer.NumElements = GetNumPixels();

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateUnorderedAccessView(m_pOutputTextureBuffer, &uavDescTexture, &m_pTextureUAV) );
}

void CGPURaytracer::Render()
{
   SetupScene();


   XMFLOAT3 f3Offset = XMFLOAT3(0.0f, 0.0f, 0.0f);

   if (GetAsyncKeyState('A') & 0x8000)
   {
      f3Offset = XMFLOAT3(0.25f, 0.0f, 0.0f);
   }
   if (GetAsyncKeyState('D') & 0x8000)
   {
      f3Offset = XMFLOAT3(-0.25f, 0.0f, 0.0f);
   }
   if (GetAsyncKeyState('W') & 0x8000)
   {
      f3Offset = XMFLOAT3(0.0f, 0.0f, 0.25f);
   }
   if (GetAsyncKeyState('S') & 0x8000)
   {
      f3Offset = XMFLOAT3(0.0f, 0.0f, -0.25f);
   }
   if (GetAsyncKeyState('Q') & 0x8000)
   {
      f3Offset = XMFLOAT3(0.0f, 0.25f, 0.0f);
   }
   if (GetAsyncKeyState('E') & 0x8000)
   {
      f3Offset = XMFLOAT3(0.0f, -0.25f, 0.0f);
   }

   m_f3Offset.x += f3Offset.x;
   m_f3Offset.y += f3Offset.y;
   m_f3Offset.z += f3Offset.z;

   // Supply the vertex shader constant data.
   Common VsConstData;
   VsConstData.dimension.x = m_iWidth;
   VsConstData.dimension.y = m_iHeight;
   VsConstData.fov = 30;  //TODO: get it from csystem class, or higher
   VsConstData.pi = XM_PI;
   VsConstData.f4BackgroundColor = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);
   VsConstData.bGeometricCollision = m_pSystem->m_bGeometricCollision;
   VsConstData.f3Offset = m_f3Offset;
   VsConstData.fRefractiveIndex = m_pSystem->m_fRefractiveIndex;

   // Fill in a buffer description.
   D3D11_BUFFER_DESC cbDesc;
   cbDesc.ByteWidth = sizeof(VsConstData);
   cbDesc.Usage = D3D11_USAGE_DYNAMIC;
   cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
   cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
   cbDesc.MiscFlags = 0;
   cbDesc.StructureByteStride = 0;

   // Fill in the subresource data.
   D3D11_SUBRESOURCE_DATA InitData;
   InitData.pSysMem = &VsConstData;
   InitData.SysMemPitch = 0;
   InitData.SysMemSlicePitch = 0;

   // Create the buffer.

   m_pCommon->Release();
   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&cbDesc, &InitData, &m_pCommon));

   D3DX11_TECHNIQUE_DESC techDesc;

   CEffects::RayTracerFX->SetTextureOut(m_pTextureUAV);
   CEffects::RayTracerFX->SetSpheres(m_pSpheres);

	CEffects::RayTracerFX->RayTracerTech->GetDesc( &techDesc );
	for(UINT p = 0; p < techDesc.Passes; ++p)
	{
      ID3DX11EffectPass* pass = CEffects::RayTracerFX->RayTracerTech->GetPassByIndex(p);
      pass->Apply(0, m_pSystem->m_pDirectx->m_pD3DContext);
      m_pSystem->m_pDirectx->m_pD3DContext->CSSetConstantBuffers(0, 1, &m_pCommon);

      m_pSystem->m_pDirectx->m_pD3DContext->Dispatch(m_iWidth / 32, m_iHeight / 32, 1);
      //m_pSystem->m_pDirectx->m_pD3DContext->Dispatch(m_iWidth / 16, m_iHeight / 16, 1);
	}

	// Unbind the input textures from the CS for good housekeeping.
	//ID3D11ShaderResourceView* nullSRV[1] = { 0 };
	//m_pSystem->m_pDirectx->m_pD3DContext->CSSetShaderResources( 0, 1, nullSRV );

	// Unbind output from compute shader (we are going to use this output as an input in the next pass, 
	// and a resource cannot be both an output and input at the same time.
	//ID3D11UnorderedAccessView* nullUAV[1] = { 0 };
	//m_pSystem->m_pDirectx->m_pD3DContext->CSSetUnorderedAccessViews( 0, 1, nullUAV, 0 );

	// Disable compute shader.
	//m_pSystem->m_pDirectx->m_pD3DContext->CSSetShader(0, 0, 0);

	// Copy the output buffer to system memory.
   m_pSystem->m_pDirectx->m_pD3DContext->CopyResource(m_pOutputTextureDebugBuffer, m_pOutputTextureBuffer);

	// Map the data for reading.
	D3D11_MAPPED_SUBRESOURCE mappedData; 
   HR( m_pSystem->m_pDirectx->m_pD3DContext->Map(m_pOutputTextureDebugBuffer, 0, D3D11_MAP_READ, 0, &mappedData) );

	//XMFLOAT4* dataView = reinterpret_cast<XMFLOAT4*>(mappedData.pData);
 //  FILE* pFile;
 //  fopen_s(&pFile, "D:/file.raw", "wb");
 //  fwrite(dataView, sizeof(XMFLOAT4), GetNumPixels(), pFile);
 //  fclose(pFile);

   m_pSystem->m_pDirectx->m_pD3DContext->Unmap(m_pOutputTextureDebugBuffer, 0);

   //map unmap texture data
   D3D11_MAPPED_SUBRESOURCE mappedResource;
   HR(m_pSystem->m_pDirectx->m_pD3DContext->Map(m_pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

   memcpy( mappedResource.pData, mappedData.pData, sizeof(float)*4*GetNumPixels() );

   m_pSystem->m_pDirectx->m_pD3DContext->Unmap(m_pTexture, 0);

   m_pQuad->Render();
}

const int CGPURaytracer::GetNumPixels() const
{
   return m_iWidth*m_iHeight;
}