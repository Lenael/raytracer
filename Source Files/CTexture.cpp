#include <windows.h>
#include "../Header Files/CTexture.h"
#include "../Header Files/DDSTextureLoader.h"
#include "../Header Files/Common.h"

CTexture::CTexture()
{
}

CTexture::CTexture( CSystem* A_pSystem )
{
   m_pSystem = A_pSystem;
}

ID3D11ShaderResourceView* CTexture::CreateTexture2DArraySRV( ID3D11Device* device, ID3D11DeviceContext* context,
		std::vector<std::string>& filenames)
{
	//
	// Load the texture elements individually from file.  These textures
	// won't be used by the GPU (0 bind flags), they are just used to 
	// load the image data from file.  We use the STAGING usage so the
	// CPU can read the resource.
	//

	UINT size = filenames.size();
   HRESULT hRes = E_FAIL;

	std::vector<ID3D11Texture2D*> srcTex(size);
	for(UINT i = 0; i < size; ++i)
	{
         
      const WCHAR *pwcsName;
      int nChars = MultiByteToWideChar(CP_ACP, 0, filenames[i].c_str(), -1, NULL, 0);

      pwcsName = new WCHAR[nChars];
      MultiByteToWideChar(CP_ACP, 0, filenames[i].c_str(), -1, (LPWSTR)pwcsName, nChars);

      //hRes = CreateDDSTextureFromFile( device, pwcsName, (ID3D11Resource**)&srcTex[i], 0 );
      hRes = CreateDDSTextureFromFileEx( device, pwcsName, 0, D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ, 0,false, (ID3D11Resource**)&srcTex[i], 0 );

	}

	D3D11_TEXTURE2D_DESC texElementDesc;
	srcTex[0]->GetDesc(&texElementDesc);

	D3D11_TEXTURE2D_DESC texArrayDesc;
	texArrayDesc.Width              = texElementDesc.Width;
	texArrayDesc.Height             = texElementDesc.Height;
	texArrayDesc.MipLevels          = texElementDesc.MipLevels;
	texArrayDesc.ArraySize          = size;
	texArrayDesc.Format             = texElementDesc.Format;
	texArrayDesc.SampleDesc.Count   = 1;
	texArrayDesc.SampleDesc.Quality = 0;
	texArrayDesc.Usage              = D3D11_USAGE_DEFAULT;
	texArrayDesc.BindFlags          = D3D11_BIND_SHADER_RESOURCE;
	texArrayDesc.CPUAccessFlags     = 0;
	texArrayDesc.MiscFlags          = 0;

	ID3D11Texture2D* texArray = 0;

	hRes = device->CreateTexture2D( &texArrayDesc, 0, &texArray);

	//
	// Copy individual texture elements into texture array.
	//

	// for each texture element...
	for(UINT texElement = 0; texElement < size; ++texElement)
	{
		// for each mipmap level...
		for(UINT mipLevel = 0; mipLevel < texElementDesc.MipLevels; ++mipLevel)
		{
			D3D11_MAPPED_SUBRESOURCE mappedTex2D;
			hRes = context->Map(srcTex[texElement], mipLevel, D3D11_MAP_READ, 0, &mappedTex2D);

			context->UpdateSubresource(texArray, 
				D3D11CalcSubresource(mipLevel, texElement, texElementDesc.MipLevels),
				0, mappedTex2D.pData, mappedTex2D.RowPitch, mappedTex2D.DepthPitch);

			context->Unmap(srcTex[texElement], mipLevel);
		}
	}	

	//
	// Create a resource view to the texture array.
	//
	
	D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = texArrayDesc.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	viewDesc.Texture2DArray.MostDetailedMip = 0;
	viewDesc.Texture2DArray.MipLevels = texArrayDesc.MipLevels;
	viewDesc.Texture2DArray.FirstArraySlice = 0;
	viewDesc.Texture2DArray.ArraySize = size;

	ID3D11ShaderResourceView* texArraySRV = 0;
	device->CreateShaderResourceView(texArray, &viewDesc, &texArraySRV);

	//
	// Cleanup--we only need the resource view.
	//

	RELEASECOM(texArray);

	for(UINT i = 0; i < size; ++i)
		RELEASECOM(srcTex[i]);

	return texArraySRV;
}