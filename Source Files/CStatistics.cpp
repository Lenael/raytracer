#include "../Header Files/CStatistics.h"

using namespace std;

CStatistics::CStatistics()
{
   m_pSystem         = NULL;
}

CStatistics::CStatistics( CSystem* A_pSystem )
{
   m_pSystem         = A_pSystem;
}

bool CStatistics::Init()
{
   AddParameter( "FPS" );
   AddParameter( "FrameTime" );
return true;
}

void CStatistics::CalculateStatistics()
{
   CalculateFrameStats();
   
}

void CStatistics::Render()
{
   CalculateStatistics();
   //for test
   //for all parameters redner name : value
}

void CStatistics::AddParameter( string A_strParameter )
{
   std::pair<string, string> myPair( A_strParameter, "" );
   m_vParameters.insert( myPair );
}

void CStatistics::RemoveParameter( string A_strParameter )
{
   m_vParameters.erase(A_strParameter);
}

void CStatistics::AddValue( string A_strParameter, std::string A_strValue )
{
   m_vParameters.at(A_strParameter) = A_strValue;
}

void CStatistics::AddValue( string A_strParameter, float A_fValue )
{
   std::ostringstream value;   
		value.precision(15);
		value <<  A_fValue;

   m_vParameters[A_strParameter] = value.str();
}

void CStatistics::CalculateFrameStats()
{
	static int frameCnt = 0;
	static float timeElapsed = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
   if( (m_pSystem->m_pTimer->TotalTime() - timeElapsed) >= 1.0f )
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		//float mspf = 1000.0f / fps;

      AddValue( "FPS", fps );
      AddValue( "FrameTime", (float)m_pSystem->m_pTimer->DeltaTime() );

      //for tests
      /*
		std::ostringstream outs;   
		outs.precision(10);
		outs <<  "    "
			 << "FPS: " << fps << "    " 
			 << "Frame Time: " << mspf << " (ms)";
          */

      //SetWindowText(m_pSystem->m_pEngine->GetHwnd(), (LPCSTR)((string)"FPS: "+(string)m_vParameters.at("FPS")).c_str() );
      SetWindowText(m_pSystem->m_pEngine->GetHwnd(), (LPCSTR)((string)"Frame time: "+(string)m_vParameters.at("FrameTime")+"s").c_str());

		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}