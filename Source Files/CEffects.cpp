#include "../Header Files/CEffects.h"
#include "../Header Files/Common.h"
#include <vector>
#include <fstream>
#include "D3DCompiler.h"

HRESULT CompileShader( _In_ LPCWSTR srcFile, _In_ LPCSTR profile, _Outptr_ ID3DBlob** blob )
{
    if ( !srcFile || !profile || !blob )
       return E_INVALIDARG;

    *blob = nullptr;

    UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    flags |= D3DCOMPILE_DEBUG;
    flags |= D3DCOMPILE_PREFER_FLOW_CONTROL;
    flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    ID3DBlob* shaderBlob = nullptr;
    ID3DBlob* errorBlob = nullptr;
    HRESULT hr = D3DCompileFromFile( srcFile, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE,
                                     "CS", profile,
                                     flags, 0, &shaderBlob, &errorBlob );





    if ( FAILED(hr) )
    {
        if ( errorBlob )
        {
            OutputDebugStringA( (char*)errorBlob->GetBufferPointer() );
            errorBlob->Release();
        }

        if ( shaderBlob )
           shaderBlob->Release();

        return hr;
    }    

    *blob = shaderBlob;

    return hr;
}

#pragma region Effect
CEffect::CEffect(ID3D11Device* device, const std::string& filename)
	: mFX(0)
{
   try
   {
	   std::ifstream fin(filename.c_str(), std::ios::binary);

	   fin.seekg(0, std::ios_base::end);
	   int size = (int)fin.tellg();
	   fin.seekg(0, std::ios_base::beg);
	   std::vector<char> compiledShader(size);

	   fin.read(&compiledShader[0], size);
	   fin.close();

      //int flags = D3DXSHADER_DEBUG;
	
      //DWORD dwShaderFlags = D3DCOMPILE_DEBUG;

	   D3DX11CreateEffectFromMemory(&compiledShader[0], size, 0/*dwShaderFlags*/, device, &mFX);

      //ID3DBlob *vsBlob = nullptr;
      //HR( CompileShader( filename, "vs_4_0_level_9_1", &vsBlob ) );
    
      //HR( D3DX11CreateEffectFromMemory(vsBlob->GetBufferPointer(),vsBlob->GetBufferSize(),NULL,device,&mFX) );

   }
   catch(...)
   {
      MessageBox( 0, "Can't find fx file!", 0, 0 );
   }
}
CEffect::CEffect()
	: mFX(0)
{
}

CEffect::~CEffect()
{
	RELEASECOM(mFX);
}
#pragma endregion

#pragma region BasicEffect
BasicEffect::BasicEffect(ID3D11Device* device, const std::string& filename)
	: CEffect(device, filename)
{
	Light0Tech    = mFX->GetTechniqueByName("Light0");
	Light1Tech    = mFX->GetTechniqueByName("Light1");
	Light2Tech    = mFX->GetTechniqueByName("Light2");
	Light3Tech    = mFX->GetTechniqueByName("Light3");

	Light0TexTech = mFX->GetTechniqueByName("Light0Tex");
	Light1TexTech = mFX->GetTechniqueByName("Light1Tex");
	Light2TexTech = mFX->GetTechniqueByName("Light2Tex");
	Light3TexTech = mFX->GetTechniqueByName("Light3Tex");

	Light0TexAlphaClipTech = mFX->GetTechniqueByName("Light0TexAlphaClip");
	Light1TexAlphaClipTech = mFX->GetTechniqueByName("Light1TexAlphaClip");
	Light2TexAlphaClipTech = mFX->GetTechniqueByName("Light2TexAlphaClip");
	Light3TexAlphaClipTech = mFX->GetTechniqueByName("Light3TexAlphaClip");

	Light0FogTech    = mFX->GetTechniqueByName("Light0Fog");
	Light1FogTech    = mFX->GetTechniqueByName("Light1Fog");
	Light2FogTech    = mFX->GetTechniqueByName("Light2Fog");
	Light3FogTech    = mFX->GetTechniqueByName("Light3Fog");

	Light0TexFogTech = mFX->GetTechniqueByName("Light0TexFog");
	Light1TexFogTech = mFX->GetTechniqueByName("Light1TexFog");
	Light2TexFogTech = mFX->GetTechniqueByName("Light2TexFog");
	Light3TexFogTech = mFX->GetTechniqueByName("Light3TexFog");

	Light0TexAlphaClipFogTech = mFX->GetTechniqueByName("Light0TexAlphaClipFog");
	Light1TexAlphaClipFogTech = mFX->GetTechniqueByName("Light1TexAlphaClipFog");
	Light2TexAlphaClipFogTech = mFX->GetTechniqueByName("Light2TexAlphaClipFog");
	Light3TexAlphaClipFogTech = mFX->GetTechniqueByName("Light3TexAlphaClipFog");

	WorldViewProj     = mFX->GetVariableByName("gWorldViewProj")->AsMatrix();
	World             = mFX->GetVariableByName("gWorld")->AsMatrix();
	WorldInvTranspose = mFX->GetVariableByName("gWorldInvTranspose")->AsMatrix();
	TexTransform      = mFX->GetVariableByName("gTexTransform")->AsMatrix();
	EyePosW           = mFX->GetVariableByName("gEyePosW")->AsVector();
	FogColor          = mFX->GetVariableByName("gFogColor")->AsVector();
	FogStart          = mFX->GetVariableByName("gFogStart")->AsScalar();
	FogRange          = mFX->GetVariableByName("gFogRange")->AsScalar();
	DirLights         = mFX->GetVariableByName("gDirLights");
	Mat               = mFX->GetVariableByName("gMaterial");
	DiffuseMap        = mFX->GetVariableByName("gDiffuseMap")->AsShaderResource();
	CubeMap           = mFX->GetVariableByName("gCubeMap")->AsShaderResource();
}

BasicEffect::~BasicEffect()
{
}
#pragma endregion


#pragma region RayTrace Effect
RayTraceEffect::RayTraceEffect(ID3D11Device* device, const std::string& filename)
//	: CEffect(device, filename)
{
   try
   {
      std::wstring stemp = std::wstring(filename.begin(), filename.end());
      LPCWSTR sw = stemp.c_str();

      ID3DBlob *vsBlob = nullptr;
      HR( CompileShader( sw, "fx_5_0", &vsBlob ) );
    
      HR( D3DX11CreateEffectFromMemory(vsBlob->GetBufferPointer(),vsBlob->GetBufferSize(),0,device, &mFX, "FX/RayTrace.fxo" ) );

   }
   catch(...)
   {
      MessageBox( 0, "Can't find fx file!", 0, 0 );
   }




   RayTraceTech   = mFX->GetTechniqueByName("RayTrace");
   //In1            = mFX->GetVariableByName("gIn1")->AsShaderResource();
   //In2            = mFX->GetVariableByName("gIn2")->AsShaderResource();
   //Out            = mFX->GetVariableByName("gOut")->AsUnorderedAccessView();

   
   Directions     = mFX->GetVariableByName("b_directions")->AsShaderResource();
   Positions      = mFX->GetVariableByName("b_positions")->AsShaderResource();
   Stacks         = mFX->GetVariableByName("b_stacks")->AsUnorderedAccessView();
   Spheres        = mFX->GetVariableByName("b_spheres")->AsShaderResource();
   PointLights    = mFX->GetVariableByName("b_pointLights")->AsShaderResource();
   TextureOut = mFX->GetVariableByName("b_textureOut")->AsUnorderedAccessView();
   //ImageData      = mFX->GetVariableByName("b_ImageData")->AsUnorderedAccessView();
}

RayTraceEffect::~RayTraceEffect()
{
}
#pragma endregion

#pragma region RayTracer Effect
RayTracerEffect::RayTracerEffect(ID3D11Device* device, const std::string& filename)
//	: CEffect(device, filename)
{
   try
   {
      std::wstring stemp = std::wstring(filename.begin(), filename.end());
      LPCWSTR sw = stemp.c_str();

      ID3DBlob *vsBlob = nullptr;
      HR(CompileShader(sw, "fx_5_0", &vsBlob));

      HR(D3DX11CreateEffectFromMemory(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), 0, device, &mFX, "FX/RayTracer.fxo"));

   }
   catch (...)
   {
      MessageBox(0, "Can't find fx file!", 0, 0);
   }




   RayTracerTech = mFX->GetTechniqueByName("RayTrace");
   //In1            = mFX->GetVariableByName("gIn1")->AsShaderResource();
   //In2            = mFX->GetVariableByName("gIn2")->AsShaderResource();
   //Out            = mFX->GetVariableByName("gOut")->AsUnorderedAccessView();


   //Directions = mFX->GetVariableByName("b_directions")->AsShaderResource();
   //Positions = mFX->GetVariableByName("b_positions")->AsShaderResource();
   //Stacks = mFX->GetVariableByName("b_stacks")->AsUnorderedAccessView();
   Spheres = mFX->GetVariableByName("b_Spheres")->AsShaderResource();
   //PointLights = mFX->GetVariableByName("b_pointLights")->AsShaderResource();
   TextureOut = mFX->GetVariableByName("b_TextureOut")->AsUnorderedAccessView();
   //Params = mFX->GetVariableByName("b_Params")->AsShaderResource();
   //ImageData      = mFX->GetVariableByName("b_ImageData")->AsUnorderedAccessView();
}

RayTracerEffect::~RayTracerEffect()
{
}
#pragma endregion

#pragma region Effects

BasicEffect*              CEffects::BasicFX              = 0;
RayTraceEffect*           CEffects::RayTracerOldFX       = 0;
RayTracerEffect*          CEffects::RayTracerFX          = 0;

void CEffects::InitAll(ID3D11Device* device)
{
   BasicFX        = new BasicEffect(device, "FX/Basic.fxo");
   //RayTracerOldFX = new RayTraceEffect(device, "FX/RayTrace.fx");
   RayTracerFX    = new RayTracerEffect(device, "FX/RayTracer.fx");
}

void CEffects::DestroyAll()
{
	SAFEDELETE(BasicFX);
   SAFEDELETE(RayTracerOldFX);
   SAFEDELETE(RayTracerFX);
}
#pragma endregion