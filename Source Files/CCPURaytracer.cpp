#include "../Header Files/CCPURaytracer.h"
#include "../Header Files/Common.h"

CCPURaytracer::CCPURaytracer()
{
   m_tabPixels = NULL;
   m_fInfinity = 1e8;
   m_iMaxRayDepth = 5;
}

CCPURaytracer::~CCPURaytracer()
{
   m_pTexture->Release();
   m_pTextureSRV->Release();
   SAFEDELETE( m_tabPixels );
   //SAFEDELETE( m_pTextureSRV );
   //SAFEDELETE( m_pTexture );
   

   if( m_tabPixels != NULL )
   {
      delete [] m_tabPixels;
      m_tabPixels = NULL;
   }

	while (!m_vCSpheres.empty())
   {
		CSphere *sph = m_vCSpheres.back();
		m_vCSpheres.pop_back();
		delete sph;
	}
}

CCPURaytracer::CCPURaytracer( CSystem* A_pSystem )
{
   m_pSystem = A_pSystem;
   m_pQuad = new CQuad( A_pSystem );
   m_fInfinity = 1e8;
   m_iMaxRayDepth = 4;
   m_tabPixels = NULL;
   m_pTexture = NULL;
   m_pTextureSRV = NULL;
   m_f3Offset = XMFLOAT3(0.0f, 0.0f, 0.0f);

   XMStoreFloat4x4( &m_mWorld, XMMatrixIdentity() );
}

float CCPURaytracer::mix(const float &a, const float &b, const float &mix)
{
	return b * mix + a * (1.0f - mix);
};

void CCPURaytracer::Init( )
{
   m_pQuad->Init( 11.6f, 8.3f );

   SetupScene();

   //scena z dyfuzj�
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 20.0f, 0.0f), 3.0f, XMFLOAT3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f, 5.0f, 5.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, -10003.0f, -20.0f), 10000, XMFLOAT3(0.0f, 0.8f, 0.0f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(-2.3f, 0.0f, -20.0f), 2.0f, XMFLOAT3(0.90f, 0.50f, 0.20f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3( 2.3f, 0.0f, -20.0f), 2.0f, XMFLOAT3(0.20f, 0.50f, 0.90f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   //const XMFLOAT3 &A_f3Position, const float &A_fRadius, const XMFLOAT3 &A_f3Color, fReflection, const float &A_fTransparency, const XMFLOAT3 &A_f3EmissionColor)

   ////scena z odbiciem - dyfuzja+reflection
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, XMFLOAT3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f, 5.0f, 5.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000, XMFLOAT3(1.0f, 1.0f, 1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(-6.0f, 2.0f, -40.0f), 4.0f, XMFLOAT3(0.9f, 0.9f, 0.4f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(5.0f, 0.0f, -35.0f), 3.0f, XMFLOAT3(0.35f, 0.8f, 0.3f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

   //scena z refrakcja - dyfuzja+refrakcja
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, XMFLOAT3(0.0f,0.0f,0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f,5.0f,5.0f)));
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000, XMFLOAT3(1.0f,1.0f,1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f,0.0f,0.0f)));
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 0.0f, -20.0f), 4.0f, XMFLOAT3(0.6f, 0.6f, 1.0f), 1.0f, 0.8f, XMFLOAT3(0.0f,0.0f,0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(5.0f, 0.0f, -25.0f), 3.0f, XMFLOAT3(0.35f, 0.8f, 0.3f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(-4.0f, 0.0f, -25.0f), 3.0f, XMFLOAT3(0.35f, 0.9f, 0.8f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

   //scena z refrakcja - dyfuzja+refrakcja + pokazanie cienia
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, XMFLOAT3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f, 5.0f, 5.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000, XMFLOAT3(0.0f, 0.3f, 0.0f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   //m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 0.0f, -20.0f), 2.0f, XMFLOAT3(0.6f, 0.6f, 1.0f), 1.0f, 0.8f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

   //scena zlozona - wszystko
	//// light              
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, XMFLOAT3(0.0f,0.0f,0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f,5.0f,5.0f)));
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000, XMFLOAT3(1.0f,1.0f,1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f,0.0f,0.0f)));
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 0.0f, -20.0f), 4.0f, XMFLOAT3(0.6f, 0.6f, 1.0f), 1.0f, 0.8f, XMFLOAT3(0.0f,0.0f,0.0f)));
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(-8.0f, 6.0f, -30.0f), 4.0f, XMFLOAT3(0.9f, 0.9f, 0.4f), 1.0f, 0.0f, XMFLOAT3(0.0f,0.0f,0.0f)));
	//m_vCSpheres.push_back(new CSphere(XMFLOAT3(5.0f, 0.0f, -25.0f), 3.0f, XMFLOAT3(0.35f, 0.8f, 0.3f), 1.0f, 0.0f, XMFLOAT3(0.0f,0.0f,0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(8.0f, 6.0f, -30.0f), 2.0f, XMFLOAT3(0.90f, 0.50f, 0.20f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(-24.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(-18.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(-12.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(-6.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(0.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(6.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(12.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(18.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
 //  m_vCSpheres.push_back(new CSphere(XMFLOAT3(24.0f, 0.0f, -70.0f), 2.0f, XMFLOAT3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

   //wygenerowana zlozona
   //GenerateComplexScene(200, 1, 0, 0, -50);

   m_iWidth = 640;
   m_iHeight = 480;
   
   m_tabPixels = new float[m_iWidth*m_iHeight*4];
   
   // create texture 2d and pass it to the quad
   D3D11_SUBRESOURCE_DATA initData;
   initData.pSysMem = &m_tabPixels[0];
   initData.SysMemPitch = sizeof(float)*m_iWidth*4;

   D3D11_TEXTURE2D_DESC desc;
   desc.Width = m_iWidth;
   desc.Height = m_iHeight;
   desc.MipLevels = 1;
   desc.ArraySize = 1;
   desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
   desc.SampleDesc.Count = 1;
   desc.SampleDesc.Quality = 0;
   desc.Usage = D3D11_USAGE_DYNAMIC;
   desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
   desc.MiscFlags = 0;

   m_pSystem->m_pDirectx->m_pD3DDevice->CreateTexture2D( &desc, &initData, &m_pTexture );

   D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = desc.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	viewDesc.Texture2DArray.MostDetailedMip = 0;
	viewDesc.Texture2DArray.MipLevels = desc.MipLevels;
	viewDesc.Texture2DArray.FirstArraySlice = 0;
	viewDesc.Texture2DArray.ArraySize = desc.ArraySize;

	m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(m_pTexture, &viewDesc, &m_pTextureSRV);

   m_pQuad->SetTextureSRV( m_pTextureSRV );
}

void CCPURaytracer::SetupScene()
{
   if (m_pSystem->m_numSceneNumber == 1)
   {
      m_vCSpheres.clear();
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, 20.0f, 0.0f), 3.0f, XMFLOAT3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f, 5.0f, 5.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, -10003.0f, -20.0f), 10000, XMFLOAT3(0.0f, 0.8f, 0.0f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(-2.3f, 0.0f, -20.0f), 2.0f, XMFLOAT3(0.90f, 0.50f, 0.20f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(2.3f, 0.0f, -20.0f), 2.0f, XMFLOAT3(0.20f, 0.50f, 0.90f), 0.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   }
   else if (m_pSystem->m_numSceneNumber == 2)
   {
      m_vCSpheres.clear();
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, XMFLOAT3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f, 5.0f, 5.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000, XMFLOAT3(1.0f, 1.0f, 1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(-6.0f, 2.0f, -40.0f), 4.0f, XMFLOAT3(0.9f, 0.9f, 0.4f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(5.0f, 0.0f, -35.0f), 3.0f, XMFLOAT3(0.35f, 0.8f, 0.3f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   }
   else if (m_pSystem->m_numSceneNumber == 3)
   {
      m_vCSpheres.clear();
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, 20.0f, 20.0f), 3.0f, XMFLOAT3(0.0f,0.0f,0.0f), 0.0f, 0.0f, XMFLOAT3(5.0f,5.0f,5.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, -10004.0f, -20.0f), 10000, XMFLOAT3(1.0f,1.0f,1.0f), 1.0f, 0.0f, XMFLOAT3(0.0f,0.0f,0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(0.0f, 0.0f, -20.0f), 4.0f, XMFLOAT3(0.6f, 0.6f, 1.0f), 1.0f, 0.8f, XMFLOAT3(0.0f,0.0f,0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(5.0f, 0.0f, -25.0f), 3.0f, XMFLOAT3(0.35f, 0.8f, 0.3f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
      m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(-4.0f, 0.0f, -25.0f), 3.0f, XMFLOAT3(0.35f, 0.9f, 0.8f), 1.0f, 0.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
   }
   else if (m_pSystem->m_numSceneNumber == 4)
   {
   }
   else if (m_pSystem->m_numSceneNumber == 5)
   {

   }
   else if (m_pSystem->m_numSceneNumber == 6)
   {

   }

}

void  CCPURaytracer::GenerateComplexScene(int A_iSphereNum, float A_fRadius, float A_fOffsetX, float A_fOffsetY, float A_fOffsetZ)
{
   float x = 0, y = 0, z = 0;

   x += A_fOffsetX;
   y += A_fOffsetY;
   z += A_fOffsetZ;

   //draw first sphere
   m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(x, y, z), A_fRadius, XMFLOAT3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

   int sphereCnt = 1;
   int level = 1;
   while (sphereCnt < A_iSphereNum)
   {
      //count sphere number on this level
      float r = A_fRadius * level; //radius of circle with spheres on this level
      float twopir = 2 * XM_PI*r;
      float floorLevelSPhereNum = floorf(twopir / (2.0f*A_fRadius));
      int iLevelSphereNum = (int)floorLevelSPhereNum;

      float angleStep = 2.0f*(float)XM_PI / floorLevelSPhereNum;

      for (int i = 0; i<iLevelSphereNum; ++i)
      {
         x = r * cosf(angleStep*i) + A_fOffsetX;
         y = r * sinf(angleStep*i) + A_fOffsetY;
         z = A_fOffsetZ + level * r*0.2f;

         //save sphere
         if (sphereCnt % 3 == 0)
            m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(x, y, z), A_fRadius, XMFLOAT3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
         else if (sphereCnt % 3 == 1)
            m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(x, y, z), A_fRadius, XMFLOAT3(0.0f, 1.0f, 0.0f), 1.0f, 1.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));
         else if (sphereCnt % 3 == 2)
            m_vCSpheres.push_back(new CSphere(m_pSystem, XMFLOAT3(x, y, z), A_fRadius, XMFLOAT3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f, XMFLOAT3(0.0f, 0.0f, 0.0f)));

         ++sphereCnt;
         if (sphereCnt == A_iSphereNum)
            break;
      }
      ++level;
   }
}

XMFLOAT3 CCPURaytracer::Trace( const XMFLOAT3 &A_f3RayOrigin, const XMFLOAT3 &A_f3RayDirection, const std::vector<CSphere *> &A_vSpheres, const int &A_iDepth)
{
	float tNearestObject = m_fInfinity;
	const CSphere *sphere = NULL;

   // znajdz kolizje z obiektami na scenie
   // jesli koliduje z wieksza liczba, wybierz ten, ktory jest blizej
	for (unsigned i = 0; i < A_vSpheres.size(); ++i)
   {
		float t0 = m_fInfinity, t1 = m_fInfinity;
		if (A_vSpheres[i]->Intersect(A_f3RayOrigin, A_f3RayDirection, &t0, &t1))
      {
			if (t0 < 0)
         {
            //jestesmy w srodku
            t0 = t1;
         }

         if (t0 < tNearestObject)
         {
            tNearestObject = t0;
				sphere = A_vSpheres[i];
			}
		}
	}
   // jesli nie ma kolizji z obiektami, to zwroc taki oto kolor (najlepiej tlo, albo czarny/bialy)
	if (!sphere)
      return XMFLOAT3( 1.0f, 1.0f, 1.0f );

	XMVECTOR xmsurfaceColor = XMVectorZero(); // color of the ray/surfaceof the object intersected by the ray
   XMVECTOR xmrayorig = XMLoadFloat3(&A_f3RayOrigin);
   XMVECTOR xmraydir = XMLoadFloat3(&A_f3RayDirection);
   // obliczamy wektor punktu kolizji
   XMVECTOR xmphit = xmrayorig + xmraydir * tNearestObject;
   // obliczamy wektor normalny na podstawie srodka sfery i punktu kolizji
   //XMVECTOR xmnhit = XMLoadFloat3(&(sphere->m_f3Position)) - xmphit;
   XMVECTOR xmnhit = xmphit - XMLoadFloat3(&(sphere->m_f3Position));
   xmnhit = XMVector3Normalize( xmnhit );

	float blad = 0.0001f; // add some bias to the point from which we will be tracing
	bool inside = false;
   // sprawdzamy raydir z normalnym wektorem, zeby stwierdzic, czy jesteesmy w srodku sfery, czy nie.

   if ( XMVectorGetX(XMVector3Dot(xmraydir,xmnhit)) > 0.0f )
   {
      xmnhit = -xmnhit;
      inside = true;
   }

   // jesli kula jest przezroczysta, albo odbija i promien nie jest finalny
   if (A_iDepth < m_iMaxRayDepth)
   {
      if ((sphere->m_fTransparency > 0 || sphere->m_fReflection > 0))
      {
         XMVECTOR xmraydotnhit = XMVector3Dot(xmraydir, xmnhit);
         //zmienna pomocnicza - pomaga w uproszczonej wersji fresnela
         float ft = -XMVectorGetX(xmraydotnhit);////////////

         // tworzymy wspolczynnik fresnela
         //float fresnel = mix(pow(1.0f - ft, 3.0f), 1.0f, 0.4f); ///////////
         float fresnel = mix(pow(1.0f - ft, 5.0f), 1.0f, m_pSystem->m_fRefractiveIndex);

         // obliczamy kierunek odbicia - zmienne juz znormalizowane
         XMVECTOR xmrefldir = xmraydir - xmnhit * 2.0f * xmraydotnhit;
         xmrefldir = XMVector3Normalize(xmrefldir);

         XMFLOAT3 t1;
         XMFLOAT3 t2;
         XMStoreFloat3(&t1, xmphit + xmnhit * blad);
         XMStoreFloat3(&t2, xmrefldir);

         XMVECTOR xmreflection = XMLoadFloat3(&Trace(t1, t2, A_vSpheres, A_iDepth + 1));
         XMVECTOR xmrefraction = XMVectorZero();

         // jesli sfera jest tez przezroczysta obliczamy promien refrakcji
         if (sphere->m_fTransparency)
         {
            float ior = 1.1f;
            //sprawdzamy, czy jestesmy w srodku, czy na zewnatrz
            float e = (inside) ? ior : 1 / ior;

            float cosi = -XMVectorGetX(XMVector3Dot(xmnhit, xmraydir));
            float k = 1 - e * e * (1 - cosi * cosi);
            XMVECTOR xmrefrdir = xmraydir * e + xmnhit * (e *  cosi - sqrt(k));
            xmrefrdir = XMVector3Normalize(xmrefrdir);

            XMStoreFloat3(&t1, xmphit - xmnhit * blad);
            XMStoreFloat3(&t2, xmrefrdir);

            xmrefraction = XMLoadFloat3(&Trace(t1, t2, A_vSpheres, A_iDepth + 1));
         }

         // mieszamy odbicie i refrakcje
         xmsurfaceColor = (xmreflection * fresnel + xmrefraction * (1.0f - fresnel) * sphere->m_fTransparency) * XMLoadFloat3(&(sphere->m_f3Color));
      }
      else
      {
         // to zwykly dyfuzyjny obiekt i nie trzeba dalej sledzic promienia, bo mozemy od razu zwrocic kolor
         for (unsigned i = 0; i < A_vSpheres.size(); ++i)
         {
            // sprawdzamy, czy emituje swiatlo (szukamy sfer, ktore oswietlaja ten punkt)
            if (A_vSpheres[i]->m_f3EmissionColor.x > 0)
            {
               XMVECTOR xmtransmission = XMLoadFloat3(&XMFLOAT3(1.0f, 1.0f, 1.0f));
               // obliczamy wektor w kierunku zrodla swiatla, zaczynajacy sie w punkcie dla ktorego liczymy kolor
               XMVECTOR xmlightDirection = XMLoadFloat3(&(A_vSpheres[i]->m_f3Position)) - xmphit;
               xmlightDirection = XMVector3Normalize(xmlightDirection);

               //sprawdzamy, czy na drodze promienia swiatla do naszego punktu jest jakis obiekt zaslaniajacy
               for (unsigned j = 0; j < A_vSpheres.size(); ++j)
               {
                  //jesli to nie ta sama sfera
                  if (i != j)
                  {
                     float t0, t1;
                     XMFLOAT3 at1, at2;
                     // doajemy blad, zeby uniknac pozytywnego sprawdzenia kolizji 
                     XMStoreFloat3(&at1, xmphit + xmnhit * blad);
                     XMStoreFloat3(&at2, xmlightDirection);
                     // sprawdzamy kolizje z reszta sfer
                     if (A_vSpheres[j]->Intersect(at1, at2, &t0, &t1))
                     {
                        //xmtransmission = XMVectorZero();

                        // -----------------------------------------------------------------------------------
                        if (A_vSpheres[j]->m_fTransparency > 0.0f)
                        {
                           XMFLOAT3 t1;
                           XMFLOAT3 t2;
                           XMStoreFloat3(&t1, xmphit + xmnhit * blad);
                           XMStoreFloat3(&t2, xmlightDirection);

                           XMVECTOR xmreflection = XMLoadFloat3(&Trace(t1, t2, A_vSpheres, A_iDepth + 1));
                           xmsurfaceColor += xmreflection;
                        }
                        else
                        {
                           xmtransmission = XMVectorZero();
                           break;
                        }
                        // --------------------------------------------------------------------
                     }
                  }
               }
               xmsurfaceColor += XMLoadFloat3(&(sphere->m_f3Color)) * xmtransmission * max(0.0f, XMVectorGetX(XMVector3Dot(xmnhit, xmlightDirection))) * XMLoadFloat3(&(A_vSpheres[i]->m_f3EmissionColor));
            }
         }
      }
   }

   XMFLOAT3 ret;
   XMStoreFloat3( &ret, xmsurfaceColor + XMLoadFloat3( &(sphere->m_f3EmissionColor) ));

   //if (A_iDepth == m_iMaxRayDepth)
   //{
   //   XMFLOAT3 one = XMFLOAT3(1.0f, 1.0f, 1.0f);
   //   XMStoreFloat3(&ret, xmsurfaceColor + XMLoadFloat3(&(sphere->m_f3EmissionColor)) + XMLoadFloat3(&one));
   //}

   //if (abs(ret.x) < 0.001)
   //{
   //   ret = ret;
   //}

	return ret;
};

void CCPURaytracer::Raytrace()
{
   XMFLOAT3 pixel;

	float invWidth = 1.0f / m_iWidth;
   float invHeight = 1.0f / m_iHeight;
	float fov = 30;
   float aspectratio = (float)m_iWidth / (float)m_iHeight;
	float angle = tan(XM_PI * 0.5f * fov / 180.0f );
   int counter = 0;
	for (unsigned y = 0; y < m_iHeight; ++y)
   {
		for (unsigned x = 0; x < m_iWidth; ++x)
      {
         //if (x == 125 && y == 420)
         //{
         //   x = x;
         //}
         // x+0.5 - chcemy wyznaczyc pozycje dla srodka piksela o wymiarach 1x1, stad przesuniecie o 0.5
         // pozniej remapowanie - 2x znormalizowana pozycja piksela - 1
         // dla y odwrotnie -> 1 - 2x znormalizowana pozycja piksela
         //  width jest wieksze, wiec skalujemy przez ratio
			float x2d = (2.0f * ((x + 0.5f) * invWidth) - 1.0f) * angle * aspectratio;
			float y2d = (1.0f - 2.0f * ((y + 0.5f) * invHeight)) * angle;
			XMFLOAT3 raydir(x2d, y2d, -1);
         XMStoreFloat3( &raydir, XMVector3Normalize( XMLoadFloat3(&raydir) ));
         pixel = Trace(XMFLOAT3(0.0f, 0.0f, 0.0f), raydir, m_vCSpheres, 0);
			m_tabPixels[counter++] = pixel.x;
         m_tabPixels[counter++] = pixel.y;
         m_tabPixels[counter++] = pixel.z;
         m_tabPixels[counter++] = 1.0f;
		}
	}

   //map unmap
   D3D11_MAPPED_SUBRESOURCE mappedResource;
   m_pSystem->m_pDirectx->m_pD3DContext->Map(m_pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

   memcpy( mappedResource.pData, m_tabPixels, sizeof(float)*m_iWidth*m_iHeight*4 );
   
   m_pSystem->m_pDirectx->m_pD3DContext->Unmap(m_pTexture, 0);
}

void CCPURaytracer::Render()
{
   SetupScene();

   if( GetAsyncKeyState('A') & 0x8000 )
   {
      //for( unsigned i=0; i<m_vCSpheres.size(); ++i )
      //{
         m_f3Offset.x += 0.25f;
         //m_vCSpheres[i]->Move( 0.25f, 0.0f, 0.0f );
      //}
   }
   if( GetAsyncKeyState('D') & 0x8000 )
   {
      //for( unsigned i=0; i<m_vCSpheres.size(); ++i )
      //{
         m_f3Offset.x -= 0.25f;
         //m_vCSpheres[i]->Move(-0.25f, 0.0f, 0.0f);
      //}
   }
   if( GetAsyncKeyState('W') & 0x8000 )
   {
      //for( unsigned i=0; i<m_vCSpheres.size(); ++i )
      //{
         m_f3Offset.z += 0.25f;
         //m_vCSpheres[i]->Move(0.0f, 0.0f, 0.25f);
      //}
   }
   if( GetAsyncKeyState('S') & 0x8000 )
   {
      //for( unsigned i=0; i<m_vCSpheres.size(); ++i )
      //{
         m_f3Offset.z -= 0.25f;
         //m_vCSpheres[i]->Move(0.0f, 0.0f, -0.25f);
      //}
   }   
   if (GetAsyncKeyState('Q') & 0x8000)
   {
      //for (unsigned i = 0; i<m_vCSpheres.size(); ++i)
      //{
         m_f3Offset.y -= 0.25f;
        // m_vCSpheres[i]->Move(0.0f, -0.25f, 0.0f);
      //}
   }
   if (GetAsyncKeyState('E') & 0x8000)
   {
      //for (unsigned i = 0; i<m_vCSpheres.size(); ++i)
      //{
         //m_vCSpheres[i]->Move(0.0f, 0.25f, 0.0f);
      //}
      m_f3Offset.y += 0.25f;
   }

   for (unsigned i = 0; i<m_vCSpheres.size(); ++i)
   {
      m_vCSpheres[i]->Move(m_f3Offset.x, m_f3Offset.y, m_f3Offset.z);
   }

   Raytrace();

   m_pQuad->Render();
  
}

void CCPURaytracer::SetPosition( CXMMATRIX M )
{
   XMStoreFloat4x4( &m_mWorld, M );
}
