#include "../Header Files/CSystem.h"
#include "../Header Files/Common.h"
#include "../Header Files/CVertex.h"
#include "../Header Files/CEffects.h"
#include "../Header Files/CRenderStates.h"

using namespace DirectX;

CSystem::CSystem()
{
   m_pDirectx = new CDirectx(this);
   m_pEngine = NULL;
   m_bEnginePaused = true;
   m_bEngineRunning = false;
   m_bMouseEnabled = false;
   m_pTimer = new CTimer();
   m_pStatistics = new CStatistics(this);
   m_pCPURaytracer = new CCPURaytracer(this);
   m_pGPURaytracer = new CGPURaytracer(this);
   m_pCamera = new CCamera(this);
   m_bRenderByCPU = false;
   m_numSceneNumber = 1;
   m_bGeometricCollision = true;
   m_fRefractiveIndex = 0.4f;
}

CSystem::CSystem(CEngineApp* A_pEngine) : CSystem()
{
   m_pEngine         = A_pEngine;
}

CSystem::~CSystem()
{
   SAFEDELETE( m_pCamera );
   SAFEDELETE( m_pCPURaytracer );
   SAFEDELETE( m_pGPURaytracer );
   SAFEDELETE( m_pDirectx );
   SAFEDELETE( m_pTimer );
   SAFEDELETE( m_pStatistics );
}

bool CSystem::Init()
{
   if( m_pDirectx->Init() == false )
      return false;

   m_pCPURaytracer->Init();

   m_pGPURaytracer->Init();

   CEffects::InitAll( m_pDirectx->m_pD3DDevice );
   InputLayouts::InitAll( m_pDirectx->m_pD3DDevice );
   CRenderStates::InitAll( m_pDirectx->m_pD3DDevice );

   // ------ end modules init --------------------------------

   // prepare lights
   m_DirLights[0].Ambient  = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
   m_DirLights[0].Diffuse  = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
   m_DirLights[0].Specular = XMFLOAT4(0.8f, 0.8f, 0.7f, 1.0f);
   m_DirLights[0].Direction = XMFLOAT3(0.707f, -0.707f, 0.0f);

   m_DirLights[1].Ambient  = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
   m_DirLights[1].Diffuse  = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
   m_DirLights[1].Specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
   m_DirLights[1].Direction = XMFLOAT3(0.57735f, -0.57735f, 0.57735f);

   m_DirLights[2].Ambient  = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
   m_DirLights[2].Diffuse  = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
   m_DirLights[2].Specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
   m_DirLights[2].Direction = XMFLOAT3(-0.57735f, -0.57735f, -0.57735f);

   // set camera variables
   //m_pCamera->SetPosition(-17.0f, 0.0f, -5.0f);
   //m_pCamera->RotateY(XM_PI/2.0f);

   m_pCamera->SetPosition( 0.0f, 0.0f, -10.0f );
   m_pCamera->SetLens(0.25f*XM_PI, static_cast<float>(m_pEngine->GetViewWidth())/static_cast<float>(m_pEngine->GetViewHeight()), 1.0f, 3000.0f);

return true;
}

void CSystem::PreMessageLoop()
{
   m_bEnginePaused = false;
   m_bEngineRunning = true;
   m_pTimer->Reset();
}

void CSystem::Render()
{
   m_pTimer->Tick();

   m_pCamera->UpdateViewMatrix();

   // --------------------------------------------------------
   
   m_pDirectx->m_pD3DContext->ClearRenderTargetView(m_pDirectx->m_pRenderTargetView, reinterpret_cast<const float*>(&CommonColors::Green));
   m_pDirectx->m_pD3DContext->ClearDepthStencilView(m_pDirectx->m_pDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);

   
   CEffects::BasicFX->SetDirLights(m_DirLights);
   CEffects::BasicFX->SetEyePosW(m_pCamera->GetPosition());

   if( m_bRenderByCPU )
   {
      m_pCPURaytracer->Render();
   }
   else
   {
      m_pGPURaytracer->Render();
   }

   m_pDirectx->m_pD3DContext->RSSetState(0);
   m_pDirectx->m_pD3DContext->OMSetDepthStencilState(0, 0);


   // draw statistics
   m_pStatistics->Render();

   m_pDirectx->Present();
}

bool CSystem::IsEnginePaused() const
{
   return m_bEnginePaused;
}

bool CSystem::IsEngineRunning() const
{
   return m_bEngineRunning;
}

void CSystem::OnEngineActive()
{
   m_bEnginePaused = false;
   m_pTimer->Start();
}

void CSystem::OnEngineInactive()
{
   m_bEnginePaused = true;
   m_pTimer->Stop();
}

void CSystem::OnResize()
{
   m_pDirectx->Resize();
}

void CSystem::OnSizeMaximized()
{
   m_bEnginePaused   = false;
   m_bMinimized      = false;
   m_bMaximized      = true;
   OnResize();
}

void CSystem::OnSizeMinimized()
{
   m_bEnginePaused   = true;
   m_bMinimized      = true;
   m_bMaximized      = false;
}

void CSystem::OnSizeRestored()
{
   if( m_bMinimized )
   {
      m_bEnginePaused   = false;
      m_bMinimized      = false;
      OnResize();
   }
   else if( m_bMaximized )
   {
      m_bEnginePaused   = false;
      m_bMaximized      = false;
      OnResize();
   }
   else if( m_bResizing )
   {

   }
   else
   {
      OnResize();
   }

}

void CSystem::OnEnterSizeMove()
{
   m_bEnginePaused   = true;
   m_bResizing       = true;
   m_pTimer->Stop();
}

void CSystem::OnExitSizeMove()
{
   m_bEnginePaused   = false;
   m_bResizing       = false;
   m_pTimer->Start();
   OnResize();
}

void CSystem::OnMouseDown(WPARAM A_BtnState, int x, int y)
{
   //TODO: move to CInput class
   if( m_bMouseEnabled )
   {
      m_pCamera->SetLastMousePos( x, y );

      SetCapture(m_pEngine->GetHwnd());
   }
}

void CSystem::OnMouseUp(WPARAM A_BtnState, int x, int y)
{
   if( m_bMouseEnabled )
	   ReleaseCapture();
}

void CSystem::OnMouseMove(WPARAM A_BtnState, int x, int y)
{
   if( m_bMouseEnabled )
   {
	   if( (A_BtnState & MK_LBUTTON) != 0 )
	   {
		   // Make each pixel correspond to a quarter of a degree.
		   float dx = XMConvertToRadians(0.25f*static_cast<float>(x - m_pCamera->GetLastMousePos().x));
		   float dy = XMConvertToRadians(0.25f*static_cast<float>(y - m_pCamera->GetLastMousePos().y));

         m_pCamera->Pitch( dy );
         m_pCamera->RotateY( dx );
	   }
   
      m_pCamera->SetLastMousePos( x, y );
   }
}

void CSystem::SetMouseEnabled( bool A_bEnabled )
{
   m_bMouseEnabled = A_bEnabled;
}