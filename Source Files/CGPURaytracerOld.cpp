#include "../Header Files/CGPURaytracerOld.h"
#include "../Header Files/Common.h"
#include "../Header Files/CEffects.h"

CGPURaytracerOld::CGPURaytracerOld()
{
   m_numElements = 32;

   m_pDirections = NULL;
   m_pSpheres = NULL;
   m_pPointLights = NULL;
   m_pPositions = NULL;
   m_pStack = NULL;
}

CGPURaytracerOld::~CGPURaytracerOld()
{
   //RELEASECOM( m_pInput1SRV );
   //RELEASECOM( m_pInput2SRV );
   //RELEASECOM( m_pOutputUAV 
   RELEASECOM( m_pDirections );
   RELEASECOM( m_pPositions );
   //RELEASECOM( m_pInputStackSRV );
   RELEASECOM( m_pStack )
   RELEASECOM( m_pSpheres );
   RELEASECOM( m_pPointLights );
   RELEASECOM( m_pTextureUAV );
   RELEASECOM( m_pTextureSRV );
}

CGPURaytracerOld::CGPURaytracerOld( CSystem* A_pSystem )
{
   m_pSystem = A_pSystem;
   m_pQuad = new CQuad(A_pSystem);
   m_numElements = 32;

   m_pDirections = NULL;
   m_pSpheres = NULL;
   m_pPointLights = NULL;
   m_pPositions = NULL;
   m_pStack = NULL;
   m_pTextureUAV = NULL;
   m_pTextureSRV = NULL;
}

bool CGPURaytracerOld::glusRaytracePerspectivef(float* directionBuffer, const float fovy, const int width, const int height)
{
   int i;

   float aspect;

   float yExtend;
   float xExtend;

   float xStep;
   float yStep;

   if (!directionBuffer || width <= 0 || height <= 0)
   {
      return false;
   }

   aspect = (float)width / (float)height;

   yExtend = tanf(XMConvertToRadians(fovy * 0.5f));
   xExtend = yExtend * aspect;

   xStep = xExtend / ((float)(width) * 0.5f);
   yStep = yExtend / ((float)(height) * 0.5f);

   for (i = 0; i < width * height; i++)
   {
      directionBuffer[i * (3) + 0] = -xExtend + xStep * 0.5f + xStep * (float)(i % width);
      directionBuffer[i * (3) + 1] = -yExtend + yStep * 0.5f + yStep * (float)(i / width);
      directionBuffer[i * (3) + 2] = -1.0f;

      // zerowanie paddingu
      //for (k = 0; k < padding; k++)
      //{
      //   directionBuffer[i * (3 + padding) + 3 + k] = 0.0f;
      //}

      XMFLOAT3 f3(directionBuffer[i * (3)],directionBuffer[(i * (3))+1],directionBuffer[(i * (3))+2]);
      
      XMVECTOR ret = XMVector3Normalize(XMLoadFloat3(&f3));
      
      XMStoreFloat3( &f3, ret );
      directionBuffer[i * (3)] = f3.x;
      directionBuffer[(i * (3))+1] = f3.y;
      directionBuffer[(i * (3))+2] = f3.z;
      //glusVector3Normalizef(&directionBuffer[i * (3 + padding)]);
   }

return true;
}

void CGPURaytracerOld::glusMatrix3x3MultiplyVector3f(float result[3], const float matrix[9], const float vector[3])
{
    int i;

    float temp[3];

    for (i = 0; i < 3; i++)
    {
        temp[i] = matrix[i] * vector[0] + matrix[3 + i] * vector[1] + matrix[6 + i] * vector[2];
    }

    for (i = 0; i < 3; i++)
    {
        result[i] = temp[i];
    }
}

void CGPURaytracerOld::glusRaytraceLookAtf(float* positionBuffer, float* directionBuffer, const float* originDirectionBuffer, const int width, const int height, const float eyeX, const float eyeY, const float eyeZ, const float centerX, const float centerY, const float centerZ, const float upX, const float upY, const float upZ)
{
   float forward[3], up[3];
   float rotation[9];
   int i;

   forward[0] = centerX - eyeX;
   forward[1] = centerY - eyeY;
   forward[2] = centerZ - eyeZ;

   //glusVector3Normalizef(forward);
   XMFLOAT3 f3(forward[0],forward[1],forward[2]);
   XMVECTOR ret = XMVector3Normalize(XMLoadFloat3(&f3));
      
   XMStoreFloat3( &f3, ret );
   forward[0] = f3.x;
   forward[1] = f3.y;
   forward[2] = f3.z;

   up[0] = upX;
   up[1] = upY;
   up[2] = upZ;

   //glusVector3Crossf(side, forward, up);
   //glusVector3Normalizef(side);
   XMFLOAT3 vforward(forward[0],forward[1],forward[2]);
   XMFLOAT3 vup(up[0],up[1],up[2]);
   XMVECTOR vside = XMVector3Cross( XMLoadFloat3(&vforward), XMLoadFloat3(&vup) );
   vside = XMVector3Normalize(vside);
   XMFLOAT3 fside;
   XMStoreFloat3(&fside,vside);

   
   XMVECTOR retup = XMVector3Cross( vside, XMLoadFloat3(&vforward) );
   XMFLOAT3 vretup;
   XMStoreFloat3(&vretup, retup );
   
   up[0] = vretup.x;
   up[1] = vretup.y;
   up[2] = vretup.z;

   //glusVector3Crossf(up, side, forward);

   rotation[0] = fside.x;
   rotation[1] = fside.y;
   rotation[2] = fside.z;
   rotation[3] = up[0];
   rotation[4] = up[1];
   rotation[5] = up[2];
   rotation[6] = -forward[0];
   rotation[7] = -forward[1];
   rotation[8] = -forward[2];

   for (i = 0; i < width * height; i++)
   {
      if (positionBuffer)
      {
         positionBuffer[i * 4 + 0] = eyeX;
         positionBuffer[i * 4 + 1] = eyeY;
         positionBuffer[i * 4 + 2] = eyeZ;
         positionBuffer[i * 4 + 3] = 1.0f;
      }

      if (directionBuffer && originDirectionBuffer)
      {
         glusMatrix3x3MultiplyVector3f(&directionBuffer[i * (3)], rotation, &originDirectionBuffer[i * (3)]);//res,matrix,vector

         //for (k = 0; k < padding; k++)
         //{
            directionBuffer[i * (3) + 3 + 1] = originDirectionBuffer[i * (3) + 3 + 1];
         //}
      }
   }
}

void CGPURaytracerOld::Init()
{
	//std::vector<Data> dataA(m_numElements);
	//std::vector<Data> dataB(m_numElements);
	//for(int i = 0; i < m_numElements; ++i)
	//{
	//	dataA[i].v1 = XMFLOAT3(1.0f, 0.0f, 1.0f);
	//	dataA[i].v2 = XMFLOAT3(1.0f, 0.0f, 1.0f);

	//	dataB[i].v1 = XMFLOAT3(1.0f, 0.0f, 1.0f);
	//	dataB[i].v2 = XMFLOAT3(1.0f, 0.0f, 1.0f);
	//}

	//// Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
	//D3D11_BUFFER_DESC inputDesc;
 //  inputDesc.Usage = D3D11_USAGE_DEFAULT;
 //  inputDesc.ByteWidth = sizeof(Data) * m_numElements;
 //  inputDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
 //  inputDesc.CPUAccessFlags = 0;
 //  inputDesc.StructureByteStride = sizeof(Data);
 //  inputDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

 //  D3D11_SUBRESOURCE_DATA vinitDataA;
 //  vinitDataA.pSysMem = &dataA[0];

 //  ID3D11Buffer* bufferA = 0;
 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDesc, &vinitDataA, &bufferA);

 //  D3D11_SUBRESOURCE_DATA vinitDataB;
 //  vinitDataB.pSysMem = &dataB[0];

 //  ID3D11Buffer* bufferB = 0;
 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDesc, &vinitDataB, &bufferB);

 //  // Create a read-write buffer the compute shader can write to (D3D11_BIND_UNORDERED_ACCESS).
 //  D3D11_BUFFER_DESC outputDesc;
 //  outputDesc.Usage = D3D11_USAGE_DEFAULT;
 //  outputDesc.ByteWidth = sizeof(Data) * m_numElements;
 //  outputDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
 //  outputDesc.CPUAccessFlags = 0;
 //  outputDesc.StructureByteStride = sizeof(Data);
 //  outputDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDesc, 0, &m_pOutputBuffer);

 //  // Create a system memory version of the buffer to read the results back from.
 //  outputDesc.Usage = D3D11_USAGE_STAGING;
 //  outputDesc.BindFlags = 0;
 //  outputDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDesc, 0, &m_pOutputDebugBuffer);


 //  D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
 //  srvDesc.Format = DXGI_FORMAT_UNKNOWN;
 //  srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
 //  srvDesc.BufferEx.FirstElement = 0;
 //  srvDesc.BufferEx.Flags = 0;
 //  srvDesc.BufferEx.NumElements = m_numElements;

 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferA, &srvDesc, &m_pInput1SRV);
 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferB, &srvDesc, &m_pInput2SRV);


 //  D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
 //  uavDesc.Format = DXGI_FORMAT_UNKNOWN;
 //  uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
 //  uavDesc.Buffer.FirstElement = 0;
 //  uavDesc.Buffer.Flags = 0;
 //  uavDesc.Buffer.NumElements = m_numElements;

 //  m_pSystem->m_pDirectx->m_pD3DDevice->CreateUnorderedAccessView(m_pOutputBuffer, &uavDesc, &m_pOutputUAV);


 //  // Views hold references to buffers, so we can release these.
 //  RELEASECOM(bufferA);
 //  RELEASECOM(bufferB);

   // ------------------------------------------------------------------------------
   
   const int width =  640, height = 480;
   
   int numPixels = width*height;

   std::vector<XMFLOAT3> directions(numPixels);
	std::vector<XMFLOAT4> positions(numPixels);

	//XMFLOAT3 pixel;
   //const int width = m_pSystem->m_pEngine->GetViewWidth(), height = m_pSystem->m_pEngine->GetViewHeight();
   //sprawdzic, czy zwraca odpowiednie wartosci ^ ------------------------------------------

   //float directionBuffer[1024 * 768 * (3)];
   float *directionBuffer = new float[numPixels*3];

   if( !glusRaytracePerspectivef( directionBuffer, 30.0f, width, height) )
   {
      MessageBox( NULL, "dupa", "dupa", NULL );
   }

   // Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
	D3D11_BUFFER_DESC inputDescDir;
   inputDescDir.Usage = D3D11_USAGE_DEFAULT;
   inputDescDir.ByteWidth = sizeof(XMFLOAT3) * numPixels;
   inputDescDir.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   inputDescDir.CPUAccessFlags = 0;
   inputDescDir.StructureByteStride = sizeof(XMFLOAT3);
   inputDescDir.MiscFlags = 0;

   for( int i=0; i<numPixels; ++i )
   {
      directions[i].x = directionBuffer[i*3];
      directions[i].y = directionBuffer[i*3+1];
      directions[i].z = directionBuffer[i*3+2];
   }

   D3D11_SUBRESOURCE_DATA vInitDataDir;
   vInitDataDir.pSysMem = &directions[0];

   ID3D11Buffer* bufferDir = 0;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescDir, &vInitDataDir, &bufferDir) );


   D3D11_SHADER_RESOURCE_VIEW_DESC srvDescDir;
   srvDescDir.Format = DXGI_FORMAT_R32G32B32_FLOAT;
   srvDescDir.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   srvDescDir.BufferEx.FirstElement = 0;
   srvDescDir.BufferEx.Flags = 0;
   srvDescDir.BufferEx.NumElements = numPixels;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferDir, &srvDescDir, &m_pDirections) );

   
   RELEASECOM(bufferDir);


   //======--------------------------- positions buffer------------------------------------
   
   float *positionBuffer = new float[numPixels*4];
   glusRaytraceLookAtf(positionBuffer, directionBuffer, directionBuffer, width, height, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

      // Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
	D3D11_BUFFER_DESC inputDescPos;
   inputDescPos.Usage = D3D11_USAGE_DEFAULT;
   inputDescPos.ByteWidth = sizeof(XMFLOAT4) * numPixels;
   inputDescPos.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   inputDescPos.CPUAccessFlags = 0;
   inputDescPos.StructureByteStride = sizeof(XMFLOAT4);
   inputDescPos.MiscFlags = 0;

   for( int i=0; i<numPixels; ++i )
   {
      positions[i].x = directionBuffer[i*3];
      positions[i].y = directionBuffer[i*3+1];
      positions[i].z = directionBuffer[i*3+2];
      positions[i].w = directionBuffer[i*3+3];
   }

   D3D11_SUBRESOURCE_DATA vInitDataPos;
   vInitDataPos.pSysMem = &positions[0];

   ID3D11Buffer* bufferPos = 0;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescPos, &vInitDataPos, &bufferPos) );


   D3D11_SHADER_RESOURCE_VIEW_DESC srvDescPos;
   srvDescPos.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
   srvDescPos.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   srvDescPos.BufferEx.FirstElement = 0;
   srvDescPos.BufferEx.Flags = 0;
   srvDescPos.BufferEx.NumElements = numPixels;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferPos, &srvDescPos, &m_pPositions) );
   
   SAFEARRAYDELETE(directionBuffer);
   SAFEARRAYDELETE(positionBuffer);
   RELEASECOM(bufferPos);

   // -------------------------- stack buffer ------------------------------------------

   // Every ray can have two sub rays (reflect and refract). This can be organized as a tree, with breadth-first indexing.
   // So, a tree with a depth has 2^depth-1 nodes. In this case we have a MAX_DEPTH of 5
   #define NUM_STACK_NODES (2 * 2 * 2 * 2 * 2 - 1)

   // As no recursion is possible in GLSL, the following is done:
   // As the number of rays (= nodes) is known, all rays plus sub rays are executed. All needed values are stored in a stack node.
   // After this is done, the tree is traversed again from the leaf node to the root. Now the color of node can be calculated
   // by the using the sub nodes. Finally, in the root node the final color is stored.
   #define STACK_NODE_FLOATS (4 + (3) + 4 + 4 + (3) + 4 + 1 + 1 + 1 + 1)

   //float *stackBuffer = new float[width*height* STACK_NODE_FLOATS * NUM_STACK_NODES];
   //Stack *stackBuffer = new Stack[numPixels * NUM_STACK_NODES];
   std::vector<float> stackBuffer(width*height* STACK_NODE_FLOATS * NUM_STACK_NODES);
   
   for( int i=0; i<width*height* STACK_NODE_FLOATS * NUM_STACK_NODES; ++i )
   {
      stackBuffer[i] = 0.0f;
   }

   // Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
	//D3D11_BUFFER_DESC inputDescStack;
 //  inputDescStack.Usage = D3D11_USAGE_DEFAULT;
 //  inputDescStack.ByteWidth = sizeof(Stack) * numPixels;
 //  inputDescStack.BindFlags = D3D11_BIND_SHADER_RESOURCE;
 //  inputDescStack.CPUAccessFlags = 0;
 //  inputDescStack.StructureByteStride = sizeof(Stack);
 //  inputDescStack.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   D3D11_SUBRESOURCE_DATA vinitDataStack;
   vinitDataStack.pSysMem = &stackBuffer[0];

   //ID3D11Buffer* bufferStack = 0;
   //m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescStack, &vinitDataStack, &bufferStack);
   //
   //D3D11_SHADER_RESOURCE_VIEW_DESC srvDescStack;
   //srvDescStack.Format = DXGI_FORMAT_UNKNOWN;
   //srvDescStack.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   //srvDescStack.BufferEx.FirstElement = 0;
   //srvDescStack.BufferEx.Flags = 0;
   //srvDescStack.BufferEx.NumElements = numPixels;

   //m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferStack, &srvDescStack, &m_pInputStackSRV);

   // Create a read-write buffer the compute shader can write to (D3D11_BIND_UNORDERED_ACCESS).
   D3D11_BUFFER_DESC outputDescStack;
   outputDescStack.Usage = D3D11_USAGE_DEFAULT;
   outputDescStack.ByteWidth = sizeof(Stack) * numPixels;
   outputDescStack.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
   outputDescStack.CPUAccessFlags = 0;
   outputDescStack.StructureByteStride = sizeof(Stack);
   outputDescStack.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDescStack, &vinitDataStack, &m_pOutputStack) );

   // Create a system memory version of the buffer to read the results back from.
   outputDescStack.Usage = D3D11_USAGE_STAGING;
   outputDescStack.BindFlags = 0;
   outputDescStack.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDescStack, &vinitDataStack, &m_pOutputDebugStack) );
   

   D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescStack;
   uavDescStack.Format = DXGI_FORMAT_UNKNOWN;
   uavDescStack.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
   uavDescStack.Buffer.FirstElement = 0;
   uavDescStack.Buffer.Flags = 0;
   uavDescStack.Buffer.NumElements = numPixels;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateUnorderedAccessView(m_pOutputStack, &uavDescStack, &m_pStack) );

   
   //SAFEARRAYDELETE(stackBuffer);
   //RELEASECOM(m_pOutputStack);
   stackBuffer.clear();

   // ----------------------------------------- test buffer -------------------------------

   //D3D11_SUBRESOURCE_DATA vinitDataStack;
   //vinitDataStack.pSysMem = &stackBuffer[0];
   ID3D11Buffer* outputTestBuffer;
   ID3D11Buffer* outputDebugTestBuffer;
   ID3D11UnorderedAccessView* uavTest;

   // Create a read-write buffer the compute shader can write to (D3D11_BIND_UNORDERED_ACCESS).
   D3D11_BUFFER_DESC testBufferDesc;
   testBufferDesc.Usage = D3D11_USAGE_DEFAULT;
   testBufferDesc.ByteWidth = sizeof(Stack) * numPixels*243;
   testBufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
   testBufferDesc.CPUAccessFlags = 0;
   testBufferDesc.StructureByteStride = sizeof(Stack);
   testBufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&testBufferDesc, NULL, &outputTestBuffer));

   // Create a system memory version of the buffer to read the results back from.
   testBufferDesc.Usage = D3D11_USAGE_STAGING;
   testBufferDesc.BindFlags = 0;
   testBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&testBufferDesc, NULL, &outputDebugTestBuffer));


   D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescTest;
   uavDescTest.Format = DXGI_FORMAT_UNKNOWN;
   uavDescTest.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
   uavDescTest.Buffer.FirstElement = 0;
   uavDescTest.Buffer.Flags = 0;
   uavDescTest.Buffer.NumElements = numPixels*243;

   HR(m_pSystem->m_pDirectx->m_pD3DDevice->CreateUnorderedAccessView(outputTestBuffer, &uavDescTest, &uavTest));


   // ----------------------------------------- spheres -----------------------------

   #define NUM_SPHERES 6

   Sphere spheres[NUM_SPHERES] = {
   // Ground sphere
       { XMFLOAT4( 0.0f, -10001.0f, -20.0f, 1.0f ), 10000.0f, /*XMFLOAT3(0.0f, 0.0f, 0.0f),*/ { XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.4f, 0.4f, 0.4f, 1.0f ), XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), 0.0f, 1.0f, 0.0f/*, 0.0f*/ /*padding*/ } },
   // Transparent sphere
       { XMFLOAT4( 0.0f, 0.0f, -10.0f, 1.0f ), 1.0f, { XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), 20.0f, 0.2f, 1.0f, /*padding*/ } },
   // Reflective sphere
       { XMFLOAT4( 1.0f, -0.75f, -7.0f, 1.0f ), 0.25f, { XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), 20.0f, 1.0f, 0.8f, /*padding*/ } },
   // Blue sphere
       { XMFLOAT4( 2.0f, 1.0f, -16.0f, 1.0f ), 2.0f, { XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.0f, 0.0f, 0.8f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), 20.0f, 1.0f, 0.2f, /*padding*/ } },
   // Green sphere
       { XMFLOAT4( -2.0f, 0.25f, -6.0f, 1.0f ), 1.25f, { XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.0f, 0.8f, 0.0f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), 20.0f, 1.0f, 0.2f, /*padding*/ } },
   // Red sphere
       { XMFLOAT4( 3.0f, 0.0f, -8.0f, 1.0f ), 1.0f, { XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.8f, 0.0f, 0.0f, 1.0f ), XMFLOAT4( 0.8f, 0.8f, 0.8f, 1.0f ), 20.0f, 1.0f, 0.2f, /*padding*/ } }
   };

	// Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
	D3D11_BUFFER_DESC inputDescSpheres;
   inputDescSpheres.Usage = D3D11_USAGE_DEFAULT;
   inputDescSpheres.ByteWidth = sizeof(Sphere) * NUM_SPHERES;
   inputDescSpheres.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   inputDescSpheres.CPUAccessFlags = 0;
   inputDescSpheres.StructureByteStride = sizeof(Sphere);
   inputDescSpheres.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   D3D11_SUBRESOURCE_DATA vinitDataSpheres;
   vinitDataSpheres.pSysMem = &spheres[0];

   ID3D11Buffer* bufferSpheres = 0;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescSpheres, &vinitDataSpheres, &bufferSpheres) );

   D3D11_SHADER_RESOURCE_VIEW_DESC srvDescSpheres;
   srvDescSpheres.Format = DXGI_FORMAT_UNKNOWN;
   srvDescSpheres.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   srvDescSpheres.BufferEx.FirstElement = 0;
   srvDescSpheres.BufferEx.Flags = 0;
   srvDescSpheres.BufferEx.NumElements = NUM_SPHERES;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferSpheres, &srvDescSpheres, &m_pSpheres) );

   RELEASECOM(bufferSpheres);

      // ----------------------------------------- pointlights -----------------------------

   #define NUM_LIGHTS 1

   PointLight lights[NUM_LIGHTS] = {
       { XMFLOAT4(0.0f, 5.0f, -5.0f, 1.0f), XMFLOAT4( 1.0f, 1.0f, 1.0f, 1.0f )}
   };

	// Create a buffer to be bound as a shader input (D3D11_BIND_SHADER_RESOURCE).
	D3D11_BUFFER_DESC inputDescLights;
   inputDescLights.Usage = D3D11_USAGE_DEFAULT;
   inputDescLights.ByteWidth = sizeof(PointLight) * NUM_LIGHTS;
   inputDescLights.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   inputDescLights.CPUAccessFlags = 0;
   inputDescLights.StructureByteStride = sizeof(PointLight);
   inputDescLights.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

   D3D11_SUBRESOURCE_DATA vinitDataLights;
   vinitDataLights.pSysMem = &lights[0];

   ID3D11Buffer* bufferLights = 0;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&inputDescLights, &vinitDataLights, &bufferLights) );

   D3D11_SHADER_RESOURCE_VIEW_DESC srvDescLights;
   srvDescLights.Format = DXGI_FORMAT_UNKNOWN;
   srvDescLights.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
   srvDescLights.BufferEx.FirstElement = 0;
   srvDescLights.BufferEx.Flags = 0;
   srvDescLights.BufferEx.NumElements = NUM_LIGHTS;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(bufferLights, &srvDescLights, &m_pPointLights) );

   RELEASECOM(bufferLights);

   // create texture 2d and pass it to the quad -------------------------------------------------------------------------------------------
   m_pQuad->Init( 11.6f, 8.3f );


   int quadWidth = 640;
   int quadHeight = 480;
   m_tabPixels = new float[quadWidth*quadHeight*4];

   D3D11_SUBRESOURCE_DATA initDataQuad;
   initDataQuad.pSysMem = &m_tabPixels[0];
   initDataQuad.SysMemPitch = sizeof(float)*quadWidth*4;

   D3D11_TEXTURE2D_DESC descQuad;
   descQuad.Width = quadWidth;
   descQuad.Height = quadHeight;
   descQuad.MipLevels = 1;
   descQuad.ArraySize = 1;
   descQuad.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
   descQuad.SampleDesc.Count = 1;
   descQuad.SampleDesc.Quality = 0;
   descQuad.Usage = D3D11_USAGE_DYNAMIC;
   descQuad.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   descQuad.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
   descQuad.MiscFlags = 0;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateTexture2D( &descQuad, &initDataQuad, &m_pTexture ) );

   D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = descQuad.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	viewDesc.Texture2DArray.MostDetailedMip = 0;
	viewDesc.Texture2DArray.MipLevels = descQuad.MipLevels;
	viewDesc.Texture2DArray.FirstArraySlice = 0;
	viewDesc.Texture2DArray.ArraySize = descQuad.ArraySize;

	HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateShaderResourceView(m_pTexture, &viewDesc, &m_pTextureSRV) );

   m_pQuad->SetTextureSRV( m_pTextureSRV );

   // ----------------------------- output texture -------------------

   // Create a read-write buffer the compute shader can write to (D3D11_BIND_UNORDERED_ACCESS).
   D3D11_BUFFER_DESC outputDescTexture;
   outputDescTexture.Usage = D3D11_USAGE_DEFAULT;
   outputDescTexture.ByteWidth = sizeof(XMFLOAT4) * numPixels;
   outputDescTexture.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
   outputDescTexture.CPUAccessFlags = 0;
   outputDescTexture.StructureByteStride = sizeof(XMFLOAT4);
   outputDescTexture.MiscFlags = 0;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDescTexture, 0, &m_pOutputTextureBuffer) );

   // Create a system memory version of the buffer to read the results back from.
   outputDescTexture.Usage = D3D11_USAGE_STAGING;
   outputDescTexture.BindFlags = 0;
   outputDescTexture.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateBuffer(&outputDescTexture, 0, &m_pOutputTextureDebugBuffer) );

   D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescTexture;
   uavDescTexture.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
   uavDescTexture.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
   uavDescTexture.Buffer.FirstElement = 0;
   uavDescTexture.Buffer.Flags = 0;
   uavDescTexture.Buffer.NumElements = numPixels;

   HR( m_pSystem->m_pDirectx->m_pD3DDevice->CreateUnorderedAccessView(m_pOutputTextureBuffer, &uavDescTexture, &m_pTextureUAV) );

}

void CGPURaytracerOld::Render()
{
   D3DX11_TECHNIQUE_DESC techDesc;

 //  CEffects::RayTracerOldFX->SetVectorsA(m_pInput1SRV);
	////CEffects::RayTracerOldFX->SetVectorsB(m_pInput2SRV);
	//CEffects::RayTracerOldFX->SetOutput(m_pOutputUAV);

   CEffects::RayTracerOldFX->SetDirections(m_pDirections);
   CEffects::RayTracerOldFX->SetPositions(m_pPositions);
   CEffects::RayTracerOldFX->SetStacks(m_pStack);
   CEffects::RayTracerOldFX->SetSpheres(m_pSpheres);
   CEffects::RayTracerOldFX->SetPointLights(m_pPointLights);
   CEffects::RayTracerOldFX->SetTextureOut(m_pTextureUAV);

	CEffects::RayTracerOldFX->RayTraceTech->GetDesc( &techDesc );
	for(UINT p = 0; p < techDesc.Passes; ++p)
	{
		ID3DX11EffectPass* pass = CEffects::RayTracerOldFX->RayTraceTech->GetPassByIndex(p);
      pass->Apply(0, m_pSystem->m_pDirectx->m_pD3DContext);

		m_pSystem->m_pDirectx->m_pD3DContext->Dispatch(20, 15, 1);
	}

	// Unbind the input textures from the CS for good housekeeping.
	ID3D11ShaderResourceView* nullSRV[1] = { 0 };
	m_pSystem->m_pDirectx->m_pD3DContext->CSSetShaderResources( 0, 1, nullSRV );

	// Unbind output from compute shader (we are going to use this output as an input in the next pass, 
	// and a resource cannot be both an output and input at the same time.
	ID3D11UnorderedAccessView* nullUAV[1] = { 0 };
	m_pSystem->m_pDirectx->m_pD3DContext->CSSetUnorderedAccessViews( 0, 1, nullUAV, 0 );

	// Disable compute shader.
	m_pSystem->m_pDirectx->m_pD3DContext->CSSetShader(0, 0, 0);

	// Copy the output buffer to system memory.
   m_pSystem->m_pDirectx->m_pD3DContext->CopyResource(m_pOutputTextureDebugBuffer, m_pOutputTextureBuffer);

	// Map the data for reading.
	D3D11_MAPPED_SUBRESOURCE mappedData; 
   m_pSystem->m_pDirectx->m_pD3DContext->Map(m_pOutputTextureDebugBuffer, 0, D3D11_MAP_READ, 0, &mappedData);

	XMFLOAT4* dataView = reinterpret_cast<XMFLOAT4*>(mappedData.pData);
	//float* dataView = reinterpret_cast<float*>(mappedData.pData);
   //std::vector<float> *dataView = reinterpret_cast<std::vector<float>*>(mappedData.pData);

 //  int cnt = 0;
	//for(int i = 0; i < 640*480; ++i)
	//{
 //     //tmp[i] = XMFLOAT4( dataView[i], dataView[i+1], dataView[i+2], dataView[i+3] );
 //     m_tabPixels[cnt++] = dataView[i].x;
 //     m_tabPixels[cnt++] = dataView[i].y;
 //     m_tabPixels[cnt++] = dataView[i].z;
 //     m_tabPixels[cnt++] = 1.0f;;
	//}
   //XMFLOAT4 testcolor1 = *dataView;
   //XMFLOAT4 testcolor2 = *(dataView+1);
   //XMFLOAT4 testcolor3 = *(dataView+2);


 //  float *positionBuffer = new float[640*480*4];
 //  for(int i = 0; i < 640*480*4; ++i)
	//{
 //     positionBuffer[i] = dataView[i];
	//}
  


   m_pSystem->m_pDirectx->m_pD3DContext->Unmap(m_pOutputTextureDebugBuffer, 0);
   
      //m_tabPixels[cnt++] = dataView[i].x;
      //m_tabPixels[cnt++] = dataView[i].y;
      //m_tabPixels[cnt++] = dataView[i].z;




   //map unmap texture data
   D3D11_MAPPED_SUBRESOURCE mappedResource;
   m_pSystem->m_pDirectx->m_pD3DContext->Map(m_pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

   memcpy( mappedResource.pData, mappedData.pData, sizeof(float)*4*640*480 );
   
   m_pSystem->m_pDirectx->m_pD3DContext->Unmap(m_pTexture, 0);

   m_pQuad->Render();
}