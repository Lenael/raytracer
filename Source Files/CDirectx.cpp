#include <d3d11.h>
#include "../Header Files/CDirectx.h"
#include "../Header Files/CEngineApp.h"
#include "../Header Files/Common.h"

using namespace DirectX;

CDirectx::CDirectx()
{
   m_b4XMsAAEnabled  = false;
   m_bWindowed       = true;
   m_pD3DContext     = NULL;
   m_pD3DDevice      = NULL;
   m_pSwapChain      = NULL;
   m_pSystem         = NULL;
}

CDirectx::CDirectx( CSystem* A_pSystem )
{
   m_b4XMsAAEnabled  = false;
   m_bWindowed       = true;
   m_pD3DContext     = NULL;
   m_pD3DDevice      = NULL;
   m_pSwapChain      = NULL;
   m_pSystem         = A_pSystem;
}

bool CDirectx::Init()
{
   UINT uiCreateDeviceFlags = 0;

#if defined( DEBUG ) || defined ( _DEBUG )
   uiCreateDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

   D3D_FEATURE_LEVEL featureLevel;
   HRESULT hr = E_FAIL;

   hr = D3D11CreateDevice(
         0, //default adapter
         D3D_DRIVER_TYPE_HARDWARE,
         0, //no software device
         uiCreateDeviceFlags,
         0, 0,   //default feature level array
         D3D11_SDK_VERSION,
         &m_pD3DDevice,
         &featureLevel,
         &m_pD3DContext
      );

   if( FAILED( hr ) )
   {
      MessageBox( 0, "D3D11CreateDevice() failed.", 0, 0 );
      return false;
   }

   if( featureLevel != D3D_FEATURE_LEVEL_11_0 )
   {
      MessageBox( 0, "D3D_FEATURE_LEVEL_11_0 not supported.", 0, 0 );
      return false;
   }

   if( FAILED( m_pD3DDevice->CheckMultisampleQualityLevels( DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m_ui4xMsAAQuality ) ) )
   {
      MessageBox( 0, "DXGI_FORMAT_R8G8B8A8_UNORM multisampling quality not supported.", 0, 0 );
      m_b4XMsAAEnabled = false;
   }
   else
      m_b4XMsAAEnabled = true;


   DXGI_SWAP_CHAIN_DESC sd;
   unsigned uiWidth = m_pSystem->m_pEngine->GetViewWidth();
   unsigned uiHeight = m_pSystem->m_pEngine->GetViewHeight();
   HWND hWnd = m_pSystem->m_pEngine->GetHwnd();

   sd.BufferDesc.Width                    = uiWidth;
   sd.BufferDesc.Height                   = uiHeight;
   sd.BufferDesc.RefreshRate.Numerator    = 60;
   sd.BufferDesc.RefreshRate.Denominator  = 1;
   sd.BufferDesc.Format                   = DXGI_FORMAT_R8G8B8A8_UNORM; //DXGI_FORMAT_R16G16B16A16_FLOAT; //
   sd.BufferDesc.ScanlineOrdering         = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
   sd.BufferDesc.Scaling                  = DXGI_MODE_SCALING_UNSPECIFIED;

   if( m_b4XMsAAEnabled )
   {
      sd.SampleDesc.Count     = 4;
      sd.SampleDesc.Quality   = m_ui4xMsAAQuality - 1;
   }
   else
   {
      sd.SampleDesc.Count     = 1;
      sd.SampleDesc.Quality   = 0;
   }

   sd.BufferUsage    = DXGI_USAGE_RENDER_TARGET_OUTPUT;
   sd.BufferCount    = 1;
   sd.OutputWindow   = hWnd;
   sd.Windowed       = m_bWindowed;
   sd.SwapEffect     = DXGI_SWAP_EFFECT_DISCARD;
   sd.Flags          = 0;

   IDXGIDevice* dxgiDevice = 0;
	if( FAILED( m_pD3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice) ) )
   {
      MessageBox( hWnd, "QueryInterface(__uuidof(IDXGIDevice), ... ) failed.", 0, 0 );
      return false;
   }
	      
	IDXGIAdapter* dxgiAdapter = 0;
	if( FAILED( dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter) ) )
   {
      MessageBox( hWnd, "QueryInterface(__uuidof(IDXGIAdapter), ... ) failed.", 0, 0 );
      return false;
   }

	IDXGIFactory* dxgiFactory = 0;
	if( FAILED(dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory)))
   {
      MessageBox( hWnd, "QueryInterface(__uuidof(IDXGIFactory), ... ) failed.", 0, 0 );
      return false;
   }

	if( FAILED( dxgiFactory->CreateSwapChain(m_pD3DDevice, &sd, &m_pSwapChain)) )
   {
      MessageBox( hWnd, "dxgiFactory->CreateSwapChain(...) failed.", 0, 0 );
      return false;
   }
	
	RELEASECOM(dxgiDevice);
	RELEASECOM(dxgiAdapter);
	RELEASECOM(dxgiFactory);

   ID3D11Texture2D* backBuffer;
   m_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer) );
   m_pD3DDevice->CreateRenderTargetView(backBuffer, 0, &m_pRenderTargetView);
   RELEASECOM( backBuffer );

   D3D11_TEXTURE2D_DESC depthStencilDesc;
   depthStencilDesc.Width     = uiWidth;
   depthStencilDesc.Height    = uiHeight;
   depthStencilDesc.MipLevels = 1;
   depthStencilDesc.ArraySize = 1;
   depthStencilDesc.Format    = DXGI_FORMAT_D24_UNORM_S8_UINT;

   if( m_b4XMsAAEnabled )
   {
      depthStencilDesc.SampleDesc.Count   = 4;
      depthStencilDesc.SampleDesc.Quality = m_ui4xMsAAQuality-1;
   }
   else
   {
      depthStencilDesc.SampleDesc.Count   = 1;
      depthStencilDesc.SampleDesc.Quality = 0;
   }

   depthStencilDesc.Usage     = D3D11_USAGE_DEFAULT;
   depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
   depthStencilDesc.CPUAccessFlags  = 0;
   depthStencilDesc.MiscFlags       = 0;

   if( FAILED( m_pD3DDevice->CreateTexture2D( &depthStencilDesc, 0, &m_pDepthStencilBuffer ) ) )
   {
      MessageBox( hWnd, "Create depth-stencil buffer failed.", 0, 0 );
      return false;
   }

   if( FAILED( m_pD3DDevice->CreateDepthStencilView( m_pDepthStencilBuffer, 0, &m_pDepthStencilView ) ) )
   {
      MessageBox( hWnd, "Create depth-stencil view failed.", 0, 0 );
      return false;
   }

   m_pD3DContext->OMSetRenderTargets( 1, &m_pRenderTargetView, m_pDepthStencilView );

   m_ScreenViewport.TopLeftX = 0.0f;
   m_ScreenViewport.TopLeftY = 0.0f;
   m_ScreenViewport.Width    = static_cast<float>( uiWidth );
   m_ScreenViewport.Height   = static_cast<float>( uiHeight );
   m_ScreenViewport.MinDepth = 0.0f;
   m_ScreenViewport.MaxDepth = 1.0f;

   m_pD3DContext->RSSetViewports( 1, &m_ScreenViewport );

return true;
}

void CDirectx::Resize()
{
   RELEASECOM(m_pRenderTargetView);
	RELEASECOM(m_pDepthStencilView);
	RELEASECOM(m_pDepthStencilBuffer);

   HRESULT hRet = E_FAIL;
   unsigned int width = m_pSystem->m_pEngine->GetViewWidth();
   unsigned int height = m_pSystem->m_pEngine->GetViewHeight();

	// Resize the swap chain and recreate the render target view.

   hRet = m_pSwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	
   ID3D11Texture2D* backBuffer;
	
   hRet = (m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer)));
	hRet = m_pD3DDevice->CreateRenderTargetView(backBuffer, 0, &m_pRenderTargetView);

	RELEASECOM(backBuffer);

   if( FAILED( hRet ) )
   {
      MessageBox( m_pSystem->m_pEngine->GetHwnd(), "Resize the swap chain and recreate the render target view failed while resizing.", 0, 0 );
      return;
   }

	// Create the depth/stencil buffer and view.

	D3D11_TEXTURE2D_DESC depthStencilDesc;
	
	depthStencilDesc.Width     = width;
	depthStencilDesc.Height    = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format    = DXGI_FORMAT_D24_UNORM_S8_UINT;

	// Use 4X MSAA? --must match swap chain MSAA values.
	if( m_b4XMsAAEnabled )
	{
		depthStencilDesc.SampleDesc.Count   = 4;
		depthStencilDesc.SampleDesc.Quality = m_ui4xMsAAQuality-1;
	}
	// No MSAA
	else
	{
		depthStencilDesc.SampleDesc.Count   = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	}

	depthStencilDesc.Usage          = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags      = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0; 
	depthStencilDesc.MiscFlags      = 0;

	hRet = m_pD3DDevice->CreateTexture2D(&depthStencilDesc, 0, &m_pDepthStencilBuffer);
	hRet = m_pD3DDevice->CreateDepthStencilView(m_pDepthStencilBuffer, 0, &m_pDepthStencilView);

   if( FAILED( hRet ) )
   {
      MessageBox( m_pSystem->m_pEngine->GetHwnd(), "Create the depth/stencil buffer and view failed while resizing.", 0, 0 );
      return;
   }

	// Bind the render target view and depth/stencil view to the pipeline.

	m_pD3DContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);
	

	// Set the viewport transform.

	m_ScreenViewport.TopLeftX = 0;
	m_ScreenViewport.TopLeftY = 0;
	m_ScreenViewport.Width    = static_cast<float>(width);
	m_ScreenViewport.Height   = static_cast<float>(height);
	m_ScreenViewport.MinDepth = 0.0f;
	m_ScreenViewport.MaxDepth = 1.0f;

	m_pD3DContext->RSSetViewports(1, &m_ScreenViewport);

   m_pSystem->m_pCamera->SetLens(0.25f*XM_PI, static_cast<float>(m_pSystem->m_pEngine->GetViewWidth())/static_cast<float>(m_pSystem->m_pEngine->GetViewHeight()), 1.0f, 3000.0f);
}

void CDirectx::Present()
{
   m_pSwapChain->Present(0,0);
}