#include "../Header Files/CSphere.h"


CSphere::CSphere( CSystem* A_pSystem, const XMFLOAT3 &A_f3Position, const float &A_fRadius, const XMFLOAT3 &A_f3Color, 
		            const float &A_fReflection, const float &A_fTransparency, const XMFLOAT3 &A_f3EmissionColor):
                  m_pSystem(A_pSystem), m_f3Position(A_f3Position), m_fR(A_fRadius), m_fSquareR(A_fRadius*A_fRadius),
                  m_f3Color(A_f3Color), m_fReflection(A_fReflection), m_fTransparency(A_fTransparency),
                  m_f3EmissionColor(A_f3EmissionColor)
		            
{
}

bool CSphere::Intersect(const XMFLOAT3 &A_f3RayOrigin, const XMFLOAT3 &A_f3RayDirection, float *t0, float *t1)
{
   if (m_pSystem->m_bGeometricCollision)
   {
      XMVECTOR xmcenter = XMLoadFloat3(&m_f3Position);
      XMVECTOR xmrayorig = XMLoadFloat3(&A_f3RayOrigin);
      XMVECTOR xmraydir = XMLoadFloat3(&A_f3RayDirection);
      XMVECTOR l = xmcenter - xmrayorig;
      float tca = XMVectorGetX(XMVector3Dot(l, xmraydir));

      if (tca < 0)
         // means that raydir and spherecenterdir points in opposite directioins
         return false;

      l = XMVector3Dot(l, l);

      // dkwadrat to odleglosc od srodka sfery prostopadle do kierunku  raydir
      float d2 = XMVectorGetX(l) - tca * tca;

      // wtedy ray jest poza sfera
      if (d2 > m_fSquareR)
         return false;

      float thc = sqrt(m_fSquareR - d2);

      if (t0 != NULL && t1 != NULL)
      {
         *t0 = tca - thc;
         *t1 = tca + thc;
      }

      return true;
   }
   else
   {
      //analytic solution
      XMVECTOR xmcenter = XMLoadFloat3(&m_f3Position);
      XMVECTOR xmrayorig = XMLoadFloat3(&A_f3RayOrigin);
      XMVECTOR xmraydir = XMLoadFloat3(&A_f3RayDirection);
      XMVECTOR l = xmrayorig - xmcenter;

      if (XMVectorGetX(XMVector3Dot(xmcenter - xmrayorig, xmraydir)) < 0)
         // means that raydir and spherecenterdir points in opposite directioins
         return false;

      float a = XMVectorGetX(XMVector3Dot(xmraydir, xmraydir));
      float b = 2 * XMVectorGetX(XMVector3Dot(xmraydir, l));
      float c = XMVectorGetX(XMVector3Dot(l, l)) - m_fSquareR;

      if (!SolveQuadratic(a, b, c, t0, t1))
         return false;

      //if (*t0 < 0)
      //{
      //   *t0 = *t1;
      //   if (*t0 < 0) return false;
      //}

      if (*t0 < 0 && *t1 < 0)
         return false;

      return true;
   }
}

bool CSphere::SolveQuadratic(const float &a, const float &b, const float &c, float *x0, float *x1)
{
   float discr = b * b - 4 * a * c;
   if (discr < 0)
      return false;
   else if (discr == 0)
      *x0 = *x1 = -0.5f * b / a;
   else
   {
      float q = (b > 0) ?
         -0.5f * (b + sqrt(discr)) :
         -0.5f * (b - sqrt(discr));
      *x0 = q / a;
      *x1 = c / q;
   }

   if (*x0 > *x1)
      std::swap(*x0, *x1);

   return true;
}

void CSphere::SetPosition( float A_fX, float A_fY, float A_fZ )
{
   m_f3Position.x = A_fX;
   m_f3Position.y = A_fY;
   m_f3Position.z = A_fZ;
}

void CSphere::Move( float A_fX, float A_fY, float A_fZ )
{
   m_f3Position.x += A_fX;
   m_f3Position.y += A_fY;
   m_f3Position.z += A_fZ;
}