#include "../Header Files/CEngineApp.h"
#include <CommCtrl.h>
#include <windowsx.h>
#include <atlstr.h>

CEngineApp::CEngineApp()
{
   m_hWnd            = NULL;
   m_uiWidth         = 640;
   m_uiHeight        = 480;
   m_uiSettingsDialogWidth    = 300;
   m_uiSettingsDialogHeight   = 274;

	m_pSystem			= new CSystem( this );
}

CEngineApp::~CEngineApp()
{
	Shutdown( );
}

bool CEngineApp::InitInstance( HANDLE hInstance, LPCTSTR lpCmdLine, int iCmdShow )
{
   if ( !CreateDisplay() )
	{
		Shutdown();
		return false;
	}

   // Set up all required game states
   if( m_pSystem->Init() == false )
   {
      Shutdown();
      return false;
   }

   return true;
}

bool CEngineApp::CreateDisplay()
{
   LPTSTR  WindowTitle		= "Praca Magisterska WFiIS AGH";
   unsigned short  Width	= m_uiWidth-6;
   unsigned short  Height	= m_uiHeight;
   HDC     hDC				   = NULL;
   RECT    rc;
    
   WNDCLASS	wc;	
	wc.style			   = CS_BYTEALIGNCLIENT | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= StaticWndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= (HINSTANCE)GetModuleHandle(NULL);
	wc.hIcon			   = NULL; //LoadIcon( wc.hInstance, MAKEINTRESOURCE(IDI_ICON));
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH )GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName	= NULL;
	wc.lpszClassName	= WindowTitle;
	RegisterClass(&wc);

	m_hWnd = CreateWindow( WindowTitle, WindowTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 
                           CW_USEDEFAULT, Width, Height, NULL, LoadMenu( wc.hInstance, NULL ),
                           wc.hInstance, this );
	
    if ( !m_hWnd ) return false;

    ::GetClientRect( m_hWnd, &rc );
    m_uiViewX      = rc.left;
    m_uiViewY      = rc.top;
    m_uiViewWidth  = rc.right - rc.left;
    m_uiViewHeight = rc.bottom - rc.top;
    
	ShowWindow(m_hWnd, SW_SHOW);
   

   m_hNormalFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

   CreateSettingsDialog();

return true;
}

bool CEngineApp::CreateTabControl()
{
   // Tab Control
   m_hSettingsTabControl = CreateWindowEx( 0, WC_TABCONTROL, 0, WS_CHILD | WS_VISIBLE | TCS_FIXEDWIDTH,
               0, 0, m_uiSettingsDialogWidth, m_uiSettingsDialogHeight, m_hWndSettings, 0, NULL, NULL );
    
   //TabCtrl_SetItemSize(m_hSettingsTabControl, 150, 20 );

   TCITEM item_main, item_additional_settings;

#define ID_TAB_MAIN        6661
#define ID_TAB_ADDITIONAL  6662

   item_main.mask       = TCIF_TEXT;
   item_main.pszText    = "Scena";
   item_main.cchTextMax = sizeof( "Scena" );
   
   item_additional_settings.mask       = TCIF_TEXT;
   item_additional_settings.pszText    = "Ustawienia";
   item_additional_settings.cchTextMax = sizeof( "Ustawienia" );

   TabCtrl_InsertItem( m_hSettingsTabControl, ID_TAB_MAIN, &item_main );
   TabCtrl_InsertItem( m_hSettingsTabControl, ID_TAB_ADDITIONAL, &item_additional_settings );


   //create main objects group
   m_hVisibleObjectsGroupBox = CreateWindowEx (0, "button", "Wyb�r sceny", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
            10, 25, 270, 140, m_hSettingsTabControl, NULL, NULL, NULL);

   HWND hScene1 = CreateWindowEx(0, "button", "Scena 1", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP,
            20, 40, 100, 20, m_hWndSettings, (HMENU)601, NULL, NULL);
   HWND hScene2 = CreateWindowEx(0, "button", "Scena 2", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 60, 100, 20, m_hWndSettings, (HMENU)602, NULL, NULL);
   HWND hScene3 = CreateWindowEx(0, "button", "Scena 3", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 80, 100, 20, m_hWndSettings, (HMENU)603, NULL, NULL);
   HWND hScene4 = CreateWindowEx(0, "button", "Scena 4", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 100, 100, 20, m_hWndSettings, (HMENU)604, NULL, NULL);
   HWND hScene5 = CreateWindowEx(0, "button", "Scena 5", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 120, 100, 20, m_hWndSettings, (HMENU)605, NULL, NULL);
   HWND hScene6 = CreateWindowEx(0, "button", "Scena 6", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 140, 100, 20, m_hWndSettings, (HMENU)606, NULL, NULL);

   m_hRenderStateGroupBox = CreateWindowEx (0, "button", "Typ renderowania", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
            10, 170, 270, 60, m_hSettingsTabControl, NULL, NULL, NULL);

   HWND hCPU = CreateWindowEx(0, "button", "CPU", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP,
            20, 185, 100, 20, m_hWndSettings, (HMENU)607, NULL, NULL);
   HWND hGPU = CreateWindowEx (0, "button", "GPU", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 205, 100, 20, m_hWndSettings, (HMENU)608, NULL, NULL);

   SendMessage(m_hSettingsTabControl, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(m_hVisibleObjectsGroupBox, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hScene1, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hScene2, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hScene3, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hScene4, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hScene5, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hScene6, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(m_hRenderStateGroupBox, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hCPU, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hGPU, WM_SETFONT, (WPARAM)m_hNormalFont, 0);

   CheckRadioButton(m_hWndSettings, 601, 606, 601);
   
   if( m_pSystem->m_bRenderByCPU )
      CheckRadioButton(m_hWndSettings, 607, 608, 607);
   else
      CheckRadioButton(m_hWndSettings, 607, 608, 608);


   //create tesselation group
   m_hCollisionTypeGroupBox = CreateWindowEx (0, "button", "Sposob liczenia kolizji", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
            10, 25, 270, 60, m_hSettingsTabControl, NULL, NULL, NULL);

   HWND hGeometric = CreateWindowEx (0, "button", "Geometryczny", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 40, 100, 20, m_hWndSettings, (HMENU)701, NULL, NULL);
   HWND hAnalytic = CreateWindowEx (0, "button", "Analityczny", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            20, 60, 100, 20, m_hWndSettings, (HMENU)702, NULL, NULL);
   HWND hLabel = CreateWindow("STATIC", "Wsp. zalamania swiatla:", WS_VISIBLE | WS_CHILD | SS_LEFT,
            10, 100, 180, 20, m_hWndSettings, (HMENU)703, NULL, NULL);
   HWND hRefractiveIndex = CreateWindow("EDIT", "", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_AUTOHSCROLL | ES_WANTRETURN,
            135, 95, 100, 20, m_hWndSettings, (HMENU)704, NULL, NULL);
   
   SendMessage(m_hCollisionTypeGroupBox, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hGeometric, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hAnalytic, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hLabel, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   SendMessage(hRefractiveIndex, WM_SETFONT, (WPARAM)m_hNormalFont, 0);
   
   CheckRadioButton(m_hWndSettings, 701, 702, 701);
   
   ShowWindow( m_hCollisionTypeGroupBox, SW_HIDE);
   ShowWindow( GetDlgItem(m_hWndSettings, 701 ), SW_HIDE );
   ShowWindow(GetDlgItem(m_hWndSettings, 702), SW_HIDE);
   ShowWindow(GetDlgItem(m_hWndSettings, 703), SW_HIDE);
   ShowWindow(GetDlgItem(m_hWndSettings, 704), SW_HIDE);

   return true;
}

bool CEngineApp::CreateSettingsDialog()
{
   LPTSTR  WindowTitle		= "Ustawienia";
   unsigned short  Width	= m_uiSettingsDialogWidth;
   unsigned short  Height	= m_uiSettingsDialogHeight;
   HDC     hDC				   = NULL;
    
   WNDCLASS	wc;	
	wc.style			   = CS_BYTEALIGNCLIENT | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= StaticSettingsWndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= (HINSTANCE)m_hWnd;
	wc.hIcon			   = NULL;
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH )GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName	= NULL;
	wc.lpszClassName	= WindowTitle;
	RegisterClass(&wc);

	m_hWndSettings = CreateWindow( WindowTitle, WindowTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX, CW_USEDEFAULT, 
                           CW_USEDEFAULT, Width, Height, NULL, LoadMenu( wc.hInstance, NULL ),
                           wc.hInstance, this );
	
    if ( !m_hWndSettings ) return false;
   
   CreateTabControl();

	ShowWindow(m_hWndSettings, SW_SHOW);

return true;
}

int CEngineApp::BeginGame()
{
   MSG		msg;

   m_pSystem->PreMessageLoop();

	while ( true ) 
   {
      // Did we recieve a message, or are we idling ?
      if ( PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) ) 
      {
	      if  (msg.message == WM_QUIT ) break;
	      TranslateMessage( &msg );
	      DispatchMessage ( &msg );
      } 
      else 
      {
	      // Advance Game Frame.
         m_pSystem->Render();
      }
	
   }

    return 0;
}

LRESULT CALLBACK CEngineApp::StaticWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   // If this is a create message, trap the 'this' pointer passed in and store it within the window.
   if ( Message == WM_CREATE ) SetWindowLong( hWnd, GWL_USERDATA, (LONG)((CREATESTRUCT FAR *)lParam)->lpCreateParams);

   // Obtain the correct destination for this message
   CEngineApp *Destination = (CEngineApp*)GetWindowLong( hWnd, GWL_USERDATA );
    
   // If the hWnd has a related class, pass it through
   if (Destination) return Destination->DisplayWndProc( hWnd, Message, wParam, lParam );
    
   // No destination found, defer to system...
   return DefWindowProc( hWnd, Message, wParam, lParam );
}

LRESULT CEngineApp::DisplayWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam )
{
	switch (Message)
   {
      case WM_CREATE:
      break;
		
      case WM_CLOSE:
		   PostQuitMessage(0);
		break;
		
      case WM_DESTROY:
		   PostQuitMessage(0);
		break;

      case WM_LBUTTONDOWN:
	   case WM_MBUTTONDOWN:
	   case WM_RBUTTONDOWN:
		   m_pSystem->OnMouseDown(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		   return 0;
	   case WM_LBUTTONUP:
	   case WM_MBUTTONUP:
	   case WM_RBUTTONUP:
		   m_pSystem->OnMouseUp(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		   return 0;
	   case WM_MOUSEMOVE:
		   m_pSystem->OnMouseMove(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		   return 0;

	   case WM_ACTIVATE:
		   if( LOWORD(wParam) == WA_INACTIVE )
		   {
            m_pSystem->OnEngineInactive();
		   }
		   else
		   {
            m_pSystem->OnEngineActive();
		   }
      break;
		
      case WM_SIZE:

         // Store new viewport sizes
         m_uiViewWidth  = LOWORD( lParam );
         m_uiViewHeight = HIWORD( lParam );
        
         if( m_pSystem->IsEngineRunning() )
         {
            if( wParam == SIZE_MINIMIZED )
            {
               m_pSystem->OnSizeMinimized();
            }
            else if( wParam == SIZE_MAXIMIZED )
            {
               m_pSystem->OnSizeMaximized();
            }
            else if( wParam == SIZE_RESTORED )
            {
               m_pSystem->OnSizeRestored();
            }

         }
		break;

	   case WM_ENTERSIZEMOVE:
         m_pSystem->OnEnterSizeMove();
      break;
      
	   case WM_EXITSIZEMOVE:
         m_pSystem->OnExitSizeMove();
      break;

      case WM_KEYDOWN:

         // Input...
		switch (wParam) 
      {
			case VK_ESCAPE:
				PostQuitMessage(0);
				return 0;
		}
		break;

		default:
			return DefWindowProc(hWnd, Message, wParam, lParam);
   }
    
   return 0;
}

LRESULT CEngineApp::StaticSettingsWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam )
{
      // If this is a create message, trap the 'this' pointer passed in and store it within the window.
   if ( Message == WM_CREATE ) SetWindowLong( hWnd, GWL_USERDATA, (LONG)((CREATESTRUCT FAR *)lParam)->lpCreateParams);

   // Obtain the correct destination for this message
   CEngineApp *Destination = (CEngineApp*)GetWindowLong( hWnd, GWL_USERDATA );
    
   // If the hWnd has a related class, pass it through
   if (Destination) return Destination->SettingsWndProc( hWnd, Message, wParam, lParam );
    
   // No destination found, defer to system...
   return DefWindowProc( hWnd, Message, wParam, lParam );
}

LRESULT CEngineApp::SettingsWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam )
{
	switch (Message)
   {
      case WM_CREATE:
      {

      }
      break;
		
      case WM_CLOSE:
		   PostQuitMessage(0);
		break;

      case WM_NOTIFY:
         LPNMHDR n;
         n = (LPNMHDR)lParam;
         if( n->code == TCN_SELCHANGE )
         {
            int index = TabCtrl_GetCurSel(m_hSettingsTabControl);
            switch(index) // index if current control
            { 
               // tab Main
               case 0: 
                  // main tab
                  ShowWindow(m_hVisibleObjectsGroupBox, SW_SHOW);
                  ShowWindow(m_hRenderStateGroupBox, SW_SHOW);
                  ShowWindow( GetDlgItem(hWnd, 601 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 601 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 602 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 602 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 603 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 603 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 604 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 604 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 605 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 605 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 606 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 606 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 607 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 607 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 608 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 608 ), SW_SHOW );

                  // tab settings
                  ShowWindow(m_hCollisionTypeGroupBox, SW_HIDE);
                  ShowWindow(GetDlgItem(m_hWndSettings, 701), SW_HIDE);
                  ShowWindow(GetDlgItem(m_hWndSettings, 702), SW_HIDE);
                  ShowWindow(GetDlgItem(m_hWndSettings, 703), SW_HIDE);
                  ShowWindow(GetDlgItem(m_hWndSettings, 704), SW_HIDE);
                  
               break;
               // tab Settings
               case 1:
                  // main tab
                  ShowWindow(m_hVisibleObjectsGroupBox, SW_HIDE);
                  ShowWindow(m_hRenderStateGroupBox, SW_HIDE);
                  ShowWindow( GetDlgItem(hWnd, 601 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 602 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 603 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 604 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 605 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 606 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 607 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 608 ), SW_HIDE );

                  // tab settings
                  ShowWindow(m_hCollisionTypeGroupBox, SW_SHOW);
                  ShowWindow( GetDlgItem(hWnd, 701 ), SW_HIDE );
                  ShowWindow( GetDlgItem(hWnd, 701 ), SW_SHOW );
                  ShowWindow( GetDlgItem(hWnd, 702 ), SW_HIDE );
                  ShowWindow(GetDlgItem(hWnd, 702), SW_SHOW);
                  ShowWindow(GetDlgItem(hWnd, 703), SW_HIDE);
                  ShowWindow(GetDlgItem(hWnd, 703), SW_SHOW);
                  ShowWindow(GetDlgItem(hWnd, 704), SW_HIDE);
                  ShowWindow(GetDlgItem(hWnd, 704), SW_SHOW);
               break;
            }
            RedrawWindow( hWnd, NULL, NULL, RDW_ALLCHILDREN );
         }
      break;
      
      case WM_COMMAND:
         if(IsDlgButtonChecked (hWnd, 607) == BST_CHECKED )
            m_pSystem->m_bRenderByCPU = true;
         else
            m_pSystem->m_bRenderByCPU = false;

         //601 - 606 - numeracja scen
         if (IsDlgButtonChecked(hWnd, 601) == BST_CHECKED)
            m_pSystem->m_numSceneNumber = 1;
         else if (IsDlgButtonChecked(hWnd, 602) == BST_CHECKED)
            m_pSystem->m_numSceneNumber = 2;
         else if (IsDlgButtonChecked(hWnd, 603) == BST_CHECKED)
            m_pSystem->m_numSceneNumber = 3;
         else if (IsDlgButtonChecked(hWnd, 604) == BST_CHECKED)
            m_pSystem->m_numSceneNumber = 4;
         else if (IsDlgButtonChecked(hWnd, 605) == BST_CHECKED)
            m_pSystem->m_numSceneNumber = 5;
         else if (IsDlgButtonChecked(hWnd, 606) == BST_CHECKED)
            m_pSystem->m_numSceneNumber = 6;

         //701 - 702 - geometryczny analityczny
         if (IsDlgButtonChecked(hWnd, 701) == BST_CHECKED)
            m_pSystem->m_bGeometricCollision = true;
         else if (IsDlgButtonChecked(hWnd, 702) == BST_CHECKED)
            m_pSystem->m_bGeometricCollision = false;
         
         // wsp. zalamania swiatla
         TCHAR buff[1024];
         GetWindowText(GetDlgItem(hWnd, 704), buff, 1024);

         m_pSystem->m_fRefractiveIndex = (float)atof(buff);

         if (m_pSystem->m_fRefractiveIndex == 0.0f)
            m_pSystem->m_fRefractiveIndex = 0.4f;
		break;

      case WM_CTLCOLORSTATIC:
            return (LONG)GetStockObject(NULL_BRUSH);
		break;
      case WM_DESTROY:
		   PostQuitMessage(0);
		break;

	   case WM_ACTIVATE:
      break;
		
      case WM_SIZE:
		break;

		default:
			return DefWindowProc(hWnd, Message, wParam, lParam);
   }
    
   return 0;
}

bool CEngineApp::Shutdown( )
{
   if ( m_hWnd ) DestroyWindow( m_hWnd );
   if( m_pSystem )
   {
      delete m_pSystem;
      m_pSystem = NULL;
   }
    
   // Clear all variables
   m_hWnd              = NULL;

return true;
}

const HWND CEngineApp::GetHwnd() const
{
   return m_hWnd;
}

const unsigned CEngineApp::GetViewWidth() const
{
   return m_uiViewWidth;
}

const unsigned CEngineApp::GetViewHeight() const
{
   return m_uiViewHeight;
}