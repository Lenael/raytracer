#include "../Header Files/Main.h"
#include "../Header Files/CEngineApp.h"

//-----------------------------------------------------------------------------
// Global Variable Definitions
//-----------------------------------------------------------------------------
CEngineApp    g_App;      // Core game application processing engine



int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int iCmdShow )
{
    int retCode;

	if ( !g_App.InitInstance( hInstance, lpCmdLine, iCmdShow ) )
		return 0;
    
    // Begin the gameplay process. Will return when app due to exit.
    retCode = g_App.BeginGame();

    // Shut down the engine, just to be polite, before exiting.
    if ( !g_App.Shutdown() )
		MessageBox( 0, "Failed to shut system down correctly.\r\n\r\nIf the problem persists, please contact Artur Magdziarz", "Non-Fatal Error", MB_OK | MB_ICONEXCLAMATION );

return retCode;
}