//#include "../Header Files/CFont.h"
//
//
//CFont::CFont()
//{
//	m_pCharset		= new CCharset;
//	m_uMaxChars		= 100;
//	//m_CharInfo		= NULL;
//	m_pTextureRV	= NULL;
//	m_pGame			= NULL;
//	m_mWorld		= new XMFLOAT4X4;
//	XMStoreFloat4x4( m_mWorld, XMMatrixIdentity() );
//	m_vPos		= new XMFLOAT3;
//	m_vPos->x	= 0.0f;
//	m_vPos->y	= 0.0f;
//	m_vPos->z	= -0.1f;
//	m_pVB		= NULL;
//	m_pIB		= NULL;
//	m_iStride	= sizeof(VertexPT);
//	m_iOffset	= 0;
//	//---------------------------------
//	m_fSize			= 5.0f;
//}
//
//CFont::~CFont()
//{
//	if( m_pCharset ) delete m_pCharset;
//	//if( m_CharInfo ) delete m_CharInfo;
//	if( m_pTextureRV ) m_pTextureRV->Release();
//	if( m_pVB ) m_pVB->Release(); 
//	if( m_pIB ) m_pIB->Release();
//	if( m_vPos ) delete m_vPos;
//	//if( m_mWorld ) delete m_mWorld;
//	if( m_pGame ) m_pGame = NULL;
//}
//
//void CFont::Init( const char* fileName, const CGame* pGame )
//{
//	GetGameInstance( pGame );
//	ParseTxtFile( fileName );
//	LoadBitmap();
//	CreateBuffers();
//}
//
//int CFont::ParseTxtFile( const char* fileName )
//{
//	ifstream file; //ofstream - zapisywanie do pliku
//	string	s;
//	unsigned short charsNumber, charID;
//
//	file.open( fileName );
//	if( !file.good() )
//	{
//		file.close();
//		MessageBox( 0, "Can't load font configuration file. Please contact support service.", "Error!", MB_OK | MB_ICONERROR );
//		return 1;
//	}
//	while( file >> s ) //TODO: napisac sprawdzanie, czy wszystkie info zebral
//	{
//		if( s.find( "lineHeight=" ) != string::npos ) //s.find zwraca nr znaku w pliku, gdzie znaleziono
//		{
//			s.erase( 0, 11 );
//			m_pCharset->lineHeight = atoi( s.c_str() );
//			continue;
//		} else
//		if( s.find( "base=" ) != string::npos )
//		{
//			s.erase( 0, 5 );
//			m_pCharset->base = atoi( s.c_str() );
//			continue;
//		} else
//		if( s.find( "scaleW=" ) != string::npos )
//		{
//			s.erase( 0, 7 );
//			m_pCharset->width = atoi( s.c_str() );
//			continue;
//		} else
//		if( s.find( "scaleH=" ) != string::npos )
//		{
//			s.erase( 0, 7 ); //od 0 kasujemy 7 wyrazow
//			m_pCharset->height = atoi( s.c_str() );
//			continue;
//		} else
//		if( s.find( "pages=" ) != string::npos )
//		{
//			s.erase( 0, 6 );
//			m_pCharset->pages = atoi( s.c_str() );
//			continue;
//		} else
//		if( s.find( "file=" ) != string::npos )
//		{
//			s.erase( 0, 6 );
//			s.erase( s.length()-1, s.length() );
//			m_pCharset->bmpName.assign( s );
//			continue;
//		}else 
//		if( s.find( "count=" ) != string::npos )
//		{
//			s.erase( 0, 6 );
//			charsNumber = atoi( s.c_str() );
//			break;
//		}
//	}
//
//	for( int i=0; i<charsNumber; i++ )
//	{
//		getline( file, s ); //przeskok do nowej linii
//		file >> s; //char
//		file >> s; //id=
//		s.erase( 0, 3 );
//		charID = atoi( s.c_str() );
//
//		file >> s; //x=
//		s.erase( 0, 2 );
//		m_pCharset->chars[charID].x = atoi(s.c_str());
//
//		file >> s; //y=
//		s.erase( 0, 2 );
//		m_pCharset->chars[charID].y = atoi(s.c_str());
//
//		file >> s; //width=
//		s.erase( 0, 6 );
//		m_pCharset->chars[charID].width = atoi(s.c_str());
//
//		file >> s; //height=
//		s.erase( 0, 7 );
//		m_pCharset->chars[charID].height = atoi(s.c_str());
//
//		file >> s; //xoffset=
//		s.erase( 0, 8 );
//		m_pCharset->chars[charID].xOffset = atoi(s.c_str());
//
//		file >> s; //yoffset=
//		s.erase( 0, 8 );
//		m_pCharset->chars[charID].yOffset = atoi(s.c_str());
//
//		file >> s; //xadvance=
//		s.erase( 0, 9 );
//		m_pCharset->chars[charID].xAdvance = atoi(s.c_str());
//	}
//
//	
//	//char buff[100];
//	//sprintf( buff, "xOffset = %d\n", m_pCharset->chars['k'].xOffset );
//	//OutputDebugString( buff );
//
//	file.close();
//return 0;
//}
//
//int CFont::LoadBitmap()
//{
//	char* buff = new char[100];
//	sprintf( buff, "Graphics/Fonts/%s", m_pCharset->bmpName.c_str() );
//
//	m_pTextureRV = m_pGame->m_pTexture->LoadTexture( buff );
//	if( !m_pTextureRV )
//	{
//		MessageBox( 0, "Failed to load BitmapFont texture. Please contact support service.", "Error!", MB_OK | MB_ICONERROR );
//		return 1;
//	}
//return 0;
//}
//
//void CFont::CreateBuffers()
//{
//	D3D11_BUFFER_DESC IBDesc, VBDesc;
//	D3D11_SUBRESOURCE_DATA IBData, VBData;
//	unsigned	verNum = m_uMaxChars*4,
//				indNum = m_uMaxChars*6;
//
//	ZeroMemory( &IBDesc, sizeof( IBDesc ) );
//	ZeroMemory( &VBDesc, sizeof( VBDesc ) );
//	ZeroMemory( &IBData, sizeof( IBData ) );
//	ZeroMemory( &VBData, sizeof( VBData ) );
//
//	//--------------Vertex Buffer------------
//	VertexPT* vertices			= new VertexPT[verNum];
//	memset( vertices, 0, sizeof(VertexPT)*verNum );
//
//	VBDesc.Usage				= D3D11_USAGE_DYNAMIC;
//	VBDesc.ByteWidth			= sizeof(VertexPT)* verNum;
//	VBDesc.BindFlags			= D3D11_BIND_VERTEX_BUFFER;
//	VBDesc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
//	VBDesc.MiscFlags			= NULL;
//	VBDesc.StructureByteStride	= NULL;
//
//	VBData.pSysMem				= vertices;
//	VBData.SysMemPitch			= 0;
//	VBData.SysMemSlicePitch		= 0;
//
//	m_pGame->m_pd3dDevice->CreateBuffer( &VBDesc, &VBData, &m_pVB );
//	
//	//--------------Index Buffer------------
//	//probuje ustawic jeden - bo w koncu quady wyswietlane sa w ten sam sposob.
//	unsigned short* indices = new unsigned short[indNum];
//
//	int ind = 0;
//	for( unsigned i=0; i<indNum; i+=6 )
//	{
//		indices[i]   = ind;
//		indices[i+1] = ind+1;
//		indices[i+2] = ind+2;
//		indices[i+3] = ind+2;
//		indices[i+4] = ind+1;
//		indices[i+5] = ind+3;
//		ind += 4;
//	}
//
//	IBDesc.Usage				= D3D11_USAGE_DEFAULT;
//	IBDesc.ByteWidth			= sizeof( unsigned short )*indNum;
//	IBDesc.BindFlags			= D3D11_BIND_INDEX_BUFFER;
//	IBDesc.CPUAccessFlags		= NULL;
//
//	IBData.pSysMem				= indices;
//	IBData.SysMemPitch			= NULL;
//	IBData.SysMemSlicePitch		= NULL;
//
//	m_pGame->m_pd3dDevice->CreateBuffer( &IBDesc, &IBData, &m_pIB );
//}
//
//void CFont::GetGameInstance( const CGame* pGame )
//{
//	m_pGame = pGame;
//}
//
////DrawText( text, x, y, scale, color );
////
////tego nie bedzie, bedzie po prostu ustawianie parametrow innymi funkcjiami przed wypisaniem i pozniejsamo draw.
////ustawic mWorld tak, zeby zmniejszala z rozmiaru tekstu, na rozmiar pikseli ekranu
//void CFont::Draw( const string& s )
//{
//	
//	unsigned	charsNum = s.size(),
//				verNum	= charsNum*4,
//				verIt = 0,
//				i;
//	unsigned short	CharX, CharY, Width, Height, CurX = 0;
//	short OffsetX, OffsetY, SrcHeight;
//	VertexPT* Verts = new VertexPT[verNum];
//
//	//tymczasowo file
//	ofstream file;
//	file.open( "FONT_RESULT.txt" );
//
//	for( i=0; i < charsNum; ++i )
//	{
//		CharX = m_pCharset->chars[s[i]].x;
//		CharY = m_pCharset->chars[s[i]].y;
//		Width = m_pCharset->chars[s[i]].width;
//		Height = m_pCharset->chars[s[i]].height;
//		OffsetX = m_pCharset->chars[s[i]].xOffset;
//		OffsetY = m_pCharset->chars[s[i]].yOffset;
//		SrcHeight = m_pCharset->lineHeight - OffsetY;
//
//		//lower left
//		Verts[i*4].Tex.x = (float) CharX / (float) m_pCharset->width;
//		Verts[i*4].Tex.y = (float) (CharY+Height) / (float) m_pCharset->height;
//		Verts[i*4].Pos.x = (float) CurX + OffsetX;
//		Verts[i*4].Pos.y = (float) SrcHeight - Height;
//		Verts[i*4].Pos.z = 0.0f;
//		
//		//upper left
//		Verts[i*4+1].Tex.x = (float) CharX / (float) m_pCharset->width;
//		Verts[i*4+1].Tex.y = (float) CharY / (float) m_pCharset->height;
//		Verts[i*4+1].Pos.x = (float) CurX + OffsetX;
//		Verts[i*4+1].Pos.y = (float) SrcHeight;
//		Verts[i*4+1].Pos.z = 0.0f;
//
//		//lower right
//		Verts[i*4+2].Tex.x = (float) (CharX+Width) / (float) m_pCharset->width;
//		Verts[i*4+2].Tex.y = (float) (CharY+Height) / (float) m_pCharset->height;
//		Verts[i*4+2].Pos.x = (float) Width + CurX + OffsetX;
//		Verts[i*4+2].Pos.y = (float) SrcHeight - Height;
//		Verts[i*4+2].Pos.z = 0.0f;
//
//		//upper right
//		Verts[i*4+3].Tex.x = (float) (CharX+Width) / (float) m_pCharset->width;
//		Verts[i*4+3].Tex.y = (float) CharY / (float) m_pCharset->height;
//		Verts[i*4+3].Pos.x = (float) Width + CurX + OffsetX;
//		Verts[i*4+3].Pos.y = (float) SrcHeight;
//		Verts[i*4+3].Pos.z = 0.0f;
//
//		CurX += m_pCharset->chars[s[i]].xAdvance;
//		file << "CurX = " << CurX << endl;
//
//	}
//	file.close();
//	//------------------------Map Buffer--------------------------
//	D3D11_MAPPED_SUBRESOURCE mappedRes;
//
//	m_pGame->m_pImmediateContext->Map( m_pVB, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRes );
//
//	memcpy( (VertexPT*)mappedRes.pData, Verts, sizeof(VertexPT) * verNum );
//
//	m_pGame->m_pImmediateContext->Unmap( m_pVB, 0 );
//
//	//------------------------Draw--------------------------------
//	m_pGame->m_pImmediateContext->IASetVertexBuffers( 0, 1, &m_pVB, &m_iStride, &m_iOffset );
//	m_pGame->m_pImmediateContext->IASetIndexBuffer( m_pIB, DXGI_FORMAT_R16_UINT, 0 );
//	m_pGame->m_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	m_pGame->m_pShader->SetShader( "PT" );
//	m_pGame->m_pTexture->SetTexture( m_pTextureRV );
//
//	ComputePosition();
//
//	//char buff[150];
//	//sprintf( buff, "X = %f\nY = %f\npX = %f\npY = %f\nPlayerPosX = %f\nPlayerPosY = %f\n", Verts[0].Pos.x*0.01f, Verts[0].Pos.y*0.01f, Verts[3].Pos.x*0.01f, Verts[3].Pos.y*0.01f, m_pGame->m_pPlayer->GetX(), m_pGame->m_pPlayer->GetY() );
//	//OutputDebugString( buff );
//
//	CBWVP cb;
//	cb.mWorld		= XMMatrixTranspose( XMLoadFloat4x4( m_mWorld ) );
//	cb.mView		= XMMatrixTranspose( m_pGame->m_View );
//	cb.mProjection	= XMMatrixTranspose( m_pGame->m_Projection );
//	m_pGame->m_pShader->SetConstantBuffer( cb );
//
//	
//	m_pGame->m_pImmediateContext->DrawIndexed( charsNum*6, 0, 0 );
//
//
//
//	delete [] Verts;
//}
//
//void CFont::Size( float size )
//{
//	m_fSize = size;
//}
//
//void CFont::Position( float x, float y, float z )
//{
//	m_vPos->x = x;
//	m_vPos->y = y;
//	m_vPos->z = z;
//}
//
//void CFont::ComputePosition()
//{
//	//calculate scale ----------------------------
//	static float scale;
//	scale = m_fSize*0.001f;
//
//	//
//
//
//	XMMATRIX mtx = XMMatrixScaling( scale, scale, 1.0f );
//	mtx *= XMMatrixTranslation( m_vPos->x, m_vPos->y, m_vPos->z );
//
//	XMStoreFloat4x4( m_mWorld, mtx );
//}