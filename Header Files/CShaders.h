#ifndef CSHADERS_H
#define CSHADERS_H

#include "CSystem.h"

class CSystem;

class CShaders
{
public:
   CShaders();
   CShaders( CSystem* A_pSystem );
   virtual ~CShaders();

   
private:
   CSystem*       m_pSystem;
};



#endif