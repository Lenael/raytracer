#ifndef CGPURAYTRACEROLD_H
#define CGPURAYTRACEROLD_H

#include "CSystem.h"

class CSystem;
class CQuad;

class CGPURaytracerOld
{
public:
   CGPURaytracerOld();
   CGPURaytracerOld( CSystem* A_pSystem );
   virtual ~CGPURaytracerOld();

   void Init();

   void Render();

private:
   struct Material
   {
      XMFLOAT4 emissiveColor;
      XMFLOAT4 diffuseColor;
      XMFLOAT4 specularColor;
      float shininess;
      float alpha;
      float reflectivity;
      //float pad1; // Padding
   };

   struct Sphere
   {
      XMFLOAT4 center;
      float radius;
      //XMFLOAT3 pad1; // Padding[3]
      Material material;
   };

   struct PointLight
   {
      XMFLOAT4 position;

      XMFLOAT4 color;
   };

   struct Stack
   {
      XMFLOAT4 position;
      XMFLOAT3 direction;
      //float    pad1;
      XMFLOAT4 color;
      XMFLOAT4 hitPosition;
      XMFLOAT3 hitDirection;
      //float    pad2;
      XMFLOAT4 biasedPositiveHitPosition;
      float valid;
      float fresnel;
      float sphereIndex;
      float insideSphere;
   };

   struct Data
   {
      XMFLOAT3 v1;
      XMFLOAT3 v2;
   };

   void CGPURaytracerOld::glusRaytraceLookAtf(float* positionBuffer, float* directionBuffer, const float* originDirectionBuffer, const int width, const int height, const float eyeX, const float eyeY, const float eyeZ, const float centerX, const float centerY, const float centerZ, const float upX, const float upY, const float upZ);
   void CGPURaytracerOld::glusMatrix3x3MultiplyVector3f(float result[3], const float matrix[9], const float vector[3]);
   bool glusRaytracePerspectivef(float* directionBuffer, const float fovy, const int width, const int height);


   CSystem*       m_pSystem;
   int            m_numElements;
	//ID3D11Buffer*  m_pOutputBuffer;
	//ID3D11Buffer*  m_pOutputDebugBuffer;
	//ID3D11ShaderResourceView* m_pInput1SRV;
	//ID3D11ShaderResourceView* m_pInput2SRV;
 //  ID3D11UnorderedAccessView* m_pOutputUAV;
   
   // positions and directions of first ray
	ID3D11ShaderResourceView* m_pDirections;
	ID3D11ShaderResourceView* m_pPositions;
	ID3D11Buffer*             m_pOutputStack;
	ID3D11Buffer*             m_pOutputDebugStack;
	ID3D11ShaderResourceView* m_pInputStackSRV;
	ID3D11UnorderedAccessView* m_pStack;
	ID3D11ShaderResourceView*  m_pSpheres;
	ID3D11ShaderResourceView*  m_pPointLights;
   
	ID3D11Buffer*              m_pOutputTextureBuffer;
	ID3D11Buffer*              m_pOutputTextureDebugBuffer;
	ID3D11UnorderedAccessView* m_pTextureUAV;
   ID3D11Texture2D*           m_pTexture;
   ID3D11ShaderResourceView*  m_pTextureSRV;
   float*                     m_tabPixels;
   CQuad*                     m_pQuad;
};









#endif // CGPURAYTRACEROLD_H