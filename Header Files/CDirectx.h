#ifndef CDIRECTX_H
#define CDIRECTX_H

#include "CSystem.h"
#include <DirectXMath.h>

#include <d3d11.h>

class CSystem;

class CDirectx
{
public:
   CDirectx();
   CDirectx( CSystem* A_pSystem );

   bool Init();
   void Resize();
   void Present();

//private:
public:
   ID3D11Device*              m_pD3DDevice;
	ID3D11DeviceContext*       m_pD3DContext;
	IDXGISwapChain*            m_pSwapChain;
	ID3D11RenderTargetView*    m_pRenderTargetView;
	ID3D11Texture2D*           m_pDepthStencilBuffer;
	ID3D11DepthStencilView*    m_pDepthStencilView;
	D3D11_VIEWPORT             m_ScreenViewport;

private:
   CSystem *      m_pSystem;
   bool           m_b4XMsAAEnabled;
   bool           m_bWindowed;

   unsigned int   m_ui4xMsAAQuality;
};










#endif //CDIRECTX_H