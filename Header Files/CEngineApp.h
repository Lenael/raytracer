#ifndef CENGINEAPP_H
#define CENGINEAPP_H

//-----------------------------------------------------------------------------
// CEngineApp Specific Includes
//-----------------------------------------------------------------------------
#include <windows.h>
#include "CSystem.h"

class CSystem;

class CEngineApp
{
public:
    //-------------------------------------------------------------------------
	// Constructors & Destructors for This Class.
	//-------------------------------------------------------------------------
	         CEngineApp();
	virtual ~CEngineApp();

	//-------------------------------------------------------------------------
	// Public Functions for This Class
	//-------------------------------------------------------------------------
   LRESULT     DisplayWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam );
   LRESULT     SettingsWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam );
	bool        InitInstance( HANDLE hInstance, LPCTSTR lpCmdLine, int iCmdShow );
   int         BeginGame( );
	bool		   Shutdown( );
   const HWND  GetHwnd( ) const;
   const unsigned GetViewWidth() const;
   const unsigned GetViewHeight() const;
	
private:
    //-------------------------------------------------------------------------
	// Private Functions for This Class
	//-------------------------------------------------------------------------
    void        FrameAdvance( );
    bool        CreateDisplay( );
    bool        CreateSettingsDialog( );
    bool        CreateTabControl( );

    //-------------------------------------------------------------------------
	// Private Static Functions For This Class
	//-------------------------------------------------------------------------
    static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);
    static LRESULT CALLBACK StaticSettingsWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);

    //-------------------------------------------------------------------------
	// Private Variables For This Class
	//-------------------------------------------------------------------------
   HWND        m_hWnd;					// Main window HWND

   HWND        m_hWndSettings;		// 
   HWND        m_hSettingsTabControl;		// TabControl of settings dialog
   HWND        m_hVisibleObjectsGroupBox;
   HWND        m_hRenderStateGroupBox;
   HWND        m_hCollisionTypeGroupBox;

   CSystem*	m_pSystem;

   unsigned	m_uiViewX;				// X Position of render viewport
   unsigned	m_uiViewY;				// Y Position of render viewport
   unsigned	m_uiViewWidth;			// Width of render viewport
   unsigned	m_uiViewHeight;	   // Height of render viewport
   unsigned	m_uiWidth;
   unsigned	m_uiHeight;
   unsigned	m_uiSettingsDialogWidth;
   unsigned	m_uiSettingsDialogHeight;

   HFONT    m_hNormalFont;

};


#endif // CENGINEAPP_H