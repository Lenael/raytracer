#ifndef CSPHERE_H
#define CSPHERE_H

#include "IObject.h"
#include "CLightHelper.h"
#include <vector>
#include "CSystem.h"

class IObject;
class CSystem;


class CSphere
{
public:
	CSphere( CSystem* A_pSystem, const XMFLOAT3 &A_f3Position, const float &A_fRadius, const XMFLOAT3 &A_f3Color, 
		      const float &A_fReflection, const float &A_fTransparency, const XMFLOAT3 &A_f3EmissionColor);

	// metoda obliczajaca kolizje
   bool Intersect(const XMFLOAT3 &A_f3RayOrigin, const XMFLOAT3 &A_f3RayDirection, float *t0 = NULL, float *t1 = NULL);
   void SetPosition( float A_fX, float A_fY, float A_fZ );
   void Move( float A_fX, float A_fY, float A_fZ );
   bool SolveQuadratic(const float &a, const float &b, const float &c, float *x0, float *x1);

	XMFLOAT3 m_f3Position;                         
	float    m_fR;  
   float    m_fSquareR;
	XMFLOAT3 m_f3Color;
   XMFLOAT3 m_f3EmissionColor;
	float    m_fTransparency;
   float    m_fReflection;
   CSystem* m_pSystem;
};








#endif // CQUAD_H