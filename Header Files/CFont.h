//alpha blending
//zrobic w shaderze modyfikacje tak jak w tutorialu microsoftu
//zeby przemnazalo ostateczny kolor w PS z podanym w bufferze, zmienia kolor czcionki
//i obiektow np
//specjalny shader do fontow, albo nauczyc sie technique, chuj
//musi sprawdzac, czy color.r (np. r) == 0, jesli tak, to znaczy, ze to nie jest font i color.a = 0.0f;
//else color.rgb = pixelColor.rgb; color.a = 1.0f;
//poczytac we wstepie o maskowaniu textur wlasnie, bo to wyzej mozna zrobic na parametr (nie koniecznie col.r == 0.0f)
//chodzi o to ladowanie textur z kanalem alfa
//zrobic w texturze obsluge przezroczystosci
//dynamiczny constant buffer? poczytac dokumentacje
//dynamiczny vertexbuffer, bo bedziemy mogli zmienic go w trakcie pracy programu-zmienic tekst

//powinno wczytywac .dds
//#ifndef CFONT_H
//#define CFONT_H
//
//#include <fstream>
//#include <string>
//using namespace std;
//
//class CGame;
//
//class CCharInfo
//{
//	friend class CFont;
//
//	unsigned short	x, y;
//	unsigned short	width, height;
//	short	xOffset, yOffset;
//	unsigned short	xAdvance;
//
//};
//
//class CCharset
//{
//	CCharset()
//	{
//		chars = new CCharInfo[256];
//	}
//	~CCharset()
//	{
//		delete [] chars;
//	}
//
//	friend class CFont;
//
//	unsigned short	lineHeight;
//	unsigned short	base;
//	unsigned short	width;
//	unsigned short	height;
//	unsigned short	pages;
//	string			bmpName;
//
//	CCharInfo*		chars;
//};
//
//class CFont
//{
//public:
//	CFont();
//	~CFont();
//	
//	void			Init( const char* fileName, const CGame* pGame );
//	void			Draw( const string& s );
//
//	void			Size( float size = 5.0f );
//	void			Position( float x = 0.0f, float y = 0.0f, float z = -0.1f );
//	//NAPISAAAAAAAC - pozniej w funkcji gui tac drawInGameMessage( tekst )
//	void			Time( float time = 1.0f );
//
//	//reszta funkcji konfigurujacych
//	//SetPosition
//	//SetColor, SetSize...
//	//Draw( string );
//	//i tyle
//
//private:
//	int				ParseTxtFile( const char* fileName );
//	int				LoadBitmap(); //nazwe bierzemy z pliku txt
//	void			GetGameInstance( const CGame* pGame );
//	void			CreateBuffers();
//	void			ComputePosition();
//
//	CCharset*					m_pCharset;
//	unsigned					m_uMaxChars;
//
//	ID3D11ShaderResourceView*	m_pTextureRV;
//	const CGame*				m_pGame;
//	XMFLOAT4X4*					m_mWorld;
//	XMFLOAT3*					m_vPos;
//
//	ID3D11Buffer*				m_pVB;
//	ID3D11Buffer*				m_pIB;
//
//	UINT						m_iStride;
//	UINT						m_iOffset;
//
//	//--------------- Font Parameters ---------------
//	//XMFLOAT4					m_Color; ?
//	float						m_fSize;
//};


#endif
