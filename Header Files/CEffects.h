#ifndef EFFECTS_H
#define EFFECTS_H

#pragma region Effect

#include <DirectXMath.h>
#include <iostream>
#include "D3dx11effect.h"
#include "CLightHelper.h"

using namespace DirectX;

class CEffect
{
public:
	CEffect();
	CEffect(ID3D11Device* device, const std::string& filename);
	virtual ~CEffect();

private:
	CEffect(const CEffect& rhs);
	CEffect& operator=(const CEffect& rhs);

protected:
	ID3DX11Effect* mFX;
};
#pragma endregion


#pragma region BasicEffect
class BasicEffect : public CEffect
{
public:
	BasicEffect(ID3D11Device* device, const std::string& filename);
	~BasicEffect();

	void SetWorldViewProj(CXMMATRIX M)                  { WorldViewProj->SetMatrix(reinterpret_cast<const float*>(&M)); }
	void SetWorld(CXMMATRIX M)                          { World->SetMatrix(reinterpret_cast<const float*>(&M)); }
	void SetWorldInvTranspose(CXMMATRIX M)              { WorldInvTranspose->SetMatrix(reinterpret_cast<const float*>(&M)); }
	void SetTexTransform(CXMMATRIX M)                   { TexTransform->SetMatrix(reinterpret_cast<const float*>(&M)); }
	void SetEyePosW(const XMFLOAT3& v)                  { EyePosW->SetRawValue(&v, 0, sizeof(XMFLOAT3)); }
	void SetFogColor(const FXMVECTOR v)                 { FogColor->SetFloatVector(reinterpret_cast<const float*>(&v)); }
	void SetFogStart(float f)                           { FogStart->SetFloat(f); }
	void SetFogRange(float f)                           { FogRange->SetFloat(f); }
	void SetDirLights(const SDirectionalLight* lights)   { DirLights->SetRawValue(lights, 0, 3*sizeof(SDirectionalLight)); }
	void SetMaterial(const SMaterial& mat)               { Mat->SetRawValue(&mat, 0, sizeof(SMaterial)); }
	void SetDiffuseMap(ID3D11ShaderResourceView* tex)   { DiffuseMap->SetResource(tex); }
	void SetCubeMap(ID3D11ShaderResourceView* tex)      { CubeMap->SetResource(tex); }

	ID3DX11EffectTechnique* Light0Tech;
	ID3DX11EffectTechnique* Light1Tech;
	ID3DX11EffectTechnique* Light2Tech;
	ID3DX11EffectTechnique* Light3Tech;

	ID3DX11EffectTechnique* Light0TexTech;
	ID3DX11EffectTechnique* Light1TexTech;
	ID3DX11EffectTechnique* Light2TexTech;
	ID3DX11EffectTechnique* Light3TexTech;

	ID3DX11EffectTechnique* Light0TexAlphaClipTech;
	ID3DX11EffectTechnique* Light1TexAlphaClipTech;
	ID3DX11EffectTechnique* Light2TexAlphaClipTech;
	ID3DX11EffectTechnique* Light3TexAlphaClipTech;

	ID3DX11EffectTechnique* Light0FogTech;
	ID3DX11EffectTechnique* Light1FogTech;
	ID3DX11EffectTechnique* Light2FogTech;
	ID3DX11EffectTechnique* Light3FogTech;

	ID3DX11EffectTechnique* Light0TexFogTech;
	ID3DX11EffectTechnique* Light1TexFogTech;
	ID3DX11EffectTechnique* Light2TexFogTech;
	ID3DX11EffectTechnique* Light3TexFogTech;

	ID3DX11EffectTechnique* Light0TexAlphaClipFogTech;
	ID3DX11EffectTechnique* Light1TexAlphaClipFogTech;
	ID3DX11EffectTechnique* Light2TexAlphaClipFogTech;
	ID3DX11EffectTechnique* Light3TexAlphaClipFogTech;

   ID3DX11EffectTechnique* Light1ReflectTech;
	ID3DX11EffectTechnique* Light2ReflectTech;
	ID3DX11EffectTechnique* Light3ReflectTech;

	ID3DX11EffectTechnique* Light0TexReflectTech;
	ID3DX11EffectTechnique* Light1TexReflectTech;
	ID3DX11EffectTechnique* Light2TexReflectTech;
	ID3DX11EffectTechnique* Light3TexReflectTech;

	ID3DX11EffectTechnique* Light0TexAlphaClipReflectTech;
	ID3DX11EffectTechnique* Light1TexAlphaClipReflectTech;
	ID3DX11EffectTechnique* Light2TexAlphaClipReflectTech;
	ID3DX11EffectTechnique* Light3TexAlphaClipReflectTech;

	ID3DX11EffectTechnique* Light1FogReflectTech;
	ID3DX11EffectTechnique* Light2FogReflectTech;
	ID3DX11EffectTechnique* Light3FogReflectTech;

	ID3DX11EffectTechnique* Light0TexFogReflectTech;
	ID3DX11EffectTechnique* Light1TexFogReflectTech;
	ID3DX11EffectTechnique* Light2TexFogReflectTech;
	ID3DX11EffectTechnique* Light3TexFogReflectTech;

	ID3DX11EffectTechnique* Light0TexAlphaClipFogReflectTech;
	ID3DX11EffectTechnique* Light1TexAlphaClipFogReflectTech;
	ID3DX11EffectTechnique* Light2TexAlphaClipFogReflectTech;
	ID3DX11EffectTechnique* Light3TexAlphaClipFogReflectTech;

	ID3DX11EffectMatrixVariable* WorldViewProj;
	ID3DX11EffectMatrixVariable* World;
	ID3DX11EffectMatrixVariable* WorldInvTranspose;
	ID3DX11EffectMatrixVariable* TexTransform;
	ID3DX11EffectVectorVariable* EyePosW;
	ID3DX11EffectVectorVariable* FogColor;
	ID3DX11EffectScalarVariable* FogStart;
	ID3DX11EffectScalarVariable* FogRange;
	ID3DX11EffectVariable* DirLights;
	ID3DX11EffectVariable* Mat;

	ID3DX11EffectShaderResourceVariable* DiffuseMap;
	ID3DX11EffectShaderResourceVariable* CubeMap;
};
#pragma endregion

#pragma region RayTrace Effect
class RayTraceEffect : public CEffect
{
public:
	RayTraceEffect(ID3D11Device* device, const std::string& filename);
	~RayTraceEffect();

   // ------------------ raytracer
   void  SetDirections(ID3D11ShaderResourceView *srv)    { Directions->SetResource(srv); }
   void  SetPositions(ID3D11ShaderResourceView *srv)     { Positions->SetResource(srv); }
	void  SetStacks(ID3D11UnorderedAccessView* uav)       { Stacks->SetUnorderedAccessView(uav); }
   void  SetSpheres(ID3D11ShaderResourceView *srv)       { Spheres->SetResource(srv); }
   void  SetPointLights(ID3D11ShaderResourceView *srv)   { PointLights->SetResource(srv); }
   void  SetTextureOut(ID3D11UnorderedAccessView *srv)   { TextureOut->SetUnorderedAccessView(srv); }
   //void  SetImageData(ID3D11UnorderedAccessView *srv)    { ImageData->SetUnorderedAccessView(srv); }

   ID3DX11EffectTechnique* RayTraceTech;
   
	ID3DX11EffectShaderResourceVariable* Directions;
	ID3DX11EffectShaderResourceVariable* Positions;
	ID3DX11EffectShaderResourceVariable* Spheres;
	ID3DX11EffectShaderResourceVariable* PointLights;
	ID3DX11EffectUnorderedAccessViewVariable* Stacks;
   ID3DX11EffectUnorderedAccessViewVariable* TextureOut;
   //ID3DX11EffectUnorderedAccessViewVariable* ImageData;
};
#pragma endregion

#pragma region RayTracer Effect
class RayTracerEffect : public CEffect
{
public:
   RayTracerEffect(ID3D11Device* device, const std::string& filename);
   ~RayTracerEffect();

   // ------------------ raytracer
   //void  SetDirections(ID3D11ShaderResourceView *srv)    { Directions->SetResource(srv); }
   //void  SetPositions(ID3D11ShaderResourceView *srv)     { Positions->SetResource(srv); }
   //void  SetStacks(ID3D11UnorderedAccessView* uav)       { Stacks->SetUnorderedAccessView(uav); }
   void  SetSpheres(ID3D11ShaderResourceView *srv)         { Spheres->SetResource(srv); }
   //void  SetPointLights(ID3D11ShaderResourceView *srv)   { PointLights->SetResource(srv); }
   void  SetTextureOut(ID3D11UnorderedAccessView *srv)   { TextureOut->SetUnorderedAccessView(srv); }
   //void  SetParams(ID3D11ShaderResourceView *srv)        { Params->SetResource(srv); }
   //void  SetImageData(ID3D11UnorderedAccessView *srv)    { ImageData->SetUnorderedAccessView(srv); }

   ID3DX11EffectTechnique* RayTracerTech;

   //ID3DX11EffectShaderResourceVariable* Directions;
   //ID3DX11EffectShaderResourceVariable* Positions;
   ID3DX11EffectShaderResourceVariable* Spheres;
   //ID3DX11EffectShaderResourceVariable* PointLights;
   //ID3DX11EffectUnorderedAccessViewVariable* Stacks;
   ID3DX11EffectUnorderedAccessViewVariable* TextureOut;
   //ID3DX11EffectShaderResourceVariable* Params;
   //ID3DX11EffectUnorderedAccessViewVariable* ImageData;
};
#pragma endregion

#pragma region Effects
class CEffects
{
public:
	static void InitAll(ID3D11Device* device);
	static void DestroyAll();

	static BasicEffect* BasicFX;
	static RayTraceEffect* RayTracerOldFX;
   static RayTracerEffect* RayTracerFX;
};
#pragma endregion

#endif // CEFFECTS_H