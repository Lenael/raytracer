#ifndef CSYSTEM_H
#define CSYSTEM_H

#include "CDirectx.h"
#include "CEngineApp.h"
#include "CTimer.h"
#include "CStatistics.h"
#include "CCamera.h"
#include "CCPURaytracer.h"
#include "CGPURaytracer.h"
#include "CLightHelper.h"

class CDirectx;
class CEngineApp;
class CTimer;
class CStatistics;
class CCPURaytracer;
class CGPURaytracer;
class CCamera;


class CSystem
{
public:
   CSystem();
   CSystem( CEngineApp* A_pEngine );
   virtual ~CSystem();

	bool Init();
   void PreMessageLoop(); // One-call function before message loop (for timers mainly)
   void Render();

public: // Properties info
   bool IsEnginePaused() const;
   bool IsEngineRunning() const;

public: // Events
   void OnEngineInactive();
   void OnEngineActive();
   void OnResize();
   void OnSizeMaximized();
   void OnSizeMinimized();
   void OnSizeRestored();
   void OnEnterSizeMove();
   void OnExitSizeMove();
   void OnMouseDown(WPARAM A_BtnState, int x, int y);
   void OnMouseUp(WPARAM A_BtnState, int x, int y);
   void OnMouseMove(WPARAM A_BtnState, int x, int y);
	void SetMouseEnabled( bool A_bEnabled ); //TODO: zrobic CInput, ktory bedzie to wszystko ogarnial

public: // rendered objects TODO: encapsulation
   bool              m_bRenderByCPU;

public:  // For other engine modules - simplifies data flow in engine
   CEngineApp *      m_pEngine;
   CDirectx *        m_pDirectx;
   CTimer *          m_pTimer;
   CStatistics *     m_pStatistics;
   CCamera*          m_pCamera;
   CGPURaytracer*    m_pGPURaytracer;
   /*CTerrain*         m_pTerrain;
   CCylinder*        m_pCylinder;
   CBox*             m_pBox;
   CQuad*            m_pQuad;
   CBezierQuad*      m_pBezierQuad;*/
   CCPURaytracer*    m_pCPURaytracer;
   SDirectionalLight m_DirLights[3];
   int               m_numSceneNumber;
   bool              m_bGeometricCollision;
   float             m_fRefractiveIndex;

private:
   bool              m_bEnginePaused;     // minimised/inactive
   bool              m_bEngineRunning;    // running != initializing
   bool              m_bMinimized;
   bool              m_bMaximized;
   bool              m_bResizing;         // True when user grabs resize bar
   bool              m_bMouseEnabled;

};






#endif // CSYSTEM_H