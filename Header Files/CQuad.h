#ifndef CQUAD_H
#define CQUAD_H

#include "IObject.h"
#include "CLightHelper.h"
#include <vector>
#include "CSystem.h"

class IObject;
class CSystem;

class CQuad : public IObject
{
public:
   struct Vertex
	{
		Vertex(){}
		Vertex(const XMFLOAT3& p, const XMFLOAT3& n, const XMFLOAT3& t, const XMFLOAT2& uv)
			: Position(p), Normal(n), TangentU(t), TexC(uv){}
		Vertex(
			float px, float py, float pz, 
			float nx, float ny, float nz,
			float tx, float ty, float tz,
			float u, float v)
			: Position(px,py,pz), Normal(nx,ny,nz),
			  TangentU(tx, ty, tz), TexC(u,v){}

		XMFLOAT3 Position;
		XMFLOAT3 Normal;
		XMFLOAT3 TangentU;
		XMFLOAT2 TexC;
	};


   CQuad();
   CQuad( CSystem* A_pSystem );
   virtual ~CQuad();

   void SetPosition( CXMMATRIX M );
   virtual void Render( );
   virtual void Init();
   virtual void Init( float A_fWidth, float A_fHeight );
   void SetTextureSRV( ID3D11ShaderResourceView* A_TextureSRV );
   std::vector<Vertex>  GetVertices() const;
   std::vector<UINT>    GetIndices() const;

private:
   CSystem*       m_pSystem;
   float          m_fWidth;
   float          m_fHeight;
   float          m_fDepth;
   
	SMaterial      m_Material;
   XMFLOAT4X4     m_mWorld;
	ID3D11Buffer*  m_pVB;
	ID3D11Buffer*  m_pIB;
   ID3D11ShaderResourceView*  m_TextureSRV;
   //ID3D11ShaderResourceView*  m_BrickNormalTextureSRV;

   std::vector<Vertex> m_vVertices;
	std::vector<UINT> m_vIndices;
};









#endif // CQUAD_H