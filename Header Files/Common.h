#ifndef COMMON_H
#define COMMON_H
#include <iostream>
#include <DirectXMath.h>
#include <comdef.h>


// macros
#define RELEASECOM(x) { if( x != NULL ){ x->Release(); x = NULL; } }

#define SAFEDELETE(x) { if( x != NULL ) { delete x; x = NULL; } }
#define SAFEARRAYDELETE(x) { delete [] x; x = NULL; }

// helper common functions
template<typename T>
   static T CommonClamp(const T& x, const T& low, const T& high)
   {
	   return x < low ? low : (x > high ? high : x); 
   };

template<typename T>
   static T CommonMin(const T& a, const T& b)
   {
      return a < b ? a : b;
   }

template<typename T>
   static T CommonMax(const T& a, const T& b)
   {
      return a > b ? a : b;
   }
	 

void CommonExtractFrustumPlanes(DirectX::XMFLOAT4 planes[6], DirectX::CXMMATRIX A_M);


namespace CommonColors
{
	XMGLOBALCONST DirectX::XMVECTORF32 White     = {1.0f, 1.0f, 1.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Black     = {0.0f, 0.0f, 0.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Red       = {1.0f, 0.0f, 0.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Green     = {0.0f, 1.0f, 0.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Blue      = {0.0f, 0.0f, 1.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Yellow    = {1.0f, 1.0f, 0.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Cyan      = {0.0f, 1.0f, 1.0f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 Magenta   = {1.0f, 0.0f, 1.0f, 1.0f};

	XMGLOBALCONST DirectX::XMVECTORF32 Silver    = {0.75f, 0.75f, 0.75f, 1.0f};
	XMGLOBALCONST DirectX::XMVECTORF32 LightSteelBlue = {0.69f, 0.77f, 0.87f, 1.0f};
};


#if defined( DEBUG ) || defined( _DEBUG )
   #ifndef HR
    //  #define HR( xxx )                                  \
	   //{                                                  \   
		  // HRESULT hr = xxx;                               \
		  // if(FAILED(hr))                                  \
		  // {                                               \
			 //  DXTrace(__FILE__, __LINE__, hr, #xxx, TRUE); \
		  // }                                               \
	   //}
	   //#endif
      #define HR( x )            \
      {                          \
         HRESULT hr = x;         \
         if( FAILED( hr ) )     \
         {                       \
            _com_error error(hr);\
            LPCTSTR errorText = error.ErrorMessage();\
            if( errorText != NULL )    \
            {                       \
               MessageBox( 0, errorText, 0, 0 );    \
            } \
         }     \
      }
      #endif
#else
   #ifndef HR
      #define HR( x ) ( x )
   #endif
#endif


#endif //COMMON_H