#ifndef CCPURAYTRACER_H
#define CCPURAYTRACER_H

#include "IObject.h"
#include "CSphere.h"
#include "CQuad.h"
#include "CSystem.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace DirectX;

class IObject;
class CSphere;
class CQuad;
class CSystem;

class CCPURaytracer : public IObject
{
public:
   CCPURaytracer();
   CCPURaytracer( CSystem* A_pSystem );
   virtual ~CCPURaytracer();

   void SetPosition( CXMMATRIX M );
   virtual void Render( );

   virtual void Init();

private:
   XMFLOAT3 Trace( const XMFLOAT3 &A_f3RayOrigin, const XMFLOAT3 &A_f3RayDirection, const std::vector<CSphere *> &A_vSpheres, const int &A_iDepth);
   void GenerateComplexScene(int A_iSphereNum, float A_fRadius, float A_fOffsetX, float A_fOffsetY, float A_fOffsetZ);
   float mix(const float &a, const float &b, const float &mix);
   void Raytrace( );
   void SetupScene();

private:
   CSystem*                   m_pSystem;
   std::vector<CSphere*>      m_vCSpheres;
   CQuad*                     m_pQuad;
   
   unsigned                   m_iWidth;
   unsigned                   m_iHeight;
   int                        m_iMaxRayDepth;
   float*                     m_tabPixels;
   float                      m_fInfinity;
   XMFLOAT4X4                 m_mWorld;
   ID3D11Texture2D*           m_pTexture;
   ID3D11ShaderResourceView*  m_pTextureSRV;
   XMFLOAT3                   m_f3Offset;
};









#endif // CCPURAYTRACER_H