#ifndef CTEXTURE_H
#define CTEXTURE_H

#include "CSystem.h"

class CSystem;

class CTexture
{
public:
	CTexture();
	CTexture( CSystem* A_pSystem );

   static ID3D11ShaderResourceView* CreateTexture2DArraySRV( ID3D11Device* device, ID3D11DeviceContext* context,
		std::vector<std::string>& filenames);
private:

   CSystem*          m_pSystem;
};

#endif // CTEXTURE_H