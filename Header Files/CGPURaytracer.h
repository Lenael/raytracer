#ifndef CGPURAYTRACER_H
#define CGPURAYTRACER_H

#include "CSystem.h"

class CSystem;
class CQuad;

class CGPURaytracer
{
public:
   CGPURaytracer();
   CGPURaytracer( CSystem* A_pSystem );
   virtual ~CGPURaytracer();

   void Init();

   void Render();

private:
   void SetupScene();

   struct Material
   {
      Material() {};
      Material(XMFLOAT3 A_f3EmissiveColor, XMFLOAT3 A_f3DiffuseColor, float A_fShininess, float A_fAlpha, float A_fReflectivity)
      {
         f3EmissiveColor = A_f3EmissiveColor;
         f3DiffuseColor = A_f3DiffuseColor;
         fShininess = A_fShininess;
         fAlpha = A_fAlpha;
         fReflectivity = A_fReflectivity;
      }
      XMFLOAT3 f3EmissiveColor;
      XMFLOAT3 f3DiffuseColor;                   
      float    fShininess;
      float    fAlpha;
      float    fReflectivity;

   };

   struct Sphere
   {
      Sphere() {};
      Sphere(XMFLOAT3 A_f3Center, float A_fRadius, Material A_sMaterial)
      {
         f3Center = A_f3Center;
         fRadius = A_fRadius;
         sMaterial = A_sMaterial;
      }
      XMFLOAT3 f3Center;
      float    fRadius;
      Material sMaterial;
   };

   struct Stack
   {
      XMFLOAT4 position;
      XMFLOAT3 direction;
      //float    pad1;
      XMFLOAT4 color;
      XMFLOAT4 hitPosition;
      XMFLOAT3 hitDirection;
      //float    pad2;
      XMFLOAT4 biasedPositiveHitPosition;
      float valid;
      float fresnel;
      float sphereIndex;
      float insideSphere;
   };

   struct Common
   {
      XMUINT2 dimension;
      float fov;
      float pi;
      XMFLOAT4 f4BackgroundColor; //float4, bo musi to byc potega 16
      bool bGeometricCollision;
      XMFLOAT3 f3Offset;
      float fRefractiveIndex;
      XMFLOAT3 f3ByteOffset;
   };


   const int GetNumPixels() const;
   void GenerateComplexScene(Sphere A_tabSphere[], int A_iSphereNum, float A_fRadius, float A_fOffsetX, float A_fOffsetY, float A_fOffsetZ);


   CSystem*       m_pSystem;

   // positions and directions of first ray
   int            m_iWidth;
   int            m_iHeight;

   //ID3D11ShaderResourceView*  m_pParamsSRV;
   ID3D11Buffer*              m_pCommon;
	ID3D11Buffer*              m_pOutputTextureBuffer;
	ID3D11Buffer*              m_pOutputTextureDebugBuffer;
	ID3D11UnorderedAccessView* m_pTextureUAV;
   ID3D11Texture2D*           m_pTexture;
   ID3D11ShaderResourceView*  m_pTextureSRV;
   ID3D11ShaderResourceView*  m_pSpheres;
   float*                     m_tabPixels;
   CQuad*                     m_pQuad;
   XMFLOAT3                   m_f3Offset;
   int                        m_numSpheresNumber;
   std::vector<Sphere>        m_vCSpheres;
};









#endif // CGPURAYTRACER_H