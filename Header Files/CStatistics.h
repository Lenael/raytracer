#ifndef CSTATISTICS_H
#define CSTATISTICS_H
#include "CSystem.h"
#include <sstream>
#include <unordered_map>


class CSystem;

class CStatistics
{
public:
   CStatistics();
   CStatistics( CSystem* A_pSystem );

   bool Init();
   void Render();
   void AddParameter( std::string A_strParameter );
   void RemoveParameter( std::string A_strParameter );
   void AddValue( std::string A_strParameter, std::string A_strValue );
   void AddValue( std::string A_strParameter, float A_fValue );

private:
   void CalculateStatistics();
   void CalculateFrameStats();


private:
   CSystem *                                       m_pSystem;
   std::unordered_map< std::string, std::string>   m_vParameters;

};










#endif //CSTATISTICS_H